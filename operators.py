import numpy as np
from td2d.kwant_wrapper import get_ham as get_h


def charge_density(rho, spin_full=False):
    """
    Compute the charge density in the entire system.

    Parameters
    ----------
    rho : numpy 2D array
        The density matrix.
    spin_full : bool (optional)
        Treat the density matrix as if each site corresponds
        to a spinless orbital (a number) or a spinfull
        orbital (a 2x2 matrix).

    Returns
    -------
    charge density : numpy 1D array
        Distribution of electron charges for a given
        density matrix.
    """

    if spin_full:
        rho_diag = np.diag(rho)
        chd = (rho_diag[0:-1:2] +
               np.append(rho_diag[1:-1:2],
                         rho_diag[-1]))
    else:
        chd = np.diag(rho)

    return np.real(chd)


class SpinDensity:

    def __init__(self, device, filename=None,
                 where=None, fmt='%10.5e'):

        self.dev = device
        self.filename = filename
        self.fmt = fmt
        if filename:
            open(filename, 'w').close()   # Clear the file

    def measure(self, time, rho):
        """
        Compute the spin density in the entire system.

        Parameters
        ----------
        time : float
            Present time in fs.
        rho : numpy 2D array
            The density matrix.

        Returns
        -------
        spin_density : numpy nd-array
            Local spin density

        Notes
        -----
        If the 'filename' was set during the object
        initialization, the `measure` function will update
        the specified file (with addtion of the time
        parameter).

        """

        # Spin-z density
        rho_diag = np.diag(rho)
        sz_up = rho_diag[0::2]
        sz_down = rho_diag[1::2]
        spdz = (sz_up - sz_down).real

        # Spin-x density
        rho_d_up = np.diagonal(rho, offset=1)[0::2]
        rho_d_down = np.diagonal(rho, offset=-1)[0::2]

        spdx = (rho_d_up + rho_d_down).real

        # Spin-y density
        spdy = -(rho_d_up - rho_d_down).imag

        spd = np.array([spdx, spdy, spdz])
        arr_size = np.size(spd)
        spd = np.reshape(spd, arr_size, order='F')

        if self.filename:
            spdt = np.insert(spd, 0, time)
            update(self.filename, spdt, fmt=self.fmt)

        return spd


class ChargeCurrent:
    """ An object used to measure local charge currents

    Parameters
    ----------
    device : an instance of the Device class
        The Device is necessary so the curent measuring
        object can querry about the time-dependent
        hamiltonian associated with this class.
    filename : string (optional)
        A txt file where you want to update the output
        after every measurement. The default value is
        'None', meaning no output is being saved
        externaly.
    where : a function (optional)
        Maps the sites for which the object will compute
        the current density. The function has to have
        one argument (the KWANTs Site object) and it
        should return a boolean value (True/False)
        wether to measure the current on that site or
        not. By default, where parameter is set to
        'None' which will measure the current on
        all sites.
    fmt : string (optional)
        Format specifier for the charge current values
        that are saved in the output file.
        Default value is '%10.5e'.
    """

    def __init__(self, device, filename=None,
                 where=None, fmt='%10.5e'):

        self.dev = device
        self.filename = filename
        self.fmt = fmt

        if filename:
            open(filename, 'w').close()   # Clear the file

        ratio = self.dev.dim_h / len(self.dev.sys.sites)

        if ratio == 1:
            self.spinfull = False
            self.norb = 1
        elif ratio == 2:
            self.spinfull = True
            self.norb = 2
        else:
            estr = ('KWANT system is probably inhomogenious'
                    'TD2D only works with systems with a '
                    ' single orbital (spinless) or '
                    ' double orbital (spinfull) not with'
                    'mixes of the two.')
            raise TypeError(estr)

        if where is None:
            self.where = self._default_where
        else:
            self.where = where

    def _default_where(self, site1):
        return True

    def measure(self, time, rho, bonds=None, eps=1.e-5):
        """Compute the charge current in the system.

        time : float
            The time in femtoseconds at a given
            propagation step.  This is needed so the
            proper Hamiltonian for a given time is envoked.
        rho : numpy 2D array
            The density matrix.
        bonds : a list of four float tuples (optional)
            Select the charge current only for specified
            sites based on their position. For example
            if two sites A and B have coordinates
            (4, 5) and (4, 4), then setting
            bonds=[(4, 5, 4, 4)], will only save the
            current between those two sites. By default
            this parameter is set to 'None', which will save
            all the bond currents specified by the 'where'
            function. If 'where' was not set, all bond
            currents will be computed.
        eps : float (optional)
            Tolerance used to compare positions if
            'bonds' parameter is provided.

        Returns
        -------
        current : numpy 2D array
            The computed charge current.

        Notes
        -----
        The current is saved as a numpy array in two formats,
        depending wether or not the 'bonds' parameter is
        provided. In case, 'bonds' is set to 'None' (the
        default value) the folowing format: (x1, y1, x2, y2,
        j) is used for each bond current. Here x1, y1 and x2,
        y2 are the coordinates of the origin and target
        sites, and j is te current between them. If
        coordinates (x1, y1, x2, y2) are provided through
        the 'bonds' parameter, then only current j is saved
        for each touple of the four positions. If the
        sites provided with bonds are not neighbors defined
        in the Hamiltonian, the returned bond current will
        be set to zero.

        If 'where' function was specified during the object
        creation, 'measure' function will only compute
        the current for thoose sites for which 'where'
        function returns True. Note that there is double
        selectivity of the bond currents with 'where'
        function and with 'bonds' parameter. The 'where'
        function determines which bond currents to compute,
        whereas 'bonds' determine which of those computed
        currents to return and save to the output.
        In other words, 'bonds' acts as an extra filter.
        Note that 'bonds' can be used even if 'where' was
        set to 'None'. In that case the bond currents will
        be computed in the entire system, and the ones
        filtered with 'bonds' will be saved. The 'where'
        function purpose is to reduce the computation
        time, if the system is big, and we only want to
        compute current on specific (small) group of sites.

        Also, if the 'filename' was set during
        the object initialization, the `measure` function
        will update the specified file (and add
        the time parameter to the output). The otput format
        for the file depends wether the 'bonds' parameter
        is provided or not (see above). If the 'filename'
        was not specified, the 'measure' function will still
        return the output variable, but it won't write any of
        the results to the output file.
        """

        current = []
        neighbors = self.dev.sys.graph.out_neighbors
        indx = self.dev.sys.id_by_site
        ham = self.dev.get_h(time)
        no = self.norb     # Number of orbitals (1 or 2)
        for site_i in self.dev.sys.sites:
            if self.where(site_i):      # Measure J on site i
                i = indx[site_i]
                xi, yi = site_i.pos
                for j in neighbors(i):
                    site_j = self.dev.sys.sites[j]
                    xj, yj = site_j.pos

                    r_ij = rho[no*i:no*(i+1), no*j:no*(j+1)]

                    h_ij = ham[no*i:no*(i+1), no*j:no*(j+1)]

                    curr_ij = r_ij*h_ij - np.conj(r_ij*h_ij)

                    curr_ij *= (-2 * np.pi * 1j)

                    curr_ij = curr_ij.sum().real

                    local_j = [xi, yi, xj, yj, curr_ij]
                    current.append(local_j)

        current = np.array(current)

        nand = np.logical_and

        if bonds:
            bond_current = []
            for bond in bonds:
                err_str = 'Wrong number of elements in bonds'
                assert len(bond) == 4, err_str
                x1, y1, x2, y2 = bond

                c1 = abs(current[:, 0] - x1) < eps
                c2 = abs(current[:, 1] - y1) < eps
                c3 = abs(current[:, 2] - x2) < eps
                c4 = abs(current[:, 3] - y2) < eps
                jb = current[:, 4]
                cond = nand(nand(c1, c2), nand(c3, c4))
                jb = jb[cond]
                if len(jb) == 0:
                    jb = 0.0

                bond_current.append(jb)
            current = np.array(bond_current)

        if self.filename:
            cout = current.flatten()
            cout = np.insert(cout, 0, time)
            update(self.filename, cout, fmt=self.fmt)

        return current


def local_sp_currents(rho, sys, sys_args, *bonds,
                      where='all'):
    """
    Function to evaluate the local spin currents.

    Parameters
    ----------
    rho : numpy 2D array
        Contains the density matrix.
    sys : kwant.builder.systems
        This is a KWANT object that can be used to
        find the Hamiltonian xi-vectors etc.
    sys_args : list
        Contains a list of arguments specific for
        building "sys".
    where : string
        Tells where to evaluate the charge currents.
    bonds : list of tuples
        Contains a list of tuples where the bond current
        is being evaulated. E.g. [(1,2) , (2, 3),
        (10, 11)]

    Returns
    -------
    local_spj : numpy 1D array
        Contains the spin bond current flowing between
        the sites provided in the list.

    """
    i, j = np.transpose(bonds)
    ham = get_h(sys, sys_args)

    # TODO: Extraction of local spin currents for all
    # condition
    if where == 'all':
        local_spj = np.array([0, 0])

    elif where == 'ij':
        i0 = 2*i
        j0 = 2*j

        i1 = 2*i
        j1 = 2*j + 1

        i2 = 2*i + 1
        j2 = 2*j

        i3 = 2*i + 1
        j3 = 2*j + 1
        x0 = (-(2*np.pi) * 1j *
              (rho[i0, j0]*ham[j1, i1] -
              np.conj(rho[i0, j0]*ham[j2, i2])))

        x1 = (-(2*np.pi) * 1j *
              (rho[i1, j1]*ham[j3, i3] -
              np.conj(rho[i1, j1]*ham[j0, i0])))

        x2 = (-(2*np.pi) * 1j *
              (rho[i2, j2]*ham[j0, i0] -
              np.conj(rho[i2, j2]*ham[j3, i3])))

        x3 = (-(2*np.pi) * 1j *
              (rho[i3, j3]*ham[j2, i2] -
              np.conj(rho[i3, j3]*ham[j1, i1])))

        spjx = x0 + x1 + x2 + x3

        y0 = (2 * np.pi * (rho[i0, j0]*ham[j1, i1] -
              np.conj(rho[i0, j0]*ham[j2, i2])))

        y1 = (2 * np.pi * (rho[i1, j1]*ham[j3, i3] +
              np.conj(rho[i1, j1]*ham[j0, i0])))

        y2 = (2 * np.pi * (-rho[i2, j2]*ham[j0, i0] -
              np.conj(rho[i2, j2]*ham[j3, i3])))

        y3 = (2 * np.pi * (-rho[i3, j3]*ham[j2, i2] +
              np.conj(rho[i3, j3]*ham[j1, i1])))

        spjy = y0 + y1 + y2 + y3

        z0 = (-(2*np.pi) * 1j * (rho[i0, j0]*ham[j0, i0] -
              np.conj(rho[i0, j0]*ham[j0, i0])))

        z1 = (-(2*np.pi) * 1j * (rho[i1, j1]*ham[j2, i2] +
              np.conj(rho[i1, j1]*ham[j2, i2])))

        z2 = (-(2*np.pi) * 1j * (-rho[i2, j2]*ham[j1, i1] -
              np.conj(rho[i2, j2]*ham[j1, i1])))

        z3 = (-(2*np.pi)*1j*(-rho[i3, j3]*ham[j3, i3] +
              np.conj(rho[i3, j3]*ham[j3, i3])))

        spjz = z0 + z1 + z2 + z3

        spj = np.real((np.array([spjx, spjy, spjz])))
        arr_size = np.size(spj)

    return np.reshape(spj, arr_size, order='F')


def update(filename, array, fmt='%10.5e'):
    with open(filename, 'ab') as f:
        if len(array.shape) == 1:
            np.savetxt(f, [array], fmt=fmt)
        else:
            np.savetxt(f, array, fmt=fmt)
