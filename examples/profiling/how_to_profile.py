# This example script demonstrates the use of python cProfile.
# There are several functions here. Run the following command
# when running the script.

# python -m cProfile -o temp.dat how_to_profile.py

# This will generate "temp.dat" which contains the profiling
# data file. Then use "snakeviz temp.dat" to view the profiling
# online. Make sure you have installed "snakeviz" first.

import time as tm

def fun_1():
    tm.sleep(1)
    return 1

def fun_2():
    tm.sleep(2)
    return 1

def fun_3():
    tm.sleep(3)
    return 1

def fun_4():
    tm.sleep(4)
    return 1

a = fun_1()
b = fun_2()
c = fun_3()
d = fun_4()

