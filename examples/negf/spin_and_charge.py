#!/usr/bin/env python3
from td2d import Device
from td2d.rk4numpy import RK4Numpy
from td2d.kwant_systems import make_fm
from td2d.kwant_systems import make_ribbon
from td2d.operators import ChargeCurrent, SpinDensity
import numpy as np
import sys


def main():

    rib = make_fm(3, 1)

    dev = Device(rib, args=[0., 0.0, 90, 0],
                 mu_leads=[0.05, -0.05])

    td_solver = RK4Numpy(dev, m_max=400, temp_k=300)

    # Create spin density and  charge current operators
    # and specify their output files.
    current = ChargeCurrent(dev, 'current.txt')
    sdens = SpinDensity(dev, 'spin_density.txt')

    dt = 0.1
    times = np.arange(0, 1, dt)    # Times in fs

    for time in times:

        print('T = %5.2f  [fs]' % time)
        td_solver.propagate(time, dt)
        rho = td_solver.rho(time)

        # Measure spin density and charge currents
        # Note that if a file is specified during the
        # creation of the current and sdens objects
        # measure will also update that file.
        # if you don't need the output, you can also
        # just write 
        # 
        #     current.measure(time, rho) 
        #
        # (without j) just to update the output file
        # By default, the returned and saved current 
        # will contain coordinates of the bonds
        # and current value on that bond (x1, y1, x2, y2, j)
        # The saved file will additionally have a time step
        # and the beginning, so it's format is
        # 
        # time, x1, y1, x2, y2, j, x1', y1', x2', y2', j',...
        #
        # You can have more control over which currents are
        # computed and saved, using 'where' parameter
        # and 'bonds' parameter of the ChargeCurrent object.
        # See the correspinding 
        # examples: 'spin_and_charge_where.py', and
        # 'spin_and_charge_bonds.py' for more details.

        j = current.measure(time, rho)
        sd = sdens.measure(time, rho)



if __name__ == '__main__':
    main()
