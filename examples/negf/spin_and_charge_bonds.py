#!/usr/bin/env python3
from td2d import Device
from td2d.rk4numpy import RK4Numpy
from td2d.kwant_systems import make_fm
from td2d.kwant_systems import make_ribbon
from td2d.operators import ChargeCurrent, SpinDensity
import numpy as np
import sys


def main():

    rib = make_fm(2, 2)

    dev = Device(rib, args=[0., 0.0, 90, 0],
                 mu_leads=[0.05, -0.05])

    td_solver = RK4Numpy(dev, m_max=400, temp_k=300)

    # Here we define the measurments operators
    # and their output locations.
    current = ChargeCurrent(dev, 'current_bonds.txt')
    sdens = SpinDensity(dev, 'spin_density_bonds.txt')

    dt = 0.1
    times = np.arange(0, 1, dt)    # Times in fs

    # Bonds are specified by the coordinates of sites for
    # which you want to save and return
    # the bond currents (x1, y1, x2, y2). Notice that the 
    # output format of the ChargeCurrent object changes
    # depending wheather 'bonds' argument is provided or not
    # (see the output of 'spin_and_charge_where.py' as an
    # example of the saved currents format when 'bonds'
    # parameter is not provided). If bonds are not provided
    # currents are saved in the format (x1, y1, x2, y2, j)
    # If bonds are provided, only the current 'j' for a
    # given bond is saved and not the coordinates of the bond
    # (since the user specified the bond).
    bonds = [(0, 0, 1, 0), (0, 1, 1, 1)] 

    for time in times:

        print('T = %5.2f  [fs]' % time)
        td_solver.propagate(time, dt)
        rho = td_solver.rho(time)

        current.measure(time, rho, bonds)
        sdens.measure(time, rho)


if __name__ == '__main__':
    main()
