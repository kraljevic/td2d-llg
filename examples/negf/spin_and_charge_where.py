#!/usr/bin/env python3
from td2d import Device
from td2d.rk4numpy import RK4Numpy
from td2d.kwant_systems import make_fm
from td2d.kwant_systems import make_ribbon
from td2d.operators import ChargeCurrent, SpinDensity
import numpy as np
import sys


def main():

    rib = make_fm(2, 2)

    dev = Device(rib, args=[0., 0.0, 90, 0],
                 mu_leads=[0.05, -0.05])

    td_solver = RK4Numpy(dev, m_max=400, temp_k=300)

    # Create the charge current operator to work only on
    # selected sites specified by the 'select' function
    # If you don't supply the 'where' argument by default
    # the current operator will compute the charge currents
    # You can do additional selection of the currents using
    # 'bonds' paramter in the ChargeCurrent.measure()
    # function (see 'spin_and_charge_bonds.py' example for
    # more details).
    current = ChargeCurrent(dev, 'current_where.txt',
                            where=select)

    sdens = SpinDensity(dev, 'spin_density_where.txt')

    dt = 0.1
    times = np.arange(0, 1, dt)    # Times in fs

    for time in times:

        print('T = %5.2f  [fs]' % time)
        td_solver.propagate(time, dt)
        rho = td_solver.rho(time)

        # Note that measure will
        current.measure(time, rho)
        sdens.measure(time, rho)


def select(site):
    # An example on how to define a 'where' function that is
    # to be passed to the ChargeCurrent 'where' parameter
    # of the 'measure' function.
    # Here, 'select' will be used for the charge current
    # operator to measure current going only on one specific
    # site x0 = 0, y0 = 0. Beside the coordinates,
    # you can use any other propertise of KWANT Site object
    # to make the selection, it doesn't have to be limited
    # only to position. When supplied to the ChargeCurrent
    # object, this function will cause it to compute only
    # the bond currents containing those sites
    # for which this function returns True. In other
    # words, this function is a map of sites where to 
    # compute the charge bond currents.

    x, y = site.pos
    eps = 1.0e-5
    
    x0 = 0
    y0 = 0
    
    c1 = abs(x - x0) < eps # Notice that coordinates are
    c2 = abs(y - y0) < eps # are usually floats, so this is
                           # better than x == x0, or y == y0
    return (c1 and c2)     
                             

if __name__ == '__main__':
    main()
