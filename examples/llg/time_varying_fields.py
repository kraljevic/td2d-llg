from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np


def custom_bfield(time, site):

    if 10 <= time <= 20:
        return np.array([0, 1000., 0])
    else:
        return np.array([0, 0, 0])


ribbon = make_ribbon(length=10, width=3)

cspins = Spins(ribbon, spin_config=(1, 0, 0), g_lambda=0.2,
               bf_func=custom_bfield)


scd = np.zeros(cspins.s.shape)

times = np.arange(0, 30, 0.1)

for time in times:

    print(time)
    cspins.llg(scd, time)
    cspins.plot(plot_type='mayavi', filename='fig_%03.0f.png'
                % (time*10))
