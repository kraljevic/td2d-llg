# ----------------------------------------
# Author : P. Mondal
# Date : April 20, 2020
# Goal : 
#    Add random spin orientation every
#    timestep
#    Customize Jsd for each defined device 
#    site
# ----------------------------------------


#!/usr/bin/env python3
from td2d import Device, Spins
from td2d.rk4numpy import RK4Numpy
from td2d.kwant_systems import make_fm
from td2d.kwant_systems import make_honeycomb, make_ribbon
from td2d.operators import ChargeCurrent, SpinDensity
import numpy as np
from td2d.constants import UNIT, SIG_X, SIG_Y, SIG_Z
from td2d.spin_configs import random_spins
import sys


def main():
    def test_custom_jsd(jsd, length):    
       def custom_jsd(time, site):
          """ Adds Jsd coupling to only spins from x>0 
              and x<length of device 
          """
          x, y = site.pos
          jsd_on = x > 0 
          jsd_off = x < length 
          # print('jsd is', jsd, length) 
          if jsd_on and jsd_off:
             return jsd 
          else:
             return 0.0 
       return custom_jsd
    
    # Build honeycomb device
    length = 7
    width  = 2
    rib = make_honeycomb(length, width, plot_sys=True)
    bias = 0.0
    dev = Device(rib, args=[0., 0.0, 90, 0],
                 mu_leads=[bias/2, -bias/2])
    dim_h = 2*length*width 
    td_solver = RK4Numpy(dev, m_max=2000, temp_k=300)

    # Here we define the measurments operators
    # and their output locations.
    current = ChargeCurrent(dev, 'current_bonds.txt')
    sdens = SpinDensity(dev, 'spin_density_bonds.txt')
    
    Jsd = 0.5 # eV
    dt = 0.1
    tp = 10 # Start perturbation 
    tf = 60.6 #100.1
    times = np.arange(0, tf, dt)    # Times in fs

    bonds = [(0, 0, 1, 0), (0, 1, 1, 1)]
    # Uncomment this line if you have saved rk_vectors for equilibrium
    # Loads saved rkvec for psi and omega 
    # td_solver.load_npz('../runlong/rk_vector.npz')

    # Adding jsd interaction only at sites: 0 < x < length  
    test_jsd = test_custom_jsd(Jsd, length)

    for time in times:
        if time > tp:
           # The argument is the random seed (optional)
           # Here I am interested in different random spin for each site 
           # for different times
           rand_spin_conf = random_spins(int(10*time))
           # Not providing the seed will return random spins for all site 
           # but will remain same for all time
           # print('random spin config at site 2, 3 ', rand_spin_conf(2),
           #                                            rand_spin_conf(5))
           cspins = Spins( rib, config=rand_spin_conf, jsd_to_negf=Jsd,
                           jsd_to_negf_func=test_jsd )
   
           print('perturbation started \n ') 
           dev.delta_onsite = cspins.onsite(time)
        print('t = %5.2f  [fs]' % time)
        td_solver.propagate(time, dt)
        rho = td_solver.rho(time)

        # print('cspins due to jsd at site 2 ',
        #                dev.delta_onsite[2])
        # print('cspins due to jsd at site 3 ',
        #                 dev.delta_onsite[3])


        current.measure(time, rho, bonds)
        sdens.measure(time, rho)
    # td_solver.save_npz('rk_vector.npz')      

if __name__ == '__main__':
    main()
