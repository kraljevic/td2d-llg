from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np

# First we create a finalized KWANT tight-binding system
ribbon = make_ribbon(length=5, width=4)

# Now we create classical spins on top of that
cspins = Spins(ribbon)
cspins.plot(filename='spins_3d.png')

# Spins can be saved either in a txt or npz file
np.savetxt('cspins.txt', cspins.s)
np.savez('cspins.npz', cspins=cspins.s)
