from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np


ribbon = make_ribbon(length=5, width=4)

cspins = Spins(ribbon, spin_config=(1., 1., 1), ani=0.1,
               ani_vec=(0., 1., 0), g_lambda=0.2)


scd = np.zeros(cspins.s.shape)

times = np.arange(0, 100, 0.1)

for time in times:

    print(time)
    cspins.llg(scd, time)
    cspins.plot(filename='fig_%03.0f.png' % (10*time))
