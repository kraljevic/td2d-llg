from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np

# First we create a finalized KWANT tight-binding system
ribbon = make_ribbon(length=5, width=4)

# Now we create classical spins
# and set a uniform Jsd coupling
cspins = Spins(ribbon, jsd_to_llg=0.1, jsd_to_negf=0.1)

# We can create a uniform (fake) nonequilibrium spin density.
# It needs to have the same shape as classical spins
s_cd = np.zeros(cspins.s.shape)

# We set it nonzero along the x axis
s_cd[:, 0] = 0.1

# Next we propagate the classical spins
times = np.arange(0, 10, 0.1)
with open('moments_vs_time.txt', "w") as f:

    for time in times:
        out_str = '%.2f ' % time
        print(out_str)

        cspins.llg(s_cd, time)
        for s in cspins.s:
            out_str += '%0.5e %0.5e %0.5e ' % (s[0], s[1], s[2])

        out_str += '\n'
        f.write(out_str)
