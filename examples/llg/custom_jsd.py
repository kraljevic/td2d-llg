from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np


def custom_to_llg(time, site):

    """Couple only spins in a ring with Jsd coupling """
    x, y = site.pos

    in_outer = (x-10)**2 + (y-10)**2 < 9**2
    in_inner = (x-10)**2 + (y-10)**2 < 7**2

    if in_outer and not in_inner:
        return 0.3
    else:
        return 0.0


ribbon = make_ribbon(length=20, width=20)
cspins = Spins(ribbon, spin_config=(0, 0, 1), g_lambda=0.2,
               jsd_to_llg_func=custom_to_llg)


scd = np.zeros(cspins.s.shape)
scd[:, 0] = 0.5

times = np.arange(0, 30, 0.1)

for time in times:

    print(time)
    cspins.llg(scd, time)
    cspins.plot(plot_type='mayavi', filename='fig_%03.0f.png'
                % (time*10))
