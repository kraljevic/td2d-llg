from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np


ribbon = make_ribbon(length=5, width=4)
cspins = Spins(ribbon, spin_config=(0, 0, 1), jsd_to_negf=0.2)

print(cspins.onsite(0.0))
