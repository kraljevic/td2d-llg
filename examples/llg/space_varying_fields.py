from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np


def custom_anisotropy(time, site):

    ani1 = 0.5
    ani2 = 0.1
    vec1 = np.array([0, 0, 1])
    vec2 = np.array([0, 1, 0])

    x, y = site.pos

    if x < 5:
        return ani1*vec1
    else:
        return ani2*vec2

def custom_exchange(time, site1, site2):

    jexc_1 = 0.1
    jexc_2 = 0.2

    jexc_12 = 0.5

    x1, y1 = site1.pos
    x2, y2 = site2.pos

    if x1 < 5 and x2 < 5:
        return jexc_1
    elif x1 > 4 and x2 > 4:
        return jexc_2
    else:
        return jexc_12


ribbon = make_ribbon(length=10, width=3)

cspins = Spins(ribbon, spin_config=(1., 1., 1), g_lambda=0.2,
               ani_func=custom_anisotropy,
               jexc_func=custom_exchange)


scd = np.zeros(cspins.s.shape)

times = np.arange(0, 100, 0.1)

for time in times:

    print(time)
    cspins.llg(scd, time)
    cspins.plot(plot_type='mayavi', filename='fig_%03.0f.png'
                % (time*10))
