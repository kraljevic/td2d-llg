from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np
from math import cosh, tanh


def main():

    ribbon = make_ribbon(length=10, width=4)
    cspins = Spins(ribbon, config=domain_wall)
    cspins.plot(plot_type='mayavi', filename='domain_wall.png')


def domain_wall(site, x_zero=5, width=2):

    x, y = site.pos
    spin = np.array([1.0/cosh((x_zero-x)/width), 0,
                     tanh((x_zero-x)/width)])
    return spin


if __name__ == main():
    main()
