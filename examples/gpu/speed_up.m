% These are the speed up results obtained from Google collab GPUs


% Results with transfer of data
mat_size = [100, 500, 1000, 2000, 5000, 8000, 10000];
spd2 = [0.91, 4.8, 6.85, 9.76, 11.47, 14.814, 15.9];

% Results without transfer of data
spd1 = [1.5, 64.11, 174.5, 606.48, 11326, 61414, 99509];

subplot(1, 2, 1)
plot(mat_size, spd1, '-*', 'linewidth', 2)
xlabel('Matrix Size')
ylabel('Speed up (GPU vs CPU)')
title('Without Data transfer')

subplot(1, 2, 2)
plot(mat_size, spd2, '-*', 'linewidth', 2)
xlabel('Matrix Size')
ylabel('Speed up (GPU vs CPU)')
title('With Data transfer')




