# THIS CODE SOLVES ODEs ON A PARAMETER 'E' IN PARALELL USING
# Charm4py THE SYSTEM OF EQUATIONS IS SOLVES A VECTOR OF SIZE 2 X 1
# --------------------------------------------------------
#	dY(t)/dt = -M(E)*Y(t)
#	Y(t) =  [y1(t); y2(t)]
#	If we choose Y(0) = [1 1], then the solution decays
# 	exponentially
# --------------------------------------------------------
# Utkarsh Bajpai, December 11, 2019
# --------------------------------------------------------

from td2d.dummy import ham_sq_latt, energy_uni_grid, two_state_sys
from td2d.qm import bra

import numpy as np
import cmath
import matplotlib.pyplot as plt

from charm4py import Chare, Group, charm, Reducer, Future

class OdeCharm(Chare):

	def charm_two_state_sys(self, ft, psi0, t, H, h_bar, energy):
		# Find the total grid size of energy
		n_grid = np.size(energy)
		# Find the total no. of PEs
		num_pes = charm.numPes()
		# Find the PE tag of this worker
		pe = self.thisIndex
		# Find the grid size for this worker
		rem = n_grid%num_pes
		worker_grid_size = (n_grid - rem)/num_pes
		# Find the initial index of energy for this worker
		posi = pe*worker_grid_size
		# Find the final index of energy for this worker
		if pe==num_pes-1:
			posf = n_grid
		else:
			posf = posi + worker_grid_size
		posi = int(posi)
		posf = int(posf)

		# Collect the energy grid for this worker
		worker_energy = energy[posi:posf]
		print('==========================')
		print(' Working in a core: ', pe)
		print('==========================')
		print('worker_energy is :: ', worker_energy)

		# Function to evolve ODEs on the given energy grid
		psi_out = two_state_sys(psi0, t, H, h_bar, worker_energy)

		return psi_out

def main(args):

	# parameters of the problem
	h_bar = 1

	Nx = 2
	Ny = 1
	gamma = 1

	ti = 0
	tf = 5
	nt = 200

	Ei = 0
	Ef = 12
	n_grid = 13

	energy = energy_uni_grid(Ei, Ef, n_grid)

	print('=============')
	print('Energy grid ')
	print('=============')
	print(energy)

	ft = Future()

	# System Hamiltonian
	H = ham_sq_latt(Ny, Nx, gamma)

	# Initial state of the system
	psi0 = [1, 1]

	# Define time points
	t = np.linspace(ti, tf, nt)
	tp = np.size(t)

	# Begin by creating a Group of Ode_on_charm objects. There is one object on each core
	# meaning, each core will take a chunck of energies to to solve the ODEs.
	workers = Group(OdeCharm)

	# Call Ode_on_charm method in each of the workers
	res = workers.charm_two_state_sys(ft, psi0, t, H, h_bar, energy, ret=True)

	ol = res.get()
	psi_out1 = ol[0]
	psi_out2 = ol[1]
	psi_out3 = ol[2]

	exit()

charm.start(main)
