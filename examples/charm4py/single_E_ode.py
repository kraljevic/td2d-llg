from td2d.dummy import ham_sq_latt
from td2d.qm import bra

import numpy as np
import cmath
from scipy.integrate import solve_ivp
#from scipy.integrate import odeint
import matplotlib.pyplot as plt

def lotkavolterra(t, z, a, b, c, d):
	x, y = z
	return a*x


def schodinger(t, y, h, h_bar):
	i1 = complex(0, 1)
	psi_1 = np.transpose(y)

	dpsidt_1 = h.dot(psi_1)/(h_bar)

	dpsidt = np.transpose(dpsidt_1)
	return dpsidt

# parameters of the problem
HBAR = 1
E = 0.5

Nx = 2
Ny = 1
gamma = 1

ti = 0
tf = 10
nt = 10

# Hamiltonian of the system
H = E*ham_sq_latt(Ny, Nx, gamma)
print(H)
# Initial state of the system
psi0 = [1, 1]

# Define time points
time = np.linspace(ti, tf, nt)

# Solve ODE
sol = solve_ivp(fun=lambda t, y: schodinger(t, y, H, HBAR), t_span=[0, 0.2], y0=psi0, t_eval=[0.1])
print(sol.t)
print(sol.y)

psi0 = sol.y.T[0, :]
sol2 = solve_ivp(fun=lambda t, y: schodinger(t, y, H, HBAR), t_span=[0, 0.2], y0=psi0, t_eval=[0.1])
print(sol2.t)
print(sol2.y)


# Plot the result
#plt.plot(sol.t,sol.y)
#plt.xlabel('time')
#plt.ylabel('y1(t)')
#plt.show()


