# THIS CODE SOLVES ODEs ON A PARAMETER 'E' IN SERIAL.
# THE SYSTEM OF EQUATIONS IS SOLVES A VECTOR OF SIZE 2 X 1
# --------------------------------------------------------
#	dY(t)/dt = -M(E)*Y(t)
#	Y(t) =  [y1(t); y2(t)]
#	If we choose Y(0) = [1 1], then the solution decays
# 	exponentially
# --------------------------------------------------------
# Utkarsh Bajpai, December 10, 2019
# --------------------------------------------------------

from td2d.dummy import ham_sq_latt, energy_uni_grid, two_state_sys
from td2d.qm import bra

import numpy as np
import cmath
import matplotlib.pyplot as plt

# parameters of the problem
HBAR = 1

Nx = 2
Ny = 1
gamma = 1

ti = 0
tf = 5
nt = 200

Ei = 2
Ef = 3
n_grid = 10
energy = energy_uni_grid(Ei, Ef, n_grid)

# System Hamiltonian
H = ham_sq_latt(Ny, Nx, gamma)

# Initial state of the system
psi0 = [1, 1]

# Define time points
t = np.linspace(ti, tf, nt)

# Function to evolve ODEs on the given energy grid
psi_out = two_state_sys(psi0, t, H, HBAR, energy)

# Plot the result
for a in range(0, n_grid):
	plt.plot(t,psi_out[:, 0, a],'-')

plt.xlabel('Time (units)')
plt.ylabel('Y(t,E)')
plt.xlim((ti, tf))
plt.show()


