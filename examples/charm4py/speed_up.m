% This code contains the results of speed ups achievd by parallelizing 
% and ODE solver. The values are directly stored as cores vs time taken

clc
clear all

cores = [1 2 3 4 5 6 7 8];
time = [44.11 43.55 17.83 13.30 10.43 9.41 8.19 7.42];
speedup = time(1)./time;

bar(cores, speedup)
F_set_labels(16,'No. of Cores', 'Speed Up', '')
xlim([0 8.5])
%ylim([0 1])
F_panel(16,'$\mathrm{T_1}$ = 44.1 s', 0.02, 0.9)
F_panel(16,'$\mathrm{N_{grid} = 5 \times 10^4}$', 0.02, 0.8)
