# Using charm4py to print helloworld from many workers(cores)

from charm4py import Chare, Group, charm, Future, Reducer


class Hello(Chare):

    def SayHi(self,pe):
        print('Hello World from element', self.thisIndex)

def main(args):
    # create Group of Hello objects (there will be one object on each core)
    hellos = Group(Hello)
    pe = Future()
    # call method 'SayHi' of all group members, wait for method to be invoked on all
    hellos.SayHi(pe,awaitable=True).get()
    print(pe.get())
    exit()

charm.start(main)
