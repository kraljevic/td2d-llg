from charm4py import charm, Chare, Group, Reducer, Future
from math import pi
import time
import numpy as np

class Worker(Chare):

    def work(self, n_steps, pi_future):
        h = 1.0 / n_steps
        s = 0.0
        for i in range(self.thisIndex, n_steps, charm.numPes()):
            x = h * (i + 0.5)
            s += 4.0 / (1.0 + x**2)
        # perform a reduction among members of the group, sending the result to the future
        self.reduce(pi_future, s * h, Reducer.sum)

def main(args):
    n_steps = 100
    if len(args) > 1:
        n_steps = int(args[1])
    mypi = Future()
    workers = Group(Worker)  # create one instance of Worker on every processor
    t0 = time.time()
    workers.work(n_steps, mypi)  # invoke 'work' method on every worker
    print('---------------------------------------------------------------------------------------------------------------------')
    print('Approximated value of pi is:', mypi.get(),  # 'get' blocks until result arrives
          'Error is', abs(mypi.get() - pi), 'Elapsed time=', time.time() - t0)
    print('---------------------------------------------------------------------------------------------------------------------')
    exit()

charm.start(main)
