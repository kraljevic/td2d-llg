import numpy as np
import matplotlib.pyplot as plt
from .constants import HBAR
from scipy.special import jv

l_coefs = [-0.006053802347392655 + -3.038275754843914j,
           -0.006053802347392655 + 3.038275754843914j,
           -0.02468590601498535 + -3.037421608394957j,
           -0.02468590601498535 + 3.037421608394957j,
           -0.05742116317008288 + -3.035697745246537j,
           -0.05742116317008288 + 3.035697745246537j,
           -0.107195209397165 + -3.032466518678096j,
           -0.107195209397165 + 3.032466518678096j,
           -0.1789896162708439 + -3.026338822240165j,
           -0.1789896162708439 + 3.026338822240165j,
           -0.2807296491956815 + -3.014346618183218j,
           -0.2807296491956815 + 3.014346618183218j,
           -0.4242256740265215 + -2.990307607636958j,
           -0.4242256740265215 + 2.990307607636958j,
           -0.6254623308453462 + -2.941866106826581j,
           -0.6254623308453462 + 2.941866106826581j,
           -0.9027061725736346 + -2.845664676391065j,
           -0.9027061725736346 + 2.845664676391065j,
           -1.269462293739291 + -2.660994507426169j,
           -1.269462293739291 + 2.660994507426169j,
           -1.717843967339638 + -2.32653278822588j,
           -1.717843967339638 + 2.32653278822588j,
           -2.19232187017386 + -1.773829176088811j,
           -2.19232187017386 + 1.773829176088811j,
           -2.724142990301394 + 0j,
           -2.574686825647225 + -0.9740465255445164j,
           -2.574686825647225 + 0.9740465255445164j]

are = [0.0003692346353700416, 0.0003692346349470505,
       0.001567986458321391, 0.001567986462027889,
       0.003914693095641886, 0.003914693072758444,
       0.00811536265429893, 0.00811536276418392,
       0.01561321291116852, 0.01561321251158325,
       0.02923515379929463, 0.0292351548977905,
       0.05410550696425286, 0.05410550464715179,
       0.09792998789922573, 0.09792999168673264,
       0.1666392975922213, 0.1666392928888215,
       0.2440571515274646, 0.2440571555226544,
       0.2448524466494443, 0.2448524453666976,
       0.004675187455483138, 0.004675185548130005,
       -0.7723764233806999, -0.4848870104687091,
       -0.484887007849131]

aim = [0.0003225450704719809, -0.0003225450675837658,
       0.001332886373795132, -0.001332886393127654,
       0.003164712333853498, -0.003164712244925103,
       0.006044850612428537, -0.006044850947931249,
       0.01021977155206156, -0.01021977052812919,
       0.01556654820087363, -0.01556655070135733,
       0.02022510155959408, -0.02022509662741808,
       0.01680884930663684, -0.01680885734567935,
       -0.01558532095891143, 0.01558533216501644,
       -0.1195514102072628, 0.1195513961007013,
       -0.3356121581018785, 0.3356121754350826,
       -0.5800213146372751, 0.5800212927596275,
       -1.470779369318321e-08, -0.5294330372672091,
       0.5294330642658172]

a_coefs = np.array(are) + 1j*np.array(aim)
l_coefs = np.array(l_coefs)


class MemoryFitter:

    def __init__(self, device):
        self.dev = device
        self.m = 27
        self.lik, self.aik = self.fit_channels()

    def fit_channels(self):

        m = self.m
        nch = len(self.dev.xi_cent)
        lik = np.zeros((nch, m), dtype=complex)
        aik = np.zeros((nch, m), dtype=complex)

        for index, channel in enumerate(self.dev.xi_cent):
            amps, lam = fit(channel)
            lik[index, :] = lam
            aik[index, :] = amps
            tf, kf = get_kernel(channel, dt=0.01, tmax=60)
            t, k = sample(amps, lam, tmax=60)
            filename = 'channel_%02d.png' % index
            print(filename)
            self.plot_fit(tf, kf, t, k, filename)
        return lik, aik

    def plot_fit(self, tfit, kfit, t, k, filename):
        fig = plt.figure(figsize=(3.375, 2.0))
        ax = fig.add_axes([0.17, 0.20, 0.80, 0.70])
        ax.set_xlim(-0.5, 60)
        ax.set_xlabel('Time (fs)')
        ax.set_ylabel(r'$K_i(t)$', labelpad=3)
        ax.plot(t, k.real, color='#BEBEBE', lw=1.7,
                label='original')
        ax.plot(tfit, kfit.real, c='C3', lw=0.7, label='fit')
        ax.legend()
        plt.savefig(filename, dpi=400)


def sample(amps, periods, dt=0.01, tmax=20):

    times = np.arange(0, tmax + dt, dt)

    func = 0
    for a, l in zip(amps, periods):
        func += (a * np.exp(l*times))
    return times, func


def fit(energy):

    lij = l_coefs + (-1j * energy / HBAR)
    return a_coefs, lij


def get_kernel(energy, dt=0.1, tmax=10., gamma=1.):
    times = np.arange(0, tmax + dt, dt)
    coef = -gamma**3/HBAR + 0j
    ctimes = 2*gamma*times/HBAR
    exp_term = np.exp(-1j*times * energy / HBAR)
    coef = coef * exp_term
    kernel = np.zeros(len(times), dtype=complex)
    kernel = coef * (jv(1, ctimes) + jv(3, ctimes))
    return times, kernel
