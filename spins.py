import numpy as np
from numpy.linalg import norm as lanorm
from .constants import MU_BOHR, GAMMA, KB
from .constants import SIG_X, SIG_Y, SIG_Z
import numpy.random as rand
from td2d.plot.plot_manager import plot_spins


class Spins:

    def __init__(self, kwant_system,
                 spin_config=(0, 0, 1),
                 time=0.0,
                 temperature=0.0,
                 seed=1,
                 bf=0.0,
                 demag=0.0,
                 ani=0.,
                 dmi=0.,
                 dmi_type='rij',
                 jsd_to_llg=0.0,
                 jsd_to_negf=0.0,
                 jexc=0.,
                 bf_vec=(0., 0., 1),
                 demag_vec=(0., 1., 0),
                 ani_vec=(0, 0, 1),
                 g_lambda=0.0,
                 dt=0.1,
                 config=None,
                 shape=None,
                 bf_func=None,
                 ani_func=None,
                 dmi_func=None,
                 demag_func=None,
                 jsd_to_llg_func=None,
                 jsd_to_negf_func=None,
                 jexc_func=None,
                 h_fit_func=None,
                 h_fit_args=None,
                 h_fit_kwargs=None,
                 fit_scale_func=None,
                 ):
        """Construct classical magnetic moments 
        
        Parameters
        ----------
        dmi_type : string (optional)
            Type of dmi interaction, possible types
            'rij', 'rijx', 'rijz'. Default is 'rij'
        fit_scale_func : single argument function
            Uses a single argument (time) and returns a 
            single float which will multiply the fitted torque
            in LLG. This function is used to switch the
            fitted torque on and off, in order to mimic the
            current pulses.
        """
        self.kwant_sys = kwant_system
        self.spin_config = spin_config
        self.time = time
        self.bf = bf
        self.bf_vec = bf_vec
        self.demag = demag
        self.demag_vec = demag_vec
        self.ani = ani
        self.dmi = dmi
        self.dmi_type = dmi_type
        self.ani_vec = ani_vec
        self.jsd_to_llg = jsd_to_llg
        self.jsd_to_negf = jsd_to_negf
        self.jexc = jexc
        self.g_lambda = g_lambda
        self.dt = dt
        self.h_fit_func = h_fit_func
        self.h_fit_args = h_fit_args
        self.h_fit_kwargs = h_fit_kwargs

        rand.seed(seed)

        self.temp = temperature
        self.D = g_lambda / (1 + g_lambda**2) * KB * temperature
        self.D = self.D / (MU_BOHR * GAMMA)
        self.D_const = np.sqrt(2 * self.D/self.dt)

        self.config = (self.default_config
                       if not config else config)
        self.shape = default_shape if not shape else shape
        self.bf_func = (self.default_bf
                        if not bf_func else bf_func)
        self.ani_func = (self.default_ani
                         if not ani_func else ani_func)
        self.dmi_func = (self.default_dmi
                         if not dmi_func else dmi_func)
        self.demag_func = (self.default_demag
                           if not demag_func else demag_func)
        self.jsd_to_llg_func = (self.default_jsd_to_llg
                                if not jsd_to_llg_func else
                                jsd_to_llg_func)
        self.jsd_to_negf_func = (self.default_jsd_to_negf
                                 if not jsd_to_negf_func else
                                 jsd_to_negf_func)
        self.fit_scale_func = (self.default_fit_scale if not
                               fit_scale_func else
                               fit_scale_func)

        self.jexc_func = (self.default_jexc_func
                          if not jexc_func else jexc_func)
        self.s = None
        self.pos_sites = None
        self.pos_spins = None
        self.ind = None

        self.generate()

        return

    def default_config(self, site):
        spin_norm = np.linalg.norm(self.spin_config)
        if spin_norm == 0.0:
            raise ValueError('Default spins can not be zero'
                             ' vectors')

        return np.array(self.spin_config) / spin_norm
    
    def default_fit_scale(self, time, site, **kwargs):
        return 1.0

    def default_bf(self, time, site, **kwargs):
        bf_norm = np.linalg.norm(np.array(self.bf_vec))
        if bf_norm == 0.0:
            raise ValueError('Default field vector can not '
                             ' be zero')
        bf_vec = self.bf_vec / bf_norm
        return self.bf * bf_vec

    def default_ani(self, time, site, **kwargs):
        ani_norm = np.linalg.norm(np.array(self.ani_vec))
        if ani_norm == 0.0:
            raise ValueError('Anisotropy vector can not be zero')
        ani_vec = self.ani_vec / ani_norm
        return self.ani * ani_vec

    def default_demag(self, time, site, **kwargs):
        demag_norm = np.linalg.norm(np.array(self.demag_vec))
        if demag_norm == 0.0:
            raise ValueError('Demagnetization vector can '
                             'not be zero')
        demag_vec = self.demag_vec / demag_norm
        return self.demag * demag_vec

    def default_jsd_to_llg(self, time, site, **kwargs):
        return self.jsd_to_llg

    def default_dmi(self, time, site, neigh_site, **kwargs):
        return self.dmi

    def default_jsd_to_negf(self, time, site, **kwargs):
        return self.jsd_to_negf

    def default_jexc_func(self, time, site, n_site, **kwargs):
        return self.jexc

    def generate(self):
        self.s = []
        self.pos_spins = []
        self.pos_sites = []
        self.ind = []
        for site in self.kwant_sys.sites:
            self.pos_sites.append(site.pos)

            if self.shape(site.pos):
                spin = np.array(self.config(site))
                spin /= np.linalg.norm(spin)
                self.s.append(spin)
                ind = self.kwant_sys.id_by_site[site]
                self.ind.append(ind)
                self.pos_spins.append(site.pos)
        self.pos_sites = np.array(self.pos_sites)
        self.pos_spins = np.array(self.pos_spins)
        self.s = np.array(self.s)
        self.ind = np.array(self.ind)

    def generate_heff(self, cspins, espins, time, **kwargs):

        ks = self.kwant_sys

        heff_vals = []
        hth = []
        for site_ind, espin, cspin in zip(self.ind, espins,
                                          cspins):

            heff = np.array([0., 0., 0.], dtype=float)
            site = ks.sites[site_ind]

            # Generate magnetic field terms
            bf_site = self.bf_func(time, site, **kwargs)
            bf_site = np.array(bf_site)
            heff = heff + (MU_BOHR * bf_site)

            # Generate Jsd terms
            jsd = self.jsd_to_llg_func(time, site, **kwargs)
            heff = heff + np.array(jsd * espin)

            # Generate anisotropy terms
            ani_vec = self.ani_func(time, site, **kwargs)
            ani_vec = np.array(ani_vec)
            ani_norm = np.linalg.norm(ani_vec)
            if ani_norm == 0.0:
                ani_norm = 1.0
            e_ani = ani_vec / ani_norm

            hval = 2 * ani_vec * np.dot(cspin, e_ani)
            heff = heff + np.array(hval)

            # Generate demagnetization field
            # Same as anisotropy but with negative sign
            demag_vec = self.demag_func(time, site, **kwargs)
            demag_vec = np.array(demag_vec)
            demag_norm = np.linalg.norm(demag_vec)
            if demag_norm == 0.0:
                demag_norm = 1.0
            e_demag = demag_vec / demag_norm

            hval = -2 * demag_vec * np.dot(cspin, e_demag)
            heff = heff + np.array(hval)

            # Generate exchange and DMI interactions
            for negh_ind in ks.graph.out_neighbors(site_ind):
                negh_site = ks.sites[negh_ind]
                if self.shape(negh_site.pos):
                    ng_ind = np.argwhere(self.ind == negh_ind)
                    ng_spin = self.s[ng_ind][0, 0, :]
                    ng_spin = np.array(ng_spin)

                    # Add exchange interaction
                    jxc = self.jexc_func(time, site,
                                         negh_site, **kwargs)
                    hval = ng_spin * jxc
                    heff = heff + hval

                    # Add DMI interaction
                    if self.dmi_type == 'rij':
                        dvec = (np.array(negh_site.pos)
                                - np.array(site.pos))
                    elif self.dmi_type == 'rijx':
                        dvec = np.cross((np.array(negh_site.pos)
                                         - np.array(site.pos)),
                                        np.array([1, 0, 0]))
                    elif self.dmi_type == 'rijz':
                        dvec = np.cross((np.array(negh_site.pos)
                                         - np.array(site.pos)),
                                        np.array([0, 0, 1]))
                    else:
                        raise ValueError('Unknow DMI type')

                    if type(dvec) == float:
                        if dvec == 0:
                            dvec = np.array([0, 0, 0])

                    dmi_cons = self.dmi_func(time, site,
                                             negh_site, **kwargs)
                    hdmi = dmi_cons * np.cross(dvec, ng_spin)

                    heff = heff + hdmi

            # Generate the field that comes from a fitted
            # function
            hfit = 0 * heff
            if self.h_fit_func:
                if self.h_fit_args and self.h_fit_kwargs:
                    hfit = self.h_fit_func(cspin,
                                           *self.h_fit_args,
                                           **self.h_fit_kwargs)
                elif self.h_fit_args:
                    hfit = self.h_fit_func(cspin,
                                           *self.h_fit_args)
                elif self.h_fit_kwargs:
                    hfit = self.h_fit_func(cspin,
                                           **self.h_fit_kwargs)
                else:
                    hfit = self.h_fit_func(cspin)
                
                fscale = self.fit_scale_func(time, site,
                                             **kwargs)
                hfit = hfit * fscale

            heff = heff / MU_BOHR
            heff = heff - hfit
            heff_vals.append(heff)

            # Generate thermal noise
            x1 = self.D_const * (rand.randint(0, 2)*2 - 1)
            x2 = self.D_const * (rand.randint(0, 2)*2 - 1)
            x3 = self.D_const * (rand.randint(0, 2)*2 - 1)
            zeta = [x1, x2, x3]
            hth.append(zeta)

        return np.array(heff_vals) + np.array(hth)

    def llg4(self, espins, time, **kwargs):
        dt = self.dt
        ds1 = self.dheun(self.s, espins, time)*dt

        time_t = time + dt/2
        spins_t = norm_spins(self.s + ds1/2)

        ds2 = self.dheun(spins_t, espins, time_t)*dt
        time_t = time + dt/2
        spins_t = norm_spins(self.s + ds2/2)

        ds3 = self.dheun(spins_t, espins, time_t)*dt

        time_t = time + dt
        spins_t = norm_spins(self.s + ds3)
        ds4 = self.dheun(spins_t, espins, time_t)*dt
        ds = (ds1 + 2*ds2 + 2*ds3 + ds4)/6.
        self.s = norm_spins(self.s + ds)

    def llg(self, espins, time, **kwargs):

        ds_pred, spins_pred = self.heun(self.s, espins,
                                        time, self.dt,
                                        **kwargs)

        ds_corr, spins_corr = self.heun(spins_pred, espins,
                                        time, self.dt,
                                        **kwargs)

        spins = self.s + 0.5*self.dt*(ds_corr + ds_pred)
        self.s = norm_spins(spins)

    def dheun(self, cspins, espins, time):
        hef = self.generate_heff(cspins, espins, time)
        sh = np.cross(cspins, hef)
        shh = np.cross(cspins, sh)
        factor = -GAMMA / (1.0 + self.g_lambda**2)
        ds = factor * (sh + self.g_lambda*shh)
        return ds

    def heun(self, cspins, espins, time, dt, **kwargs):
        hef = self.generate_heff(cspins, espins, time)

        sh = np.cross(cspins, hef)
        shh = np.cross(cspins, sh)

        factor = -GAMMA / (1.0 + self.g_lambda**2)
        ds = factor * (sh + self.g_lambda*shh)
        spins_new = cspins + ds*dt
        spins_new = norm_spins(spins_new)
        return ds, spins_new

    def plot(self, plot_type='3d', ax=None, filename=None,
             time=None, **kwargs):
        plot_spins(self, plot_type=plot_type, filename=filename,
                   ax=ax, time=time, **kwargs)

    def onsite(self, time):

        onsites = []
        for site, moment in zip(self.kwant_sys.sites, self.s):
            jsd = self.jsd_to_negf_func(time, site)
            mx, my, mz = moment
            ons = -jsd*(mx*SIG_X + my*SIG_Y + mz*SIG_Z)
            onsites.append(ons)

        return np.array(onsites)


def norm_spins(spins):
    norm_spins = lanorm(spins, axis=1)[:, np.newaxis]
    nspins = spins/norm_spins
    return nspins


def default_shape(pos):
    return True
