from .device import Device
import numpy as np
from .constants import HBAR
from .constants import KB
from .kwant_wrapper import lead_and_channel_finder as lc_find
from .boundstates import Boundstates
import time



def fermi_fn(x):
    return 1. / (1 + np.exp(x))


class RK4Numpy:

    def __init__(self, device, emin=-2., emax=2.,
                 estep=0.01, temp_k=100.0, efermi=0.0,
                 m_max=100, j_couple=1.0, k_couple=1.0,
                 gamma=1.0, weight=0.01,
                 bs_low=-3, bs_upr=3, bs_de=0.01,
                 bs_tol=10**(-10), bs_iter_max=100,
                 bs_adaptive=False,
                 verbose=False):
        '''A simple propagation solver for TD-NEGF based on
        RK4 method

        Parameters
        ----------
        device : an instance of the Device class
            The device whose state we want to evolve in time.
            The solver needs this so it can extract the
            system hamiltonian and other usefull parameters.
        emin : float (optional)
            The bottom of the band. The solver needs this
            so it can compute the energy integral for the
            rho matrices. Default value -2.1.
        emax : float (optional)
            The top of the band. The solver needs this
            so it can compute the energy integral for the
            rho matrices. Default value 2.1.
        estep : float (optional)
            The energy step used in integration of the
            rho matrices. Default value 0.01
        temp_k : float (optional)
            The temperature of the system in Kelvins (K).
            Default value 100.
        efermi : float (optional)
            The Fermi energy at which to perform the time
            propagation. Default value 0.0.
        m_max : integer (optional)
            The dimension of omega vectors. It determines
            the maximum time interval where the solver
            produces acceptable results.
        j_couple : float (optional)
            Coupling strength J in the equation for the
            propagation of omega matrices (see the theory
            part of the manual). Default value 1.
        k_couple : float (optional)
            Coupling strength K in the equation for the
            propagation of omega matrices (see the theory
            part of the manual). Default value 1.
        gamma : float (optional)
            Hopping integral from the system to the leads.
            Leads are always assumed to be square lattice
            with uniform hopping to the system.
        weight : float (optional)
            Weight of the energy points depending upon
            the integration scheme used to do the energy
            integral. Default value is 0.01.
        bs_low: float (optional)
            Lower limit of energy till where the solver
            will look for bound states.
        bs_upr: float (optional)
            Upper limit of energy till where the solver
            will look for bound states.
        bs_de: float (optional)
            Energy step dE which is used to find bound states.
            This is only used if you do not use and adaptive
            grid to find them.
        bs_tol: float (optional)
            Tolerance of convergence when finding bound states.
        bs_iter_max: int (optional)
            Sets the maximum number of steps used in
            self-consisten scheme to find bound states.
        bs_adaptive: bool (optional)
            Lets you decide whether or not use an adaptive grid
            to find bound states. Default value is False
        verbose : boolean (optional)
            Print computation times necessary for profiling.
        '''

        assert isinstance(device, Device), (
            'The suplied object under device argument is '
            'not and instance of the Device class.')

        self.emin = emin
        self.emax = emax
        self.estep = estep
        self.egrid = np.arange(emin, emax+estep, estep)
        self.ne_grid = len(self.egrid)
        self.temp_k = temp_k
        self.efermi = efermi
        self.m_max = m_max
        self.j_couple = j_couple
        self.k_couple = k_couple
        self.dim_h = device.dim_h
        self.nc_tot = device.nc_tot
        self.gamma = gamma
        self.device = device
        self.verbose = verbose
        self.w = weight * np.ones(self.ne_grid)
        self.bs_low =  bs_low
        self.bs_upr = bs_upr
        self.bs_de = bs_de
        self.bs_tol = bs_tol
        self.bs_iter_max = bs_iter_max
        self.bs_adaptive = bs_adaptive

        self.psi = np.zeros((self.dim_h, self.nc_tot,
                             self.ne_grid), dtype=complex)

        self.omega = np.zeros((self.m_max, self.nc_tot,
                               self.nc_tot, self.ne_grid),
                              dtype=complex)

        # Initiate bound states
        device_bs = Boundstates(self.device,
                                bs_low=self.bs_low,
                                bs_upr=self.bs_upr,
                                bs_de=self.bs_de,
                                bs_tol=self.bs_tol,
                                bs_iter_max=self.bs_iter_max,
                                bs_adaptive=self.bs_adaptive,
                                gamma=self.gamma)
        bs_peaks, bs_states, bs_pop = device_bs.exact_bound_state_peaks()

        self.bs_peaks = bs_peaks
        self.bs_states = bs_states
        self.bs_pop = bs_pop
        if self.bs_peaks == []:
            self.bs_exist = False
        else:
            self.bs_exist = True

    @property
    def dmat(self):
        """ D-matrix used in the EOM of Omega"""
        dmat = np.zeros((self.m_max, self.m_max))
        for a in range(0, self.m_max-1):
            dmat[a, a+1] = -1
            dmat[a+1, a] = 1

        dmat[0, 1] = -2
        return dmat

    def evolve_psi(self, time, psi, omega):
        """
        Function that returns the RHS of psi EOM.
        """

        # Generate the Hamiltonian of your system
        ham = self.device.get_h(time)

        # dimension of the Hamiltonian
        dim_h = self.device.dim_h
        xic = np.array(self.device.xi)

        # Contruucting the RHS
        t1 = np.einsum('ij,jkl', ham, psi, optimize='greedy')

        t2 = -np.einsum('ij,jkm,ml', np.identity(dim_h), psi,
                        np.diag(self.egrid),
                        optimize='greedy')

        t3 = self.j_couple*np.einsum('ib,kbl', xic,
                                     omega[0, :, :, :] +
                                     omega[2, :, :, :],
                                     optimize='greedy')

        t4 = self.k_couple*np.repeat(xic[:, :, np.newaxis],
                                     self.ne_grid, axis=2)

        psi_rhs = t1 + t2 + t3 + t4

        psi_rhs *= (-1j/HBAR)

        return psi_rhs

    def save_npz(self, filename):
        """
        Saves psi and omega vectors
        """
        np.savez(filename, psi=self.psi, omega=self.omega)

    def load_npz(self, filename):
        """
        Loads psi and omega vectors
        """
        data = np.load(filename)
        self.omega = data['omega']
        self.psi = data['psi']

    def evolve_omg(self, psi, omega):
        """
        Function that returns the RHS of omega EOM.
        """
        time1 = time.clock()
        t1 = np.array(omega)
        for index, xi_cent in enumerate(self.device.xi_cent):
            t1[:, :, index, :] *= xi_cent
        time2 = time.clock()
        if self.verbose:
            print('Term 1:', time2 - time1)

        time1 = time.clock()
        t2 = np.array(omega)
        for index, en in enumerate(self.egrid):
            t2[:, :, :, index] *= -en
        time2 = time.clock()
        if self.verbose:
            print('Term 2:', time2 - time1)

        time1 = time.clock()
        t3 = 0*omega
        for index in range(len(t3)):
            if index == 0:
                t3[index] = -2*omega[1]
            elif index == (len(t3)-1):
                t3[index] = omega[index-1]
            else:
                t3[index] = omega[index-1] - omega[index+1]
        t3 *= (1j*self.gamma)
        time2 = time.clock()
        if self.verbose:
            print('Term 3:', time2 - time1)

        time1 = time.clock()
        omg_rhs = t1 + t2 + t3
        time2 = time.clock()
        if self.verbose:
            print('Sum time', time2 - time1)

        t4 = np.einsum('sk,sjl', np.conj(self.device.xi), psi)
        t4 *= (self.j_couple * self.gamma**2)

        omg_rhs[0, :, :, :] += t4
        omg_rhs *= (-1j/HBAR)

        return omg_rhs

    def propagate(self, time, dt):
        """
        Fuction that returns the new psi, omg and bs_states vectors
        after time-evolution using a simple hand written RK4.

        Parameters
        ----------
        time : float
            Used to create parametric Hamiltonian
        dt : float
            The time-step we are working with.

        Notes
        -----
        The function updates the internal state of
        psi, omega and bound states vectors using Runge-Kutta 4
        time-stepping scheme.
        """

        # Apply Runge-Kutta step 1
        psi1 = self.evolve_psi(time, self.psi, self.omega)*dt
        omega1 = self.evolve_omg(self.psi, self.omega)*dt
        if self.bs_exist:
            bs_states1 = self.evolve_bs(time, self.bs_states)*dt

        # Apply Runge-Kutta step 2
        time_t = time + dt/2.
        psi_t = self.psi + psi1/2.
        omega_t = self.omega + omega1/2.
        if self.bs_exist:
            bs_states_t = self.bs_states + bs_states1/2.

        psi2 = self.evolve_psi(time_t, psi_t, omega_t)*dt
        omega2 = self.evolve_omg(psi_t, omega_t)*dt
        if self.bs_exist:
            bs_states2 = self.evolve_bs(time_t, bs_states_t)*dt

        # Apply Runge-Kutta step 3
        time_t = time + dt/2.
        psi_t = self.psi + psi2/2.
        omega_t = self.omega + omega2/2.
        if self.bs_exist:
            bs_states_t = self.bs_states + bs_states2/2.

        psi3 = self.evolve_psi(time_t, psi_t, omega_t)*dt
        omega3 = self.evolve_omg(psi_t, omega_t)*dt
        if self.bs_exist:
            bs_states3 = self.evolve_bs(time_t, bs_states_t)*dt

        # Apply Runge-Kutta step 4
        time_t = time + dt
        psi_t = self.psi + psi3
        omega_t = self.omega + omega3
        if self.bs_exist:
            bs_states_t = self.bs_states + bs_states3

        psi4 = self.evolve_psi(time_t, psi_t, omega_t)*dt
        omega4 = self.evolve_omg(psi_t, omega_t)*dt
        if self.bs_exist:
            bs_states4 = self.evolve_bs(time_t, bs_states_t)*dt

        # Final values at new time step
        self.psi += (psi1 + 2*psi2 + 2*psi3 + psi4)/6.
        self.omega += ((omega1 + 2*omega2 +
                        2*omega3 + omega4)/6.)
        if self.bs_exist:
            self.bs_states += ((bs_states1 + 2*bs_states2 +
                                2*bs_states3 + bs_states4)/6.)

        return

    def semi_circle(self, center):
        """Return a semi-circle representing diagonalised
        selfenergy (an energy of a eigen-channel)
        """

        semi = np.zeros(self.ne_grid)
        for index, en in enumerate(self.egrid):
            up_limit = en > (center + 2*self.gamma)
            dw_limit = en < (center - 2*self.gamma)

            if up_limit or dw_limit:
                semi[index] = 0.
            else:
                val = 4.*self.gamma**2 - (en - center)**2
                semi[index] = np.sqrt(val)
        return semi

    def channel_weight(self, time):
        """
        Returns the weight W(E) which multiplies to
        |psi><psi| in energy integration.

        Returns
        -------
        channel_fn: 2D numpy array
            Contains the 2D array weight of the all channels
            as a function of energy. The first index runs on
            channels and the second one on the energy grid.
        """

        # Allocate space for the output channel weight
        ch_fn = np.zeros((self.nc_tot, self.ne_grid))

        for ch in range(self.nc_tot):

            args = self.device.get_args(time)
            mu_leads = self.device.get_mu(time)
            sys = self.device.sys
            # Find the lead and respective channel number
            ld_no, ld_ch_no = lc_find(ch, sys, args)

            # Find the chemical potential of the lead the
            # "ch" channel belongs to
            mu = mu_leads[ld_no]

            # Now populate the output function with it
            x = (self.egrid - mu) / (KB * self.temp_k)
            fermi_term = fermi_fn(x)

            cent = self.device.xi_cent[ch]
            circle_term = self.semi_circle(cent)

            ch_fn[ch, :] = fermi_term * circle_term

        return ch_fn

    def rho(self, time):
        """
        Function to evaluate the nonequilibrium
        density matrix
        """

        psi_conj = np.conj(self.psi)

        # Create the function which multiplies to the
        # psi functions

        ch_fn = self.channel_weight(time)

        # now calculate rho using scipy einsum method
        # (rho_ij, c=channel, g=grid)
        rho = np.einsum('icg,jcg,cg,g', self.psi, psi_conj,
                        ch_fn, self.w, optimize='greedy')

        rho = rho * (1./(2*np.pi))

        if self.bs_exist:
            rho_bs = self.rho_bs(time)
            rho = rho + 0*rho_bs

        return rho


    def evolve_bs(self, time, bound_states):
        """
        Function that returns the RHS of boundstate EOM.
        """
        ham = self.device.get_h(time)
        se = self.device.selfenergy(-np.sqrt(8), time)
        ham = ham + se
        bs_rhs = np.einsum('ij,jk', ham, bound_states, optimize='greedy')
        bs_rhs *= (-1j/HBAR)

        return bs_rhs

    def rho_bs(self, time):
        """
        Function that returns the density matrix of the bound states.
        """
        bs_states_conj = np.conj(self.bs_states)
        # now calculate rho_bs using scipy einsum method
        # (rho_ij, b=bound_state_number, f(E_bs)*|bs><bs|)
        # (NOTE: The above thing is not correct, the correct population
        # needs to be decided)
        beta = 1/(KB*self.temp_k)
        population = fermi_fn(beta*(self.bs_peaks - self.efermi))
        rho_bs = np.einsum('ib,jb,b',
                           self.bs_states,
                           bs_states_conj,
                           population,
                           optimize='greedy')
        print(self.efermi)

        return rho_bs
