from td2d.ozaki import Ozaki
import numpy as np
import matplotlib.pyplot as plt

n_poles = 150
poles, residue = Ozaki.poles_and_residue(n_poles)
poles1 = np.sort(np.imag(poles))
poles2 = poles1[1: len(poles)] - poles1[0: len(poles)-1]

fig, ax = plt.subplots()
ax.plot(np.log10(poles2), ls='none', marker='.')
fig.savefig("poles.pdf")
