from td2d.ozaki import Ozaki
from td2d.kwant_systems import make_ribbon
from td2d.kwant_wrapper import get_ham as get_h

import numpy as np

# Define system
sys = make_ribbon(length=2, width=1, tsoc=0, plot_sys=False,spin=False)
sys_args = ([0.1, 0.1])

# parameters
n_poles = 100
efermi = 0
temp = 100

# find the ozaki density matrix
rho = Ozaki.rho(sys,  sys_args, n_poles, efermi, temp)

print(np.real(rho))






