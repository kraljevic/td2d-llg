import numpy as np
from td2d.initiate import memory_kernel
from td2d.constants import HBAR
from scipy import special

mem_t = 40
mem_dt = 0.1
gamma = 1
dim_h = 5
nc_tot = 3
n_grid = 20

time = np.arange(0, mem_t+mem_dt, mem_dt)
time_grd = len(time)

xi = np.random.rand(dim_h,nc_tot)
xi_cent = np.random.rand(nc_tot)
energy = np.random.rand(n_grid)


kernel_expected = np.zeros((dim_h, dim_h, n_grid, time_grd), dtype=complex)
tc = 0
for t in time:
    for ch in range(nc_tot):
        for k in range(n_grid):
            kernel_expected[:, :, k, tc] = kernel_expected[:, :, k,  tc] \
                             + np.outer(xi[:, ch], np.conj(xi[:, ch]))\
                             *np.exp(-1j*(xi_cent[ch]-energy[k])*t/HBAR)\
                             *(special.jn(0, 2*t*gamma/HBAR)
                             + special.jn(2, 2*t*gamma/HBAR))*(gamma**2)/HBAR

    tc = tc+1

kernel_fn = memory_kernel(mem_t, mem_dt, gamma, xi, xi_cent, energy)

print('--------------------------------------')
print('Checking difference between results ')
print('--------------------------------------')
res = np.allclose(kernel_fn, kernel_expected)
if res:
    print('Passed : No difference')
else:
    print('Failed : Difference')


