import numpy as np
from td2d.measure import Measure

rho = np.array([[0.6, 1, 0, 0],
				[1, 0.5, 0, 0],
				[0, 0, 0.3, 3+2j],
				[0, 0, 3-2j, 0.4]])

spd_fn = Measure.sp_density(rho, where='all')
spd_exp = np.array([2, 0, 0.1, 6, 4, -0.1])

print('-----------------')
print('Expected Result ')
print('-----------------')
print(spd_exp)
print('---------------------')
print('Result from function')
print('---------------------')
print(spd_fn)
print('-------------------')
print('test results')
print('-------------------')
res = np.allclose(spd_fn, spd_exp)
if res:
    print('Passed')
else:
    print('Failed')


