import numpy as np
from td2d.measure import Measure

dim_h  = 2
nc_tot = 4
n_grid = 3
temp = 100


psi = np.zeros((dim_h, nc_tot, n_grid))
psi[1, :, :] = 1.0

energy = np.array([0, 0, 0])
weight = np.array([1, 1, 1])
mu_leads = np.array([0, 0])
xi_cent = np.array([0, 0, 0, 0])
gamma = 1
eom_obj_new = [psi, 'nothing']

rho = Measure.rho(eom_obj_new, energy, weight, mu_leads, xi_cent, gamma, temp)

print(rho)




