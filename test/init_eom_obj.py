# Script to test the function "init_eom_obj.py" in the initiate module
import numpy as np
from td2d.initiate import init_eom_obj

# Decide a solver
solver = 'scipy_solver'
solver_init_args = [4]


# Arguments
energy = np.linspace(0, 10, 11)
dim_h = 2
nc_tot = 3

# Construct the eom objects at t=0
print('------------------------')
obj = init_eom_obj(energy,
                 dim_h,
                 nc_tot,
                 solver,
                 solver_init_args,
                 verbose=True)

print('------------------------')

# Print necessary information
print('   PARAMETERS  ')
print('------------------------')
print('Chosen Solver: ', solver)
print('Solver Arguments : ', solver_init_args)
print('energy grid : ', energy)
print('dim_h : ', dim_h)
print('nc_tot : ', nc_tot)


print(type(obj))

print(type(obj[0]))
print(np.shape(obj[0]))

print(type(obj[1]))
print(np.shape(obj[1]))

# Bonus1 (Try the Kernel method for once)
# Bonus2 (Try choosing a wrong solver )
# Good luck for both
if solver == 'kernel_solver': print(obj[1])
