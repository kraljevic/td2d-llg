function DL = expand_DL(DL_m,dimc)
DL = zeros(dimc);
s  = length(DL_m);
DL(1:s,1:s) = DL_m(1:s,1:s);
end