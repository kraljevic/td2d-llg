function Hjsd  =F_ham_jsd(Nx,Ny,jsd,ort);
% Dimension Of hilbert space
dim = Nx*Ny*2;
Hjsd = zeros(dim,dim);

% Various Available orientations
switch ort 
    case 1
        for a=1:dim
            Hjsd(a,a) = -jsd*(-1)^(a-1);
        end   
    otherwise
    
end


end
