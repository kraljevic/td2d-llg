function psi_out = F_psi_evolve(psi_inp,omg_inp,a,e_num,E,t)
%-------------------------
% Load required variables
%-------------------------
global H
global dim
global hbar
global P
global xi
global Nc_tot
global J
global K
global approach

if approach==1
term1 = H*psi_inp;
term3 = exp(-1i*E*t/hbar)*xi(:,a);
end

if approach==2
term1 = (H-eye(dim)*E)*psi_inp;
term3 = xi(:,a);
end


term2 = zeros(dim,1);
for b=1:Nc_tot
    term2 = term2 + (P*omg_inp(:,b))*xi(:,b);
end

psi_out = (term1 + J*term2 + K*term3)/(1i*hbar);

end
