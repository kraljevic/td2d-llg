function F_run_test_calculations()
%--------------------------
% Call required variables
%--------------------------
global ti
global tf
global dt
global gamma
global hbar
global E_test

% Analytical Solution to Psi vectors
time = (ti:dt:tf)';
psi1 = exp(1i*E_test*time/hbar).*cos(gamma*time/hbar);
psi2 = 1i*exp(1i*E_test*time/hbar).*sin(gamma*time/hbar);
psi = [psi1, psi2];
dlmwrite('./test_functions/psi_test_an.dat',[time, psi]);

% Analytical solution to Omg vectors
global circle_cent;
global C1_omg_test;
global C2_omg_test;
global dim_omg;

lb = circle_cent(1,C2_omg_test);
omg = zeros(length(time),dim_omg);
for c=1:dim_omg
    omg(:,c) = exp(-1i*(lb-E_test)*time/hbar).*exp(-gamma*time/hbar);
end
dlmwrite('./test_functions/omg_test_an.dat',[time, omg])
