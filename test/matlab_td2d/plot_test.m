clc
clear all
close all

%-----------------
% Load Variables
%----------------
F_set_global_variables();

F_set_memory();

global E_test;
global C_psi_test;
global C1_omg_test;
global C2_omg_test;
global ti
global tf
global dim_omg

%----------------------
% Load files into data
%----------------------
data = dlmread('./test_functions/psi_test_an.dat');
time = data(:,1);
psi1_an = data(:,2);
psi2_an = data(:,3);

data = dlmread('./test_functions/omg_test_an.dat');
omg_an = data(:,2:end);

data = dlmread('./test_functions/psi_test.dat');
time = data(:,1);
psi1 = data(:,2);
psi2 = data(:,3);

data = dlmread('./test_functions/omg_test.dat');
omg = data(:,2:end);

%----------------------
% Plot the Psi Vectors
%----------------------
fig1 = figure(1);

subplot(2,2,1)
plot(time,real(psi1),'-red',time,real(psi1_an),':black','linewidth',2)
F_set_labels(20,'','Real $\langle 1 |\Psi_a(t,E)\rangle$','')
h = legend('Numerical','Analytical');
set(h,'interpreter','latex')
set(h,'orientation','horizontal')
set(h,'fontsize',20)
set(h,'Position',[0.45 0.92 0.1 0.1])
legend boxoff

xlim([ti tf])
ylim([-1 1])

subplot(2,2,2)
plot(time,imag(psi1),'-red',time,imag(psi1_an),':black','linewidth',2)
F_set_labels(20,'','Imag $\langle 1 |\Psi_a(t,E)\rangle$','')
xlim([ti tf])
ylim([-1 1])

subplot(2,2,3)
plot(time,real(psi2),'-red',time,real(psi2_an),':black','linewidth',2)
F_set_labels(20,'Time (fs)','Real $\langle 2 |\Psi_a(t,E)\rangle$','')
xlim([ti tf])
ylim([-1 1])

subplot(2,2,4)
plot(time,imag(psi2),'-red',time,imag(psi2_an),':black','linewidth',2)
F_set_labels(20,'Time (fs)','Imag $\langle 2 |\Psi_a(t,E)\rangle$','')
xlim([ti tf])
ylim([-1 1])


set(fig1,'Position',[10 10 800 700])
set(fig1,'Papersize',[10 10])
print(fig1,'-dpdf','psi_comparison.pdf')


%-------------------------
% plot the omega vectors
%-------------------------
fig2 = figure(2);

for a=1:dim_omg
	pos1 = (a-1)*2 + 1;
	pos2 = 2*a;

	subplot(dim_omg,2,pos1)
	plot(time,real(omg(:,a)),'-blue',time,real(omg_an(:,a)),':black','linewidth',2)
	if pos1==7 || pos1==8
	    F_set_labels(20,'Time (fs)','','')
	    xlim([ti tf])
	    ylim([-1 1])
	else
	    F_set_labels(20,'','','')
	    xlim([ti tf])
	    ylim([-1 1])
	end

	 subplot(dim_omg,2,pos2)
         plot(time,imag(omg(:,a)),'-blue',time,imag(omg_an(:,a)),':black','linewidth',2)
	 if pos1==7 || pos1==8
            F_set_labels(20,'Time (fs)','','')
            xlim([ti tf])
            ylim([-1 1])
        else
            F_set_labels(20,'','','')
            xlim([ti tf])
            ylim([-1 1])
        end



end

h = legend('Numerical','Analytical');
legend boxoff
set(h,'interpreter','latex')
set(h,'orientation','horizontal')
set(h,'fontsize',20)
set(h,'Position',[0.45 0.92 0.1 0.1])

set(fig2,'Position',[900 10 800 1000])
set(fig2,'Papersize',[10 11])
print(fig2,'-dpdf','omg_comparison.pdf')

