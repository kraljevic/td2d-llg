function F_set_lead_vectors()
% This function will set the lead vectors in the xi variable 
% which is for all the lead that are attched to the system
% It will store the vectors in a reordering scheme where \alpha C indices 
% have been mapped to an index ``a''. We also are assuming that all 
% leads are identical. 

%-------------------------
% Call required Variables
%-------------------------
global Nx
global Ny
global gamma
global norbs
global dim

%--------------------------------
% Construct the lead Hamiltonian
%--------------------------------
% Supercell Hamiltonian
H0L = zeros(Ny,Ny);
for a=1:Ny-1
    H0L(a,a+1) = -gamma;
    H0L(a+1,a) = -gamma;
end
H0L = kron(H0L,eye(norbs));

% Hopping Hamiltonian
H1L = -gamma*eye(Ny);
H1L = kron(H1L,eye(norbs));

% Coupling Hamiltonian
H1cl = H1L;

%-----------------------------
% Gamma Matrix and its vectors
%-----------------------------
% Gamma Matrix
Gamma = F_gamma_matrix(H0L,H1L,0.3,H1cl);

% test Gamma matrix by plotting it



% Find the eigen vectors
[V,D] = eig(Gamma);

%----------------------------------------
% Set the ordering in the index variable
%----------------------------------------
F_set_indexing();

% Print the indexing into a file
global idx
dlmwrite('./test_functions/F_set_indexing.dat', idx, 'delimiter', '\t')

%------------------------------------
% Now load the vectors in xi vectors 
%------------------------------------
F_load_in_xi_vectors(V);

% Print these xi vectors into a file
global xi
dlmwrite('./test_functions/F_load_in_xi_vectors_real.dat', real(xi), 'delimiter', '\t', 'precision', '%.3f')
dlmwrite('./test_functions/F_load_in_xi_vectors_imag.dat', imag(xi), 'delimiter', '\t', 'precision', '%.3f')

%------------------------------------------
% Now load the centers of the semi-circles
%------------------------------------------
F_load_in_circle_cent(H0L)
global circle_cent
dlmwrite('./test_functions/F_load_in_circle_cent.dat', real(circle_cent), 'delimiter', '\t', 'precision', '%.3f')

end
