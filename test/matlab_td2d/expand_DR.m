function DR = expand_DR(DL_m,dimc)
DR = zeros(dimc);
s  = length(DL_m);
DR((dimc-s+1):dimc,(dimc-s+1):dimc) = DL_m(1:s,1:s);
end