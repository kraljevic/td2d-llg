function F_set_inital_WF_and_OMG()
%--------------------------
% Call required variables
%--------------------------
global Ei
global Ef
global dE
global psi_old
global psi
global omg
global omg_old
global Nx
global Ny
global gamma
global D
global xi
global L_t
global norbs
global dim_omg
global Nc_tot
global J
global test_int
global test_rk
global warmup 

if warmup==1 

['Choosing Pre-Warmup initial conditions']

	%--------------------------------
	% Construct the lead Hamiltonian
	%--------------------------------
	% Supercell Hamiltonian
	H0L = zeros(Ny,Ny);
	for a=1:Ny-1
    	H0L(a,a+1) = -gamma;
    	H0L(a+1,a) = -gamma;
	end
	H0L = kron(H0L,eye(norbs));
	
	% Hopping Hamiltonian
	H1L = -gamma*eye(Ny);
	H1L = kron(H1L,eye(norbs));
	
	% Coupling Hamiltonian
	H1cl = H1L;
	
	H1R = H1L;
	H0R = H0L;
	
	%-----------------------------------
	% Construct the system Hamiltonian
	%-----------------------------------
	H0C = F_ham_hop(Nx,Ny,gamma);
	
	%-----------------------------------
	% Set the initial condition of WF
	% and Omega vectors
	%-----------------------------------
	%e0_vec = zeros(dim_omg,1);
	%e0_vec(1,1) = 1; 
	
	e_num = 1;
	for E=Ei:dE:Ef
	GF_sys_ret = F_G_ret_nano_j(H0L,H0C,H0R,H1L,H1R,E,H1cl);
	psi_old(:,:,e_num) = GF_sys_ret*xi(:,:);
	
	%	for a=1:Nc_tot
	%	    for b=1:Nc_tot
	%		omg_old(:,a,b,e_num) = (-inv(gamma*D -1i*L_t(1,b)))*((xi(:,a))'*GF_sys_ret*xi(:,b))*e0_vec;		
	%	    end
	%	end
	e_num=e_num+1;
	end	
end

%------------------------------------------
% For testing analytical solution with RK4
%------------------------------------------
if test_rk==1

['Choosing RK test initial conditions']

	psi_old(1,:,:) = 1;
	omg_old(:,:,:,:) = 1;
end

%----------------------------
% For testing the integrator
%----------------------------
if test_int==1
['Choosing integrator test initial conditions']
    e_num=1;
    for E=Ei:dE:Ef
	% Define the 3 three components of the WF
	psi_old(1,:,e_num) = 1;
	psi_old(2,:,e_num) = E^2;
	psi_old(3,:,e_num) = E^4;	
	% We don't have to worry about omega (its already set to zero)
        e_num = e_num+1;
    end
end

psi = psi_old;
omg = omg_old;

end
