function omg_out = F_omg_evolve(psi_inp,omg_inp,a,e_num,E,t)
% Load required variables
global Nc_tot
global xi
global D
global L_t
global gamma
global hbar
global dim_omg
global idx
global circle_cent
global J
global approach

% Deifine the column vector e_0 in omega vectors space
e0_vec = zeros(dim_omg,1);
e0_vec(1,1) = 1;

omg_out = zeros(dim_omg,Nc_tot);
for b=1:Nc_tot
    % Find the lead index
    alpha = idx(b,2); 
    % Define the lead voltage parameter
    L_t = circle_cent(1,b) + F_lead_voltage(alpha,t);
    % Propagated omega
    if approach==2
         omg_out(:,b) = ((((xi(:,b))'*psi_inp)*gamma^2*J)*e0_vec + (L_t-E)*omg_inp(:,b) + 1i*gamma*D*omg_inp(:,b))/(1i*hbar);
    else
         omg_out(:,b) = (((xi(:,b))'*psi_inp)*e0_vec*gamma^2*J + (L_t)*omg_inp(:,b) + 1i*gamma*D*omg_inp(:,b))/(1i*hbar);
    end
end

end
