function val = F_W(a,E)

global mu_alpha
global idx
global mu_alpha
global circle_cent
global gamma
global beta_it
global test_int

% Find the alpha (lead index)
alpha = idx(a,2);

% Find the channel of the lead
C = idx(a,3);

% Find the chemical potential of the lead
mu = mu_alpha(1,alpha);

% Find the center of the semicircle
epsilon = circle_cent(1,a);


% now calculate the function value
E1 = epsilon - 2*gamma;
E2 = epsilon + 2*gamma;

g_alpha_C = F_g_alpha_C(E1,E2,epsilon,E);
fermi_val = F_fermi_val(E,mu);

val = fermi_val*g_alpha_C;

%---------------------
% Integrator tester
%---------------------
if test_int==1 % Only run when integrator is being tested 
 val = exp(-E^2);
end
     
end
