function GF_sys_ret =F_G_ret_nano_j(H0L,H0C,H0R,H1L,H1R,E,H1cl)

%----------------------------------------------------------------
% CALCULATES RETARDED GREENS FUNCTION OF NJ (ADVANCED -COMMENTED)
%----------------------------------------------------------------

dim = length(H0L);  % Hilber space of Lead supercell
dimc = length(H0C); % Hilbert space dimension of NJ
Id  = eye(dim);     % Identity matrix in lead
Idc  = eye(dimc);   % Identity matrix in NJ
iter= 50;
eta = 0.0005;

%-----------------
% RIGHT LEAD DATA
%-----------------
   AR_ret = (E+1i*eta)*Id - H0R;
   BR_ret = H1R;
%   AR_ad = ctranspose(AR_ret);
%   BR_ad  =  H1R;

   % Now we construct the GF_Right lead

   GF_RL_ret = rightleadGF(AR_ret,BR_ret,H0R,E,iter);
%   GF_RL_ad  = ctranspose(GF_RL_ret);

%----------------
% LEFT LEAD DATA
%----------------
   AL_ret = (E+1i*eta)*Id - H0L;
   BL_ret  = H1L;
%   AL_ad   = ctranspose(AL_ret)
%   BL_ad   = H1L;

   % Now we construct the GF_left lead

   GF_LL_ret = leftleadGF(AL_ret,BL_ret,H0L,E,iter);
%   GF_LL_ad  = ctranspose(GF_LL_ret);

%----------------------
% SELF ENERGY MATRICES
%----------------------

   DL_ret = ctranspose(H1cl)*GF_LL_ret*H1cl;
   DL_ret = expand_DL(DL_ret,dimc);            % Expand to HilbS of NanoJ
%   DL_ad  = ctranspose(H1cl)*GF_LL_ad*H1cl;
   DL_ad = ctranspose(DL_ret);              % Expand to HilbS of NanoJ

   DR_ret = H1cl*GF_RL_ret*ctranspose(H1cl);
   DR_ret = expand_DR(DR_ret,dimc);
%   DR_ad  = H1cl*GF_RL_ad*ctranspose(H1cl);
   DR_ad = ctranspose(DR_ret);

%------------------
% GREENS FUNCTIONS
%------------------
   GF_sys_ret = inv((E+1i*eta)*Idc - H0C - (DR_ret + DL_ret));
%  GF_sys_ad  = ctranspose(GF_sys_ret);

end
