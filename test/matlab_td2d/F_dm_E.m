function rho_E = F_dm_E(e_num,E)

%--------------------------
% Call required Variables
%--------------------------
global psi
global Nc_tot
global dim
global hbar

rho_E = zeros(dim,dim);
for a=1:Nc_tot
    rho_E = rho_E + (psi(:,a,e_num)*ctranspose((psi(:,a,e_num)))*F_W(a,E))/(2*pi);
end

end
