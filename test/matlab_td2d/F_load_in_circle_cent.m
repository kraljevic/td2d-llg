function F_load_in_circle_cent(H0L)

%-----------------------------
% Load any required variables
%-----------------------------
global Nc_alpha
global Nc_tot
global N_alpha
global circle_cent

%--------------------------------
% find the centers of the circle
%--------------------------------
evals = eig(H0L);

for alpha=1:N_alpha
        if alpha==1
            posi = 1;
            posf = Nc_alpha(1,alpha);
            circle_cent(1,posi:posf) = (evals)';
        else if alpha==2
            pos2_i = Nc_tot - Nc_alpha(1,alpha) + 1;
            pos2_f = pos2_i + Nc_alpha(1,alpha) - 1;
            circle_cent(1,pos2_i:pos2_f) = (evals)';
        end
end


end
