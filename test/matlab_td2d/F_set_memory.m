function F_set_memory()

% Central Scatering Region System Parameters
global Nx
global Ny
global norbs
global dim
dim = Nx*Ny*norbs;

% Lead and reservoirs Variables
global N_alpha;
global Nc_alpha;          
Nc_alpha = zeros(1,N_alpha);
	for a=1:N_alpha
    	    Nc_alpha(1,a) = Ny*norbs;
	end
global Nc_tot;
Nc_tot = sum(Nc_alpha);

global xi;       xi = zeros(dim,Nc_tot);
global L_t;      L_t= zeros(1,Nc_tot);

global Efermi;
global eVb;
global mu_alpha; 
	for a=1:N_alpha
	    if a==1
	        mu_alpha(1,a) = Efermi+0.5*eVb;
            else if a==2
		mu_alpha(1,a) = Efermi-0.5*eVb;
	    else
	        mu_alpha(1,a) = Efermi;
	    end
	end
global circle_cent; circle_cent = zeros(1,Nc_tot);

% Indexing variables
global idx;
idx = zeros(Nc_tot,3);

% time propagation Variables
global ti;
global tf;
global dt;
tnos = length(ti:dt:tf);

% Energy integration Variables
global Ei;
global Ef;
global dE;
enos = length(Ei:dE:Ef);
 
% Psi WF variables
global psi; psi = zeros(dim,Nc_tot,enos);
global psi_old; psi_old = zeros(dim,Nc_tot,enos);

% Omega matrix variables and test variables
global m_max;
global dim_omg;
dim_omg = m_max+1;
global D; D = zeros(dim_omg,dim_omg);
global P; P = zeros(1,dim_omg);
global omg; omg = zeros(dim_omg,Nc_tot,Nc_tot,enos);
global omg_old; omg_old = zeros(dim_omg,Nc_tot,Nc_tot,enos);
global psi_test; psi_test = zeros(tnos,dim);
global omg_test; omg_test = zeros(tnos,dim_omg);

% Hamiltonian
global H; H = zeros(dim,dim);

end
