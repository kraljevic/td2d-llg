clc
clear all

%------------------
% Load variables
%------------------
Ei = -3;
Ef = 3;
dE = 0.01;

epsilon = 0;
E1 = epsilon - 2;
E2 = epsilon + 2;

mu = 1;

energy = Ei:dE:Ef;
func1 = zeros(1,length(energy));
func2 = zeros(1,length(energy));

c=1;
for E=Ei:dE:Ef
    func1(1,c) = F_g_alpha_C(E1,E2,epsilon,E);
    func2(1,c) = F_fermi_val(E,mu);
    c=c+1;
end


subplot(2,2,1)
plot(energy,func1,'-red','linewidth',2)

subplot(2,2,2)
plot(energy,func2,'-blue','linewidth',2)
