function val = F_g_alpha_C(E1,E2,epsilon,E)

    if E<E1 || E>E2
       val = 0;
    else
       val = sqrt(4 - (E-epsilon)^2); % Inside the semi-circle
    end 
    
end
