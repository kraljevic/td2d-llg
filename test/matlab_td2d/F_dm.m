function rho = F_dm(t)
%--------------------------
% Call required variables
%--------------------------
global Ei
global Ef
global dE

global dim

global psi_old;
global omg_old;
global psi;
global omg;
global test_rk;
global test_int;
global warmup 

%---------------------------
% Initialize density matrix 
%---------------------------
rho = zeros(dim,dim);

%--------------------------
% Perform Energy integral
%--------------------------
e_num = 1;
for E=Ei:dE:Ef
        % Now construct the density matrix
        rho = rho + F_dm_E(e_num,E)*dE;
	% Update the Objects of EOM at the specific energy
	% Update only when the integrator is not being tested
	% Update only when the warmup is not being tested
	if test_int==0 && warmup==0 
            F_update_EOM_objects(e_num,E,t);
	end

    e_num=e_num+1;
end


%-------------------------------------------------------------
% Save the WF and Omega vector at testing energy and channels
%-------------------------------------------------------------
if test_rk==1 % Only runs when RK test is being done

	global E_test;
	global C_psi_test;
	global C1_omg_test;
	global C2_omg_test;
	global ti;
	global dt;
	global psi_test;
	global omg_test;

	t_num = length(ti:dt:t);
	e_test_num = length(Ei:dE:E_test);

	psi_test(t_num,:) = transpose(psi_old(:,C_psi_test,e_test_num));
	omg_test(t_num,:) = transpose(omg_old(:,C1_omg_test,C2_omg_test,e_test_num));
end

%-----------------------------------------------
% Update the Old values of WF and Omega vectors
%-----------------------------------------------
psi_old = psi;
omg_old = omg;


end
