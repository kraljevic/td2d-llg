function G=rightleadGF(A,B,H0,E,iter)
Id= eye(length(B)); 
X = inv(A)*ctranspose(B);
Y = inv(A)*B;
S = X;
alpha = Y;
  for a=1:iter
     T = inv(Id - X*Y -Y*X);
     X = T*X*X;
     Y = T*Y*Y;
     s = alpha*X;
     S=S+s;
     alpha = alpha*Y;
  end
 G = inv(E*Id - H0 - B*S);
end