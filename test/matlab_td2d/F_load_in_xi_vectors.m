function F_load_in_xi_vectors(V)

global Nc_alpha
global Nc_tot
global xi
global N_alpha
global dim

for alpha=1:N_alpha
	if alpha==1
            posi = 1;
	    posf = Nc_alpha(1,alpha);
            xi(posi:posf,posi:posf) = V;
	else if alpha==2
	    pos1_i = dim - Nc_alpha(1,alpha) + 1; 
	    pos1_f = pos1_i + Nc_alpha(1,alpha) - 1;
	    pos2_i = Nc_tot - Nc_alpha(1,alpha) + 1;
	    pos2_f = pos2_i + Nc_alpha(1,alpha) - 1;		
	    xi(pos1_i:pos1_f,pos2_i:pos2_f) = V;
	end
end

end
