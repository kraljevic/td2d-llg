function F_set_global_variables(file_str,tag_no)

	% Central Scatering Region System Parameters
        global Nx ;    
        global Ny ;   
        global gamma ;
        global tsoc; 
        global jsd;
        global ort;
	global norbs ; 
        global dim;

        % Lead and reservoir Variables
	global  temp;        % Temperature of reservoirs   
	global  N_alpha;     % Number of leads
	global  Nc_alpha     % Vector of Number of channels in each lead
        global  Nc_tot;      % Number of channels when all leads are included
        global  xi;          % xi vectors of all leads
	global  L_t;         % lead voltage and semi circle parameters(time dependent)
	global  mu_alpha;    % Row vector of lead chemical potentials
	global  Efermi;      % Fermi-energy of system
	global  eVb;	     % Bias Voltage
        global  circle_cent  % Centers of the semi-circles
        
	% Indexing variables
	global idx;	
 
	% time propagation variables
	global ti; 
	global tf;
	global dt;

	% Energy integration Variables
	global Ei; 
	global Ef; 
	global dE; 

	% Psi WF variables
	global psi           % Wave Function variable
	global psi_old	     % Wave Function at previous time-step

	% Omega matrix variables and testing variables
	global m_max;        % maximum modes
	global D;	     % D-matrix
	global P;	     % P-vector (row)
	global omg;	     % Omega vector variable
	global omg_old;	     % omega vector variable at previous time-step

	% Testing parameters
	global test_rk       % Paramter to test RK solver
	global J;            % Coupling between omega and psi EOMs
	global K;	     % Parameter which kills xi vectors in psi EOM	
	global E_test;	     % Energy at which EOM is tested
	global C_psi_test;   % Channel at which Psi vector is tested
	global C1_omg_test;  % Channel 1 at which omg vector is tested
	global C2_omg_test;  % Channel 2 at which omg vector is tested
	global test_int;     % paramteter to test the integrator
	global warmup;       % Warmup paramters
	global approach	     % Solver approach 
	global psi_test;     % Testing wave function
	global omg_test;     % Testing omega vector	
        global tag;          % tag for output file 
               tag=tag_no;

	% Hamiltonian
	global H
	
        % Constants
	global hbar
        global  kb
        global  beta_it
   
	%-----------------------------
	% Now assigin these variables
	%-----------------------------
	var  = F_load_variables(file_str);
	s=1;
	
	% Central Scatering Region System Parameters
        Nx = var(s,1);     s=s+1;
        Ny = var(s,1);     s=s+1;
        gamma = var(s,1);  s=s+1;
        tsoc = var(s,1);   s=s+1;
        jsd = var(s,1);    s=s+1;
        ort = var(s,1);    s=s+1;
	norbs = var(s,1); s=s+1;
 
	% Lead and reservoir Variables
	temp = var(s,1);     s=s+1;
	N_alpha = var(s,1);  s=s+1;
        Efermi = var(s,1);   s=s+1;
        eVb = var(s,1);      s=s+1;

        % time propagation variables
        ti = var(s,1); s=s+1;
	tf = var(s,1); s=s+1;
	dt = var(s,1); s=s+1;

	% Energy integration variables
	Ei = var(s,1); s=s+1;
	Ef = var(s,1); s=s+1;
	dE = var(s,1); s=s+1;

        % Omega matrix maximum modes
	m_max = var(s,1); s=s+1;

	% Testing parameters
	test_rk = var(s,1); s=s+1;
	J = var(s,1); s=s+1;
	K = var(s,1); s=s+1;
	E_test = var(s,1); s=s+1;
	C_psi_test = var(s,1);s=s+1;
	C1_omg_test = var(s,1);s=s+1;
	C2_omg_test = var(s,1);s=s+1;
	test_int = var(s,1);s=s+1;
	warmup = var(s,1);s=s+1;
	approach = var(s,1);

	% Fundamental constants
        hbar = 0.6582119514; % eVfs
        kb = 8.617333262*10^(-5); % eV/K  
	beta_it = 1/(kb*temp); % 1/eV


        %-------------------------------
	% Section for testing the code
	%-------------------------------
	fig1 = figure(1);
	set(fig1,'DefaultFigureVisible','off')
	plot(1,1)
	xlim([-10 10]);
        ylim([-10 10]);
	set(gca,'Visible','off')

	xb = -9;
	yb = 9;
	ds = 1;

	v1 = ['$N_x$ = ' num2str(Nx) ];
	v2 = ['$N_y$ = ' num2str(Ny) ];
	v3 = ['$\gamma$ = ' num2str(gamma)];
        v3a = ['$t_{soc}$ = ' num2str(tsoc)];
        v3b = ['jsd = ' num2str(jsd)];
        v3c = ['Orientation = ' num2str(ort)];
	v4 = ['No. of Orbitals per site = ' num2str(norbs)];
	text(xb,yb,v1,'interpreter','latex'); con = 1;
	text(xb,yb-con*ds,v2,'interpreter','latex'); con=con+1;
	text(xb,yb-con*ds,v3,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v3a,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v3b,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v3c,'interpreter','latex'); con=con+1;
	text(xb,yb-con*ds,v4,'interpreter','latex'); con=con+1;

	v5 = ['Temperature = ' num2str(temp) ' K'];
	v6 = ['$N_{\alpha}$ = ' num2str(N_alpha) ];
	v7 = ['$E_F$ = ' num2str(Efermi) ' eV'];
	v8 = ['$eV_b$ = ' num2str(eVb) ' eV'];
	text(xb,yb-con*ds,v5,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v6,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v7,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v8,'interpreter','latex'); con=con+1;

	v9 =  ['$t_i$ = ' num2str(ti) ' fs'];
        v10 = ['$t_f$ = ' num2str(tf) ' fs'];
        v11 = ['$\Delta t$ = ' num2str(dt) ' fs'];
        text(xb,yb-con*ds,v9,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v10,'interpreter','latex');con=con+1;
        text(xb,yb-con*ds,v11,'interpreter','latex');con=con+1;

	v12 =  ['$E_i$ = ' num2str(Ei) ' eV'];
        v13 = ['$E_f$ = ' num2str(Ef) ' eV'];
        v14 = ['$\Delta E$ = ' num2str(dE) ' eV'];
        text(xb,yb-con*ds,v12,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v13,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v14,'interpreter','latex'); con=con+1;

	v15 =  ['$m_\mathrm{max}$ = ' num2str(m_max)];
        text(xb,yb-con*ds,v15,'interpreter','latex'); con=con+1;
	
	v16 =  ['$\hbar$ = ' num2str(hbar)];
        v17 = ['$k_b$ = ' num2str(kb)];
        text(xb,yb-con*ds,v16,'interpreter','latex'); con=con+1;
        text(xb,yb-con*ds,v17,'interpreter','latex'); con=con+1;

	
	% Save figure
	set(fig1,'Visible','off')
	set(fig1,'Papersize',[8 5])
        str = ['./results/params' num2str(tag) '.pdf'];
	print(fig1,'-dpdf', str)
		        
end
