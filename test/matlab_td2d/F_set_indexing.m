function F_set_indexing()

global idx;
global N_alpha;
global Nc_alpha;

c=1;
for alpha=1:N_alpha
    for Nc=1:Nc_alpha(1,alpha)
	idx(c,1) = c;
        idx(c,2) = alpha;
        idx(c,3) = Nc;
        c=c+1;
    end
end


end
