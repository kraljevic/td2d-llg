function Gamma_L = F_gamma_matrix(H0L,H1L,E,H1cl)

%----------------------------------------------------------------
% CALCULATES RETARDED GREENS FUNCTION OF NJ (ADVANCED -COMMENTED)
%----------------------------------------------------------------

dim = length(H0L);  % Hilber space of Lead supercell
Id  = eye(dim);     % Identity matrix in lead
iter= 50;
eta = 0.0005;

%----------------
% LEFT LEAD DATA
%----------------
   AL_ret = (E+1i*eta)*Id - H0L;
   BL_ret  = H1L;
   % Now we construct the GF_left lead
   GF_LL_ret = leftleadGF(AL_ret,BL_ret,H0L,E,iter);

%----------------------
% SELF ENERGY MATRICES
%----------------------

   DL_ret = ctranspose(H1cl)*GF_LL_ret*H1cl;
   DL_ad  = ctranspose(DL_ret);
%----------------
% GAMMA MATRICES
%----------------

   Gamma_L = 1i*(DL_ret - DL_ad);


end
