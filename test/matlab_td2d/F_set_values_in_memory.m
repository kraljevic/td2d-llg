function F_set_values_in_memory()

% Central Scatering Region System Parameters
global dim;
global gamma;
global J;

% Lead and reservoirs Variables
F_set_lead_vectors();

% Indexing variables (Will be set in the previous step)

% time propagation variables

% Energy integration Variables

% Psi WF variables
global psi_old; 
%psi_old = F_set_initial_WF();

% Omega matrix variables
global dim_omg;
global D;
global test_rk;
if test_rk==0 % When the psi and omg EOM are coupled
    for a=1:dim_omg-1
        if a==1
        D(a,a+1) = -2;
        else
	D(a,a+1) = -1;
	end
	D(a+1,a) = 1;
    end
else   % When the psi and omg EOM are decoupled
    D = -eye(dim_omg);
end

%dlmwrite('./test_functions/D_matrix.dat', real(D) , 'delimiter', '\t', 'precision', '%.3f')

global P;
m=0;
    for a=1:dim_omg
        P(1,a) = F_P_func(m,gamma);
    m=m+1;
    end
%dlmwrite('./test_functions/F_P_func_real.dat', real(P), 'delimiter', '\t', 'precision', '%.3f')
%dlmwrite('./test_functions/F_P_func_imag.dat', imag(P), 'delimiter', '\t', 'precision', '%.3f')

% Hamiltonian
global H; H = zeros(dim,dim);

end
