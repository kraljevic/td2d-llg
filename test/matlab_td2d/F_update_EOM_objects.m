function F_update_EOM_objects(e_num,E,t)
%--------------------------
% Call required Variables
%--------------------------
global psi
global psi_old
global omg
global omg_old
global Nc_tot
global dt
global dim_omg

%--------------------
% Runge-Kutta Steps
%--------------------
omg_inp = zeros(dim_omg,Nc_tot);

for a=1:Nc_tot

    % Take the old values of Psi and omega vectors at this particular mode 
    psi_inp = psi_old(:,a,e_num);
    omg_inp(:,:) = omg_old(:,a,:,e_num);
    
    % Take the Runge-Kutta steps to evolve 
    psi1 = F_psi_evolve(psi_inp,omg_inp,a,e_num,E,t)*dt;
    omg1 = F_omg_evolve(psi_inp,omg_inp,a,e_num,E,t)*dt;
 
    psi2 = F_psi_evolve(psi_inp+psi1/2,omg_inp+omg1/2,a,e_num,E,t+dt/2)*dt;
    omg2 = F_omg_evolve(psi_inp+psi1/2,omg_inp+omg1/2,a,e_num,E,t+dt/2)*dt;

    psi3 = F_psi_evolve(psi_inp+psi2/2,omg_inp+omg2/2,a,e_num,E,t+dt/2)*dt;
    omg3 = F_omg_evolve(psi_inp+psi2/2,omg_inp+omg2/2,a,e_num,E,t+dt/2)*dt;

    psi4 = F_psi_evolve(psi_inp+psi3,omg_inp+omg3,a,e_num,E,t)*dt;
    omg4 = F_omg_evolve(psi_inp+psi3,omg_inp+omg3,a,e_num,E,t)*dt;

    % Construct the updated WF and Omega vector of this mode
    psi_out = psi_inp + ((psi1 + 2*psi2 + 2*psi3 + psi4)/6);
    omg_out = omg_inp + ((omg1 + 2*omg2 + 2*omg3 + omg4)/6);
    
    % Load the updated WF and omega vector into their variables
    psi(:,a,e_num) = psi_out;
    omg(:,a,:,e_num) = omg_out;
        
end


end
