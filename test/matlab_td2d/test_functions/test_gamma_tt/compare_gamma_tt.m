clc
clear all

%----------
% Variables
%-----------
hbar = 0.658211951;
gamma = 1;

% Position of semicircle
epsilon = 0;
E1 = epsilon - 2*gamma;
E2 = epsilon + 2*gamma;

% Energy integral parameters
Ei = -2.1;
Ef = 2.1;
dE = 0.01;

%------------------------------
% Construction of expression 1
%------------------------------
xi = -20;
xf = 20;
dx = 0.01;

x = (xi:dx:xf)';
y1 = zeros(length(xi:dx:xf),1);

for E=Ei:dE:Ef
    if E<E1 || E>E2 
        g = 0;
    else
        g = sqrt(4*gamma^2 - (E-epsilon)^2);
    end
    y1 = y1 + g*exp(-1i*E*x/hbar);
end
y1 = y1*(dE/(2*pi*hbar));

%------------------------------
% Construction of Expression 2
%------------------------------
y2 = (1/hbar)*exp(-1i*epsilon*x/hbar).*(besselj(0,2*x*gamma/hbar) + besselj(2,2*x*gamma/hbar));

%---------------------
%  Plot the functions
%---------------------
plot(x,real(y1),'-blue',x,real(y2),'red','linewidth',2)
