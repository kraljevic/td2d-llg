clc
clear all
close all
%-----------
% Variables
%-----------
Nymax = 50;
gamma = 1;
N = 1:Nymax;
wid = zeros(1,Nymax);

for Ny=1:Nymax         
	%---------------------------
	% Calculate the hamiltonian
	%---------------------------
	H = zeros(Ny,Ny);
	for a=1:Ny-1
   	H(a,a+1) = -gamma;
   	H(a+1,a) = -gamma;
	end

	%--------------------------------------------
	% Find the eigen values and pick lowest one
	%--------------------------------------------
        [V D] = eig(H);
        myval = diag(D);
        val = min(myval);
	val = val;
        
	%----------------------
	% Set the value in
	%----------------------
	wid(1,Ny) = abs(val); 
end

fig1 = figure(1);

plot(N,wid,'-*red')
F_set_labels(16,'Width','Half Bandwidth','')


set(fig1,'Position',[10 10 300 280])
set(fig1,'papersize',[10 10])
F_pdf(fig1,'bandwidth.pdf',600)
