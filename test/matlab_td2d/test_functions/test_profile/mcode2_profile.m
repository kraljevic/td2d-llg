clc
clear all

fig1 = figure(1);

%----------------
% Extract data 2
%----------------
data = dlmread('width_prof.dat');
Ny = data(:,1);
time = data(:,2);
time_rat = time./time(1);

x = 1:0.01:7;
y = x.^2;
n=1.7;
ytest = x.^n;

subplot(1,3,1)
plot(Ny,time_rat,'-*blue',x,y,'-red','linewidth',2)
F_set_labels(12,'Width of Wire','Relative CPU Time','')
xlim([1 7])
xticks([1 2 3 4 5 6 7])
ylim([1 32])
F_panel(12,'W=1',0.02,0.9)
F_panel(12,'30 fs $\sim$ 10 min.[Mott]',0.02,0.8)


%----------------
% Extract data 1
%----------------

data = dlmread('omega_prof2x2.dat');
omega = data(:,1);
tmax = data(:,2);
tmax = tmax/tmax(1);

subplot(1,3,2)
plot(omega,tmax,'-*blue','linewidth',2)
F_set_labels(12,'Length of $\vec{\mathrm{\Omega}}$','Max. Relative Time ($\mathrm{t_{max}}$)','')
xlim([60 201])
xticks([60 80 100 120 140 160 180 200])
ylim([1 3.5])
F_panel(12,'$\mathrm{N_{x} \times N_y = 2\times2}$',0.02,0.9)
%F_panel(12,'300 fs $\sim$ 7.84 hrs[Bardeen]',0.02,0.8)

%----------------
% Extract data 3
%----------------
data = dlmread('time_prof2x2.dat');
time_sim = data(:,1);
time_cpu = data(:,2);
time_cpu = time_cpu./time_cpu(1);

subplot(1,3,3)
plot(time_sim,time_cpu,'-*blue','linewidth',2)
F_set_labels(12,'Simulation Time (fs)','Relative CPU Time','')
xlim([1 100])
%xticks([1 2 3 4 5 6 7])
%ylim([1 6])
F_panel(12,'$\mathrm{N_x \times N_y} = $2$\times$2',0.02,0.9)
F_panel(12,'24 fs $\sim$ 761 s [Mott]',0.02,0.8)
%F_panel(12,'30 fs $\sim$ 10 min.[Mott]',0.02,0.8)

%-----------------
% Set figure size
%-----------------
set(fig1,'Position',[10 10 1200 300])
set(fig1,'papersize',[12 6])
F_pdf(fig1,'profile.pdf',600)

