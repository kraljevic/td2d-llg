function H0 = F_ham_hop(Nx,Ny,gamma)

%-------------------------
% y-direction Hamiltonian
%-------------------------
H0y = zeros(Ny,Ny);
for a=1:Ny-1
    H0y(a,a+1) = -1;
    H0y(a+1,a) = -1;
end

%-------------------------
% x-direction Hamiltonian
%-------------------------
H0x = -gamma*eye(Ny);

%----------------------------
% Now create the Hamiltonian
%----------------------------
H0 = zeros(Nx*Ny,Nx*Ny);
for a=1:Nx-1
    posi = (a-1)*Ny  + 1;
    posf = posi + Ny - 1;
    H0(posi:posf,posi:posf) = H0y;
    H0(posi:posf,posi+Ny:posf+Ny) = H0x;
    H0(posi+Ny:posf+Ny,posi:posf) = ctranspose(H0x);
end
H0(end-Ny+1:end,end-Ny+1:end) = H0y;

end
