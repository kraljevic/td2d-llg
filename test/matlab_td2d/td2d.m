function td2d(str,tag_no,ncores)
close all
LASTN = maxNumCompThreads(ncores);
%-----------------------
% Set the primary steps
%-----------------------
F_set_global_variables(str,tag_no);

F_set_memory();

F_set_values_in_memory();

F_set_eq_ham();

F_set_initial_WF_and_OMG();

%-----------------------------
% Call the required Variables
%-----------------------------
global ti
global tf
global dt
global dim
global test_rk
global test_int

%-----------------
% Time loop begins
%-----------------
time = (ti:dt:tf)';
rho_ij = zeros(length(time),dim^2);

c=1;
time_str = ['Calculation has been started ... ']
tic
for t=ti:dt:tf
t
	% Hamiltonian at t_n time step
	F_ham_td(t);
        % Density Matrix t_n time step (first calculates then updates)
	rho = F_dm(t);
        % Calculate the charge on each site
	rho_ij(c,:) = reshape(rho,[1, dim^2]);
   c=c+1;
end
time_str = [' Time elapsed in the calculation is :: ']
toc
%---------------
% Time loop end
%---------------
% If you are testing the integrator
if test_int==1
    if t==tf
	dlmwrite('./test_functions/rho_real.dat', real(rho), 'delimiter', '\t', 'precision', '%.5f')
	dlmwrite('./test_functions/rho_imag.dat', imag(rho), 'delimiter', '\t', 'precision', '%.5f')
    end
end
%---------------------
% Save data into files
%---------------------
% if you are testing the evolved Omega and Psi functions
if test_rk==1
	% Save the Evolved test Wave function
	global psi_test
	dlmwrite('./test_functions/psi_test.dat',[time, psi_test])

	% Save the Evolved Omega vectors 
	global omg_test
	dlmwrite('./test_functions/omg_test.dat',[time, omg_test])
end

% Save the Density matrix of the problem
global tag
string = ['./results/rho_ij' num2str(tag) '.dat'];
dlmwrite(string,[time,rho_ij])

%--------------------------
% Run the analytical tests
%--------------------------
if test_rk==1
F_run_test_calculations();
end

end
