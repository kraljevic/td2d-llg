function H0 = F_ham_rashba(Nx,Ny,tso,t)

sigmax = [0 1;1 0];
sigmay = [0 -1i;1i 0];


%-------------------------
% y-direction Hamiltonian
%-------------------------
H0y_common = zeros(Ny,Ny);
H0y_soc = zeros(Ny,Ny);
for a=1:Ny-1
    H0y_common(a,a+1) = 1; H0y_soc(a,a+1) = 1i*tso;
    H0y_common(a+1,a) = 1; H0y_soc(a+1,a) = -1i*tso;
end
% SOC part
H0y_soc = kron(H0y_soc,sigmax);
% Hopping part
H0y_hop = -t*kron(H0y_common,eye(2));
% Total Y-direction Hamiltonina
H0y = H0y_hop + H0y_soc;

%-------------------------
% x-direction Hamiltonian
%-------------------------
H0x_common = eye(Ny);
% SOC part
H0x_soc = 1i*tso*kron(H0x_common,sigmay);
% Hopping part
H0x_hop = -t*kron(H0x_common,eye(2));
% total X-direction hooping
H0x = H0x_hop + H0x_soc;


%----------------------------
% Now create the Hamiltonian
%----------------------------
H0 = zeros(2*Nx*Ny,2*Nx*Ny);
for a=1:Nx-1
    posi = (a-1)*2*Ny  + 1;
    posf = posi + 2*Ny - 1;
    H0(posi:posf,posi:posf) = H0y;
    H0(posi:posf,posi+2*Ny:posf+2*Ny) = H0x;
    H0(posi+2*Ny:posf+2*Ny,posi:posf) = ctranspose(H0x);
end
H0(end-2*Ny+1:end,end-2*Ny+1:end) = H0y;


end
