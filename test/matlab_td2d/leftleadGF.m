function G=leftleadGF(A,B,H0,E,iter)
Id= eye(length(B)); 
X = inv(A)*B;
Y = inv(A)*ctranspose(B);
S = X;
alpha = Y;
  for a=1:iter
     T = inv(Id - X*Y -Y*X);
     X = T*X*X;
     Y = T*Y*Y;
     s = alpha*X;
     S=S+s;
     alpha = alpha*Y;
  end
  G = inv(E*Id - H0 - ctranspose(B)*S);
end