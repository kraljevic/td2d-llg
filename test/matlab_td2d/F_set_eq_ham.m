function H_eq = F_set_eq_ham()
global Nx
global Ny
global gamma 
global H
global tsoc
global jsd
global tag
global norbs
global ort

%-------------------
% Hamiltonian
%-------------------
if norbs==2
    H_soc = F_ham_rashba(Nx,Ny,tsoc,gamma);
    H_jsd = F_ham_jsd(Nx,Ny,jsd,ort);
    H = H_soc + H_jsd;
else
    H_hop  = F_ham_hop(Nx,Ny,gamma);
    H = H_hop
end

%----------------------------------
% Save the Equilibirum Hamiltonian
%----------------------------------
str1 = ['./results/h_real' num2str(tag) '.dat'];
str2 = ['./results/h_img' num2str(tag) '.dat'];

F_print_m(str1,real(H),3);
F_print_m(str2,imag(H),3);
end
