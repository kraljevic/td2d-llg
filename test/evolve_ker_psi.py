import numpy as np
from td2d.evolve import evolve_ker_psi
from td2d.kwant_systems import make_ribbon
from td2d.kwant_wrapper import get_ham as get_h

# Create fake parameters
n_grid = 20
dim_h = 4
nc_tot = 10
n_ker = 20
n_grid = 16

energy = np.random.rand(n_grid)
psi_old = np.random.rand(dim_h, nc_tot, n_grid)
psi_mem_old = np.random.rand(dim_h, nc_tot, n_grid, n_ker)
xi = np.random.rand(dim_h, nc_tot)
kernel_matrix = np.random.rand(dim_h, dim_h, n_grid, n_ker)

# create kwant system
sys = make_ribbon(length=2, width=2, plot_sys=True,spin=False)
args_ls = ([0.1, 0.2])
ham = get_h(sys, args=args_ls, sparse=False)

# Create expected result
rhs_expected = np.zeros((dim_h, nc_tot, n_grid), dtype=complex)
term2 = np.zeros((dim_h, nc_tot, n_grid), dtype=complex)
for k in range(n_grid):
	rhs_expected[:, :, k] = np.dot(ham, psi_old[:, :, k]) - energy[k]*psi_old[:, :, k] + xi
	for l in range(n_ker):
		term2[:, :, k] = term2[:, :, k] + np.dot(kernel_matrix[:, :, k, l], psi_mem_old[:, :, k, l])

	rhs_expected[:, :, k] = rhs_expected[:, :, k] - 1j*term2[:, :, k]


rhs_fn = evolve_ker_psi(energy, psi_old, psi_mem_old,
                   sys, args_ls, xi, kernel_matrix)

print('--------------------------------------')
print('Checking difference between results ')
print('--------------------------------------')
res = np.allclose(rhs_fn, rhs_expected)
if res:
    print('Passed : No difference')
else:
    print('Failed : Difference')


