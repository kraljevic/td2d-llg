# This is a testing script to test the "hybridize" and "un_hybridize"
# functions in evolve.py module.

import numpy as np
from td2d.evolve import hybridize
from td2d.evolve import un_hybridize

# Hamiltonian dimension
dim_h = 2
# total channells
nc_tot = 2
# Size of omega vectors
m_max = 4
# total number of grid points
n_grid = 10

# Create random psi_old and omg_old variables
psi_old1 = np.random.rand(dim_h, nc_tot, n_grid)
omg_old1 = np.random.rand(m_max, nc_tot, nc_tot, n_grid)

# Create their hybrid using hybridize function
hyb = hybridize(psi_old1, omg_old1)

# Create the un_hybridized versions of the two using the un_hybridzie function
psi_old2, omg_old2 = un_hybridize(hyb, dim_h, nc_tot, n_grid, m_max)

# Decide some tolerence to check the difference
rtol = 1e-05
atol = 1e-08

print('-------------------------------------------')
print('If both of the guys below are true then the')
print('the function are correctly constructed.')
print('-------------------------------------------')
print(np.allclose(psi_old2, psi_old1, rtol, atol))
print(np.allclose(omg_old2, omg_old1, rtol, atol))

