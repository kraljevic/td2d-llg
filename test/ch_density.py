import numpy as np
from td2d.measure import Measure

rho = np.array([[0.5, 1, 0, 0],
				[1, 0.5, 0, 0],
				[0, 0, 0.3, 1+2j],
				[0, 0, 1-2j, 0.3]])
spin = True
chd_fn = Measure.ch_density(rho, spin_full=spin, where='all')
if spin:
    chd_exp = np.real([rho[0, 0]+rho[1, 1], rho[2, 2]+rho[3, 3]])
else:
    chd_exp = np.diag(rho)

print('-----------------')
print('Expected Result ')
print('-----------------')
print(chd_exp)
print('---------------------')
print('Result from function')
print('---------------------')
print(chd_fn)
print('-------------------')
print('test results')
print('-------------------')
res = np.allclose(chd_fn, chd_exp)
if res:
	print('Passed')
else:
	print('Failed')


