from td2d.kwant_wrapper import lead_and_channel_finder
from td2d.kwant_systems import make_ribbon
import numpy as np

sys = make_ribbon(length=2, width=5, tsoc=0, plot_sys=False,spin=False)
sys_args = ([0, 0])
ch = 4
lead_no, lead_ch_no = lead_and_channel_finder(ch, sys, sys_args)

print('lead number = ', lead_no)
print('lead channel number = ', lead_ch_no)
