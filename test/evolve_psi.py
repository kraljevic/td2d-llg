''' This script tests if 'evolve_psi' function is correctly
	coded. It use fake inputs and then compares the expected
	result with the result given by the function '''

import numpy as np
import cmath
from td2d.evolve import evolve_psi
from td2d.constants import HBAR
from td2d.dummy import ham_sq_latt

#-------------------------
# Define Fake  parameters
#-------------------------
# Energy grid
n_grid = 2;
energy = np.zeros((n_grid))
energy[0] = 4
energy[1] = 4

# Testing Hamiltonian
ham = ham_sq_latt(1, 3, 1)

# Testing xi vectors
xi = np.ones((3, 3))
xi[0, 0] = 0
xi[1, 1] = 0
xi[2, 2] = 0

# Testing psi_old vector
psi_old = np.zeros((3, 3, n_grid))
psi_old[:, :, 0] = np.identity(3)
psi_old[:, :, 1] = np.identity(3)

# Testing omg_old vectors
omg_old = np.zeros((4, 3, 3, n_grid))
for a in range(0, 3):
	for b in range(0, 3):
		omg_old[0, a, b, 0] = 1
		omg_old[2, a, b, 0] = 3
		omg_old[0, a, b, 1] = 1
		omg_old[2, a, b, 1] = 3

# J and K parameters
J=1
K=1

# Define the three expected terms
grd_pt = 1
T1 = ham - energy[grd_pt]*np.identity(3)
T2 = 8*np.ones((3, 3))
T3 = xi

#---------------------
# Compare the results
#---------------------
# Expected result
result_exp = (-1j/HBAR)*(T1 + J*T2 + K*T3)

#print(result_exp)
# Result from the function
result_func = evolve_psi(energy, psi_old, omg_old, sys, args_ls, xi, J, K)

print(result_func[:, :, grd_pt] - result_exp)

