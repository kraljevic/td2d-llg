import numpy as np
from td2d.measure import Measure
from td2d.kwant_wrapper import lead_and_channel_finder

import matplotlib.pyplot as plt

# Testing energy grid
energy = np.arange(-4, 4+0.1, 0.1)
print(energy)

# Testing semi-circles and their centers
xi_cent  = np.array([-0.5, 1, 1.5])

# Testing lead chemical potential
mu_leads = np.array([1, 2])

# Testing semi circle function
#rs = Measure.semi_circle(3, 0, 1)
#print(rs)

# Testing lead_and-channel_finder
lead_no, lead_ch_no = lead_and_channel_finder(2)
print('lead number is ', lead_no)
print('lead channel number is ', lead_ch_no)

# Testing the function now
channel_fn = Measure.channel_weight(energy, xi_cent, mu_leads, 1, 1000, 0, 0)

# Plotting the results
fig = plt.figure()
gs = fig.add_gridspec(1,1)
ax = fig.add_subplot(gs[0,0])

ax.plot(energy, channel_fn[0, :])
ax.plot(energy, channel_fn[1, :])
ax.plot(energy, channel_fn[2, :])
fig.savefig('./results_pdf/channel_weight.pdf',
			 bbox_inches='tight')
