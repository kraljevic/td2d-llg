import numpy as np
import cmath

from td2d.evolve import dmat, evolve_hybrid, un_hybridize, evolve_psi, evolve_omg
from td2d.dummy import ham_sq_latt
from td2d.kwant_wrapper import create_ham as ch
from td2d.kwant_wrapper import get_xi as get_xi

import time
import matplotlib.pyplot as plt

#---------------------------
# Define Testing parameters
#---------------------------
nc_tot = 2   # no. of channels
m_max = 4  # size of omg vectors
n_grid = 10  # energy grid points

J = 0
K = 0

N = 2	    # number of sites
gamma = 1   # hopping in lead

# Random Energy grid
energy = np.random.rand(n_grid)

# Testing paremetric Hamiltonian
sys = ch(plot_sys=True)
my_ham = sys.hamiltonian_submatrix(args=([0.1, 0.2]))

# Testing xi vectors (they dont matter because K=0)
xi, xi_cent = get_xi(sys, args=([0.1, 0.2]))
xi = np.asarray(xi)
xi = xi.T

# Random psi_old vector
psi_old = np.random.rand(N, nc_tot, n_grid)

# Random omg_old vectors
omg_old = np.random.rand(m_max, nc_tot, nc_tot, n_grid)

# Dmatrix
d_mat = dmat(m_max)

#args
args_ls=([0.1, 0.1])

# Construct RHS from two methods and compare

hyb = evolve_hybrid(energy, psi_old, omg_old, sys, args_ls, xi, xi_cent, d_mat, gamma, J, K)
psi1, omg1 = un_hybridize(hyb, N, nc_tot, n_grid, m_max)

psi2 = evolve_psi(energy, psi_old, omg_old, sys, args_ls, xi, J, K)
omg2 = evolve_omg(energy, psi_old, omg_old, xi,  xi_cent, d_mat, gamma, J)

rtol = 1e-08
atol = 1e-011

print('-----------------------------------')
print('  Results for psi and omega ')
print('-----------------------------------')
print(np.allclose(psi2, psi1, rtol, atol))
print(np.allclose(omg2, omg1, rtol, atol))

