clc
clear all

% Variables
gamma = 1;
hbar = 0.658211951;
epsilon_max = 2;
tol = 10^(-3);

% Build decay part of the kernel
Ti = 0;
Tf = 40;
dT = 0.1;
T = Ti:dT:Tf;

IofT = (besselj(0,2*T*gamma/hbar) ...
                + besselj(2,2*T*gamma/hbar));

subplot(2,2,1)            
plot(T,IofT) 
xlim([Ti,Tf])
xlabel('time (fs)')
ylabel('I(t)')
xlim([0 20])

subplot(2,2,2)            
semilogy(T,abs(IofT), [Ti, Tf],[1, 1]*tol) 
xlim([Ti,Tf])
xlabel('time (fs)')
ylabel('I(t)')

% Build the oscillating part of the kernel
osc_rel = real(exp(-1i*epsilon_max*T/hbar));
osc_img = imag(exp(-1i*epsilon_max*T/hbar));

subplot(2,2,3)            
plot(T,osc_rel,'-*') 
xlim([0,2*pi*hbar/epsilon_max])
xlabel('time (fs)')
ylabel('O(t) (real)')

subplot(2,2,4)            
plot(T,osc_img,'-*') 
xlim([0,2*pi*hbar/epsilon_max])
xlabel('time (fs)')
ylabel('O(t) (imag)')