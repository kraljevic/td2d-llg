''' This script tests if 'evolve_omg' function is correctly
	coded. It use fake inputs and then compares the expected
	result with the result given by the function '''

import numpy as np
import cmath
from td2d.evolve import evolve_omg, dmat
from td2d.constants import HBAR
from td2d.dummy import ham_sq_latt

#-------------------------
# Define Fake  parameters
#-------------------------
# Energy grid points
n_grid = 2
nc_tot = 3
m_max  = 4
dim_h  = 3

# Testing energy
energy = np.random.rand(n_grid)

# Testing psi_old vector
psi_old = np.random.rand(dim_h, nc_tot, n_grid)

# Testing xi vectors
xi = np.random.rand(nc_tot, nc_tot)

# Testing omg_old vectors
omg_old = np.random.rand(m_max, nc_tot, nc_tot, n_grid)

# d-matrix
d_mat = dmat(m_max)

# Centers of semicircles
xi_cent = 2*np.ones((nc_tot))

# Gamma parameter
gamma = 1.0

# J parameter
J=1

# Define the parameters to select omg
a=0
b=2
g=1

# Define the expected RHS
omg_rhs_exp = (xi_cent[b]-energy[g])*omg_old[:, a, b, g] \
			+ 1j*gamma*np.dot(d_mat, omg_old[:, a, b, g])

omg_rhs_exp[0] = omg_rhs_exp[0] + J*gamma**2*np.dot(xi[:,b].T,psi_old[:,a,g])
omg_rhs_exp = omg_rhs_exp/(1j*HBAR)
omg_rhs_func = evolve_omg(energy, psi_old, omg_old, xi,  xi_cent, d_mat, gamma, J)

print('-----------------')
print('Expected Result')
print('------------------------')
print(omg_rhs_exp.real)
print(omg_rhs_exp.imag)
print('------------------------')
print('Results From function')
print('------------------------')
print(omg_rhs_func[:, a, b, g].real)
print(omg_rhs_func[:, a, b, g].imag)

print('----------------')
print(' Result of Test')
print('----------------')
res = np.allclose(omg_rhs_exp, omg_rhs_func[:, a, b, g])
if res:
	print('Passed')
else:
	print('Failed')

