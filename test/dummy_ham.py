from td2d.dummy import  ham_sq_latt, xi_vec
from td2d.qm import ket, bra

# Hamiltonian of a square lattice
nx = 1;
ny = 2;
gamma = 1;
H = ham_sq_latt(nx, ny, gamma)
print(H)


# test braket ket function
A = ket(0,3)
print(A)

B = bra(1,4)
print(B)

# test xi vectors
xi = xi_vec(5, 8)
print(xi)

