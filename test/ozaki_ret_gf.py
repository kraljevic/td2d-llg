from td2d.ozaki import Ozaki
from td2d.kwant_systems import make_ribbon
from td2d.kwant_wrapper import get_ham as get_h

import numpy as np

# Define system
sys = make_ribbon(length=2, width=1, tsoc=0, plot_sys=False,spin=False)
sys_args = ([0.1, 0.1])

# variables
efermi = 0

# GF
gf = Ozaki.ret_gf(sys, sys_args, efermi)
