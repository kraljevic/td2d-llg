import numpy as np
from td2d.measure import Measure
from td2d.kwant_systems import make_ribbon

# Density matrix
rho = np.zeros((2, 2), dtype=complex)
rho[0, 1] = 1j
rho[1, 0] = -1j
rho = np.kron(rho, np.identity(2))

# Add spin current parameters
delta = 0.5
rho[0, 2] = rho[0, 2] + 1j*delta
rho[1, 3] = rho[1, 3] - 1j*delta
rho[2, 0] = rho[0, 2] - 1j*delta
rho[3, 1] = rho[3, 1] + 1j*delta

rho[0, 3] = delta
rho[1, 2] = delta
rho[3, 0] = delta
rho[2, 1] = delta

# System (1D chain)
sys = make_ribbon(length=2, width=1, spin=True )
sys_args=([0.1, 0.6])
bonds = [(0, 1)]
spj_fn = Measure.local_sp_currents(rho, sys, sys_args, *bonds, where='ij')

# Expected results
spj_exp = -4*np.pi*np.array([0, 0, 1])
# Test results
print('-----------------')
print('Spin currents ')
print('------------------')
print('Expected result ')
print(spj_exp)

print('Result from function')
print(spj_fn)

res = np.allclose(spj_exp, spj_fn)
if res:
    print('Passed')
else:
    print('Failed')







