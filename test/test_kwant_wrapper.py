from td2d.kwant_systems import make_ribbon, rice_mele_light, spin_pump
from td2d.kwant_wrapper import get_ham as get_h
from td2d.kwant_wrapper import get_xi as get_xi
import numpy as np

print('Building system')
sys = make_ribbon(length=2, width=1, tsoc=0, plot_sys=False,spin=False)
args_ls = ([0.1, 0.1])

print('getting Hamiltonian')
ham = get_h(sys, args=args_ls, sparse=False)

print('getting xi vectors')
xi, xi_cent = get_xi(sys, args=args_ls)

print('------------')
print('Hamiltonian : Real')
print('------------')
print(ham.real)

print('------------')
print('Hamiltonian : Imag')
print('------------')
print(ham.imag)

print('------------')
print('xi vectors')
print('-------------')
print(xi.shape)
print(xi)

print('-------------')
print('xi centers')
print('-------------')
print(xi_cent)

# Checking some basic stuff
lead_index, lead  = sys.leads
print('--------')
print(lead_index)
print('-------')
print(lead)
print(lead.selfenergy(energy=0, args=args_ls))
