import numpy as np
from td2d.measure import Measure
from td2d.kwant_systems import make_ribbon

rho1 = np.zeros((4, 4), dtype=complex)
rho1[0, 1] = 1j
rho1[1, 0] = -1j
rho1[1, 2] = 2j
rho1[2, 1] = -2j

sys = make_ribbon(length=4, width=1)
sys_args=([0.1, 0.6])
bonds = [(0, 1), (1, 2)]
currents_fn = Measure.local_ch_currents(rho1, sys, sys_args, *bonds,
									 where='ij',
									 spin_full=False)

currents_exp = -np.array([2, 4])*2*np.pi

print('------------------')
print('Spin less current')
print('------------------')
print('Expected result ')
print(currents_exp)

print('Result from function')
print(currents_fn)

res = np.allclose(currents_exp, currents_fn)
if res:
    print('Passed')
else:
    print('Failed')

rho2 = np.kron(rho1, np.identity(2))
sys = make_ribbon(length=4, width=1, spin=True)

currents_fn_sp = Measure.local_ch_currents(rho2, sys, sys_args, *bonds,
                                     where='ij',
                                     spin_full=True)


currents_exp_sp = 2*currents_exp
print('-----------------')
print('Spin full current')
print('------------------')
print('Expected result ')
print(currents_exp_sp)

print('Result from function')
print(currents_fn_sp)

res = np.allclose(currents_exp_sp, currents_fn_sp)
if res:
    print('Passed')
else:
    print('Failed')







