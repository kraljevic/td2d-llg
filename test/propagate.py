import numpy as np
import cmath

from td2d.evolve import dmat, propagate
from td2d.initiate import init_eom_obj
from td2d.dummy import ham_sq_latt
from td2d.constants import HBAR
from td2d.kwant_systems import make_ribbon as ch
from td2d.kwant_wrapper import get_xi as get_xi

import time
import matplotlib.pyplot as plt

#---------------------------
# Define Testing parameters
#---------------------------
nc_tot = 2   # no. of channels
m_max = 4    # size of omg vectors
n_grid = 10  # energy grid points

J = 0
K = 0

N = 2       # number of sites
gamma = 1   # hopping in lead
ti = 0      # inital time
tf = 20     # final time
dt = 0.1   # small time step

# time vector
time = np.arange(ti, tf+dt, dt)
dt_chk = time[1]-time[0]
step_tot = len(time)

print('time step: ', dt_chk)
print('no. of tot steps:', step_tot)
print('t_ini: ', time[0])
print('t_fin: ', time[step_tot-1])
print('------------------------')

# Energy grid
energy = np.zeros((n_grid))
energy[0] = 4
energy[1] = 2

# Testing paremetric Hamiltonian
sys = ch(length=2, width=3, spin=False, plot_sys=True)
my_ham = sys.hamiltonian_submatrix(args=([0.1, 0.2]))
dim_h, dim = np.shape(my_ham)

print('Hamiltonian')
print(my_ham.real)
print(my_ham.imag)
print('-------------------------')

# Testing xi vectors (they dont matter because K=0)
xi, xi_cent = get_xi(sys, args=([0.1, 0.2]))
print(xi)

xi = np.asarray(xi)
xi = xi.T

print(xi)

# Initalize EOM objects
solver = 'naive_solver'
solver_init_args = [m_max]

eom_obj = init_eom_obj(energy,
                       dim_h,
                       nc_tot,
                       solver,
                       solver_init_args,
                       verbose=True)


# Pull out the individual components to set some desired values
# for test example
psi_old = eom_obj[0]
psi_old[0, :, :] = 1
omg_old = np.ones((m_max, nc_tot, nc_tot, n_grid), dtype=complex)

# Put them back into eom_obj
eom_obj_old = [psi_old, omg_old]

# Dmatrix
d_mat = dmat(m_max)

# Variables for measurements
which_element = 0
which_channel = 0
which_grd_pt = 1
phys_qt = np.zeros((step_tot), dtype=complex)
phys_qt[0] = psi_old[which_element,
                    which_channel,
                    which_grd_pt] # Sets the inital conditon into your output

args_ls = ([0.1, 0.2])

#-----------
# Time loop
#-----------
solver_args = [d_mat, gamma, J, K]

for step in range(step_tot-1):
    t = time[step]

    # Find the new eom_obj
    eom_obj_new = propagate(eom_obj_old,
                            energy,
                            sys,
                            args_ls,
                            xi,
                            xi_cent,
                            t, dt,
                            solver,
                            solver_args)

    # Make measuremets (in this case no fancy way of evaluating)
    # Just pick the required element and save it (analytical solution exists)
    psi_new = eom_obj_new[0]
    phys_qt[step+1] = psi_new[which_element, \
                            which_channel, \
                            which_grd_pt]

    # Update the eom_obj
    eom_obj_old = eom_obj_new


# Analytical solution
E = energy[which_grd_pt]
analytical = np.multiply(np.cos((gamma/HBAR)*time), np.cos((E/HBAR)*time))

#-----------------------------
# Plotting numerical solution
#-----------------------------
fig, axs = plt.subplots(2)

axs[0].plot(time, analytical,'-',label='Analytical')
axs[0].plot(time, np.real(phys_qt), ':',label='Numerical')
axs[0].set_ylabel('C_1(t)')
axs[0].legend(loc='upper right')
axs[0].set_ylim(-1, 2)
axs[0].set_xlim(ti, tf)

axs[1].plot(time, np.real(phys_qt)-analytical,'-')
axs[1].set_xlabel('time (fs)')
axs[1].set_ylabel('Error')


plt.savefig('./results_pdf/propagate.pdf')
