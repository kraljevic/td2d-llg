import numpy as np
from scipy import linalg
import warnings
import matplotlib
import matplotlib.pyplot as plt
warnings.filterwarnings("ignore")
matplotlib.use('Agg')


def get_ham(sysf, args=(), sparse=False, verbose=False):
    """A wrapper of the KWANT's hamiltonian_submatrix method
    finalized

    Parameters
    ----------
    sysf : kwant.Builder.FiniteSystem
        KWANT tight-binding system from which to extract the
        Hamiltonian.
    args : list or tuple
        Parameters of the Hamiltonian required accoriding
        the created system. To be passed to KWANT's 'args'
        parameter in the hamiltonian.
    sparse : bool, optional
        Return the Hamiltonian as a sparse or full matrix
        (default is False).
    verbose : bool, optional
        Print completed steps (default is False).

    Returns
    -------
    ham : two-dimensional numpy ndarray or sparse matrix
        The system Hamiltonian in sparse or full form.
    """
    args = list(args)
    ham = sysf.hamiltonian_submatrix(args=args, sparse=sparse)
    if verbose:
        print("Hamiltonian obtained from system given")
    return ham


def get_xi(sysf, args=(), e_f=0.1, plot_gamma_all=False,
           plot_gamma_ordered=False, verbose=False):
    """A function to obtain the xi vectors and xi_centers

    Parameters
    ----------
    sysf : kwant.Builder.FiniteSystem
        KWANT tight-binding system.
    args : list or tuple
        Parameters of the Hamiltonian required accoriding
        the created system. To be passed to KWANT's 'args'
        parameter in the selfenergy function.
    ef : float, optional
        Energy at which to evaluate the eigenvectors
        of gamma matrices (default is 0.1).
    sparse : bool, optional
        Return the Hamiltonian as a sparse or full matrix
        (default is False).
    plot_gamma_all : bool, optional
        Plot all lead selfenergies (default is False).
    plot_gamm_ordered : bool, optional
        Plot first two to verify ordering. Will fail if
        lead doesn't have at least two channels
        (default is False).
    verbose : bool, optional
        Print completed steps (default is False).

    Returns
    -------
    xi : ndarray
        List containing the xi vectors in the order of the
        corresponding centers of semicircles from min to max.
    xi_cent : list
        List containing the centers of the semicircles.
    lead_info : array
        array containing the lead information
    """

    H_dim = len(sysf.hamiltonian_submatrix(args=args))

    xi = []
    xi_cent = []
    for lead_index, lead in enumerate(sysf.leads):

        intf = sysf.lead_interfaces[lead_index]
        energies = np.linspace(-5, 5, 100)

        sigma = lead.selfenergy(energy=e_f, args=args)

        num_ch = len(sigma.imag)
        spin_d = int(num_ch/len(intf))

        if spin_d == 2:
            # contraction of gamma matrix to obtain spinless,
            # to remove degenracies for getting proper eigen
            # vectors
            gamma_spin = sigma.imag
            d_spin = len(gamma_spin)
            d = int(d_spin/2)
            gamma = np.zeros((d, d))
            p = 0
            for i in np.arange(0, d_spin, 2):
                q = 0
                for j in np.arange(0, d_spin, 2):
                    gamma[p, q] = gamma_spin[i, j]
                    q = q + 1
                p = p + 1

            e_vals, e_vecs = linalg.eigh(gamma)
            # extending the gamma vectors to full dimension
            e_vecs = np.kron(e_vecs, np.eye(2))
        else:
            e_vals, e_vecs = linalg.eigh(sigma.imag)

        cell_H = lead.cell_hamiltonian(args=args)
        xi_cent.extend(linalg.eigh(cell_H, eigvals_only=True))

        semi_circ = np.zeros((len(energies), num_ch))

        for e_index, energy in enumerate(energies):
            sigma = lead.selfenergy(energy, args=args)
            gamma = sigma.imag

            for ch_index in range(num_ch):

                # Unitary transforation to get eigenvalues
                # at different energies without mixing.
                semi_circ[e_index][ch_index] = (
                    np.dot(e_vecs[:, ch_index].T,
                           np.dot(gamma, e_vecs[:, ch_index])))

        sort_id = np.zeros(num_ch)

        for ch_index in range(num_ch):
            # Get the argument index of max of each semicircles
            temp = int(np.argmax(abs(semi_circ[:, ch_index])))
            sort_id[ch_index] = temp

        # Get the sorted index(idx)
        idx = np.argsort(sort_id)
        xi_vec = []

        for sort_index in idx:
            xi_vec.append(e_vecs[:, sort_index])

        for ch_index in range(num_ch):

            xi_lead = np.zeros(H_dim)

            for intf_index, intf_pos in enumerate(intf):
                # Positioning the eigen vector to xi vector
                # according to the numbering of the interface sites.
                imin = spin_d * intf_pos
                imax = spin_d * (intf_pos + 1)
                jmin = spin_d * intf_index
                jmax = spin_d * (intf_index + 1)
                xi_lead[imin:imax] = xi_vec[ch_index][jmin:jmax]

            xi.append(xi_lead)

        if plot_gamma_all:
            for ch_index in range(num_ch):
                plt.plot(energies, -semi_circ[:, ch_index])
            out_file = 'lead_' + str(lead_index) + '_gamma_all.pdf'
            plt.savefig(out_file)
        if plot_gamma_ordered:
            plt.plot(energies, -semi_circ[:, idx[0]], 'r', energies,
                     -semi_circ[:, idx[1]], 'g')
            out_file = 'lead_' + str(lead_index) + '_gamma_2s_rg.pdf'
            plt.savefig(out_file)

    xi = np.asarray(xi)
    xi = xi.T

    xi_cent = np.asarray(xi_cent)

    lead_info = []
    for lead_index, lead in enumerate(sysf.leads):
        lead_ch = len(lead.cell_hamiltonian(args=args))
        channels = np.arange(lead_ch)
        lead_info.extend(channels)
    lead_info = np.asarray(lead_info)

    return xi, xi_cent, lead_info


def lead_and_channel_finder(ch, sys, sys_args):
    """
    Function to indentify the lead and channel number when
    the serialized channel number is provided.

    Parameters
    ----------
    ch: integer
        The serialized channel number.
    sys: Kwant.builder.system
        The system we are working with.
    sys_args: list of arguments needed for the Hamiltonian

    Returns
    -------
    lead_no: integer
        The lead to which the channel belongs
    lead_ch_no: integer
        The channel number within the lead.
    """
    # TODO: Abhin should write this function. May not be
    # the best way.

    ch_scanned = 0

    for lead_index, lead in enumerate(sys.leads):
        # Count number of channels in the present lead
        sys_args = list(sys_args)
        cell_ham = lead.cell_hamiltonian(args=sys_args)
        lead_chs = np.shape(cell_ham)[0]
        ch_scanned = ch_scanned + lead_chs
        if ch <= ch_scanned-1:
            lead_no = lead_index
            lead_ch_no = lead_chs - (ch_scanned - (ch+1)) - 1
            break

    return lead_no, lead_ch_no


def lead_ch_finder(ch, lead_info):
    """Function to indentify the lead and lead channel
    number from channel number. This numbering includes zero

    Parameters
    ----------
    ch: integer
        The channel number.
    lead_info: array
        The information regarding the leads.

    Returns
    -------
    lead_no: int
            Returns the lead number.
    lead_ch_no: int
                Returns the lead channel number.

    """
    lead_ch_no = lead_info[ch]

    lead_no = np.count_nonzero(lead_info[:ch] == 0)

    return lead_no, lead_ch_no
