from .device import Device
import numpy as np
from .constants import HBAR
from .constants import KB
from .kwant_wrapper import lead_and_channel_finder as lc_find
from scipy.special import jv


def fermi_fn(x):
    return 1. / (1 + np.exp(x))


class Memory:

    def __init__(self, device, emin=-2.1, emax=2.1,
                 estep=0.01, temp_k=100.0, efermi=0.0,
                 dt=0.1, gamma=1.0, verbose=False):
        '''A simple propagation solver for TD-NEGF based on
        RK4 method

        Parameters
        ----------
        device : an instance of the Device class
            The device whose state we want to evolve in time.
            The solver needs this so it can extract the
            system hamiltonian and other usefull parameters.
        emin : float (optional)
            The bottom of the band. The solver needs this
            so it can compute the energy integral for the
            rho matrices. Default value -2.1.
        emax : float (optional)
            The top of the band. The solver needs this
            so it can compute the energy integral for the
            rho matrices. Default value 2.1.
        estep : float (optional)
            The energy step used in integration of the
            rho matrices. Default value 0.01
        temp_k : float (optional)
            The temperature of the system in Kelvins (K).
            Default value 100.
        efermi : float (optional)
            The Fermi energy at which to perform the time
            propagation. Default value 0.0.
        nt : integer (optional)
            The dimension of omega vectors. It determines
            the maximum time interval where the solver
            produces acceptable results.
        gamma : float (optional)
            Hopping integral from the system to the leads.
            Leads are always assumed to be square lattice
            with uniform hopping to the system.
        weight : float (optional)
            Weight of the energy points depending upon
            the integration scheme used to do the energy
            integral. Default value is 0.01.
        verbose : boolean (optional)
            Print computation times necessary for profiling.
        '''

        assert isinstance(device, Device), (
            'The suplied object under device argument is '
            'not and instance of the Device class.')

        times = np.arange(0, 20, dt)
        nt = len(times)
        print('LEN', nt)
        self.nt = nt
        self.emin = emin
        self.emax = emax
        self.estep = estep
        self.egrid = np.arange(emin, emax+estep, estep)
        self.ne_grid = len(self.egrid)
        self.temp_k = temp_k
        self.efermi = efermi
        self.dt = dt
        self.dim_h = device.dim_h
        self.nc_tot = device.nc_tot
        self.gamma = gamma
        self.device = device
        self.verbose = verbose
        self.w = estep * np.ones(self.ne_grid)

        self.psi = np.zeros((self.dim_h, self.nc_tot,
                             self.ne_grid), dtype=complex)

        self.xi_psi = np.zeros((self.nc_tot,
                               self.nc_tot, self.ne_grid,
                               nt), dtype=complex)
        self.omega = np.zeros((self.nc_tot, self.nc_tot,
                              self.ne_grid), dtype=complex)

        ctimes = 2 * gamma * times / HBAR
        exp_times = -1j * times / HBAR

        self.kernel = np.zeros((self.nt, self.nc_tot),
                               dtype=complex)

        self.oker = np.zeros((self.nt, self.nc_tot),
                             dtype=complex)

        jterm = jv(1, ctimes) + jv(3, ctimes)
        jterm *= -(gamma**3/HBAR)
        jkt = jv(0, ctimes) + jv(2, ctimes)
        jkt *= (gamma**3/HBAR)
        for i in range(self.nc_tot):
            eps = np.exp(self.device.xi_cent[i] * exp_times)
            self.kernel[:, i] = jterm * eps
            self.oker[:, i] = jkt * eps

    def evolve_psi(self, time, psi, omega):

        ham = self.device.get_h(time)
        xic = np.array(self.device.xi)

        t1 = np.einsum('ij,jkl', ham, psi, optimize='greedy')
        t3 = np.einsum('ib,kbl', xic, omega,
                       optimize='greedy')

        t4 = np.zeros(self.psi.shape, dtype=complex)
        for i, en in enumerate(self.egrid):
            for ch in range(self.nc_tot):
                econst = np.exp(-1j*en*time/HBAR)
                t4[:, ch, i] = econst*self.device.xi[:, ch]

        psi_rhs = t1 + t3 + t4

        psi_rhs *= (-1j/HBAR)
        return psi_rhs

    def update_xi_psi(self, psi):
        xi_psi_temp = np.array(self.xi_psi)
        xi_psi = 0*np.array(self.xi_psi)
        xi_psi[:, :, :, 1:] = xi_psi_temp[:, :, :, 0:-1]

        for i in range(self.nc_tot):
            for j in range(self.nc_tot):
                left = np.conj(self.device.xi[:, i])
                right = psi[:, j, :]
                xi_psi[i, j, :, 0] = np.dot(left, right)
        return xi_psi

    def evolve_memory(self, omega, psi, dt_last):
        xi_psi = self.update_xi_psi(psi)
        domega = 0*self.omega
        for i in range(self.nc_tot):
            for j in range(self.nc_tot):
                dij = self.kernel[:, i]*xi_psi[i, j, :, :]
                dij[:, 1:] = dij[:, 1:] * self.dt
                dij[:, 0] = dij[:, 0] * dt_last
                domega[i, j, :] = dij.sum(axis=1)
        return domega

    def evolve_omg(self, time, psi, omega, dt_last,
                   include=False):
        t1 = np.array(omega)
        for index, xi_cent in enumerate(self.device.xi_cent):
            t1[:, index, :] *= xi_cent

        if include:
            t3 = self.evolve_memory(omega, psi, dt_last)
        else:
            t3 = 0

        omg_rhs = t1 + t3
        t4 = np.einsum('sk,sjl', np.conj(self.device.xi),
                       psi)
        t4 *= self.gamma**2
        omg_rhs = omg_rhs + t4
        omg_rhs *= (-1j/HBAR)
        return omg_rhs

    def propagate(self, time):

        dt = self.dt
        dp1 = self.evolve_psi(time, self.psi, self.omega)*dt
        do1 = self.evolve_omg(time, self.psi, self.omega,
                              dt_last=dt, include=True)*dt

        psi = self.psi + dp1/2
        omega = self.omega + do1/2

        dp2 = self.evolve_psi(time, psi, omega)*dt
        do2 = self.evolve_omg(time, psi, omega,
                              dt_last=dt, include=True)*dt

        self.psi = self.psi + dp2
        self.omega = self.omega + do2

        self.xi_psi = self.update_xi_psi(self.psi)

        return

    def semi_circle(self, center):
        """Return a semi-circle representing diagonalised
        selfenergy (an energy of a eigen-channel)
        """

        semi = np.zeros(self.ne_grid)
        for index, en in enumerate(self.egrid):
            up_limit = en > (center + 2*self.gamma)
            dw_limit = en < (center - 2*self.gamma)

            if up_limit or dw_limit:
                semi[index] = 0.
            else:
                val = 4.*self.gamma**2 - (en - center)**2
                semi[index] = np.sqrt(val)
        return semi

    def channel_weight(self, time):
        """
        Returns the weight W(E) which multiplies to
        |psi><psi| in energy integration.

        Returns
        -------
        channel_fn: 2D numpy array
            Contains the 2D array weight of the all channels
            as a function of energy. The first index runs on
            channels and the second one on the energy grid.
        """

        # Allocate space for the output channel weight
        ch_fn = np.zeros((self.nc_tot, self.ne_grid))

        for ch in range(self.nc_tot):

            args = self.device.get_args(time)
            mu_leads = self.device.get_mu(time)
            sys = self.device.sys
            # Find the lead and respective channel number
            ld_no, ld_ch_no = lc_find(ch, sys, args)

            # Find the chemical potential of the lead the
            # "ch" channel belongs to
            mu = mu_leads[ld_no]

            # Now populate the output function with it
            x = (self.egrid - mu) / (KB * self.temp_k)
            fermi_term = fermi_fn(x)

            cent = self.device.xi_cent[ch]
            circle_term = self.semi_circle(cent)

            ch_fn[ch, :] = fermi_term * circle_term

        return ch_fn

    def rho(self, time):
        """
        Function to evaluate the nonequilibrium
        density matrix
        """

        psi_conj = np.conj(self.psi)

        # Create the function which multiplies to the
        # psi functions

        ch_fn = self.channel_weight(time)

        # now calculate rho using scipy einsum method
        # (rho_ij, c=channel, g=grid)
        rho = np.einsum('icg,jcg,cg,g', self.psi, psi_conj,
                        ch_fn, self.w, optimize='greedy')

        rho = rho * (1./(2*np.pi))

        return rho

    def save_npz(self, filename):
        np.savez(filename, psi=self.psi, omega=self.omega)

    def load_npz(self, filename):
        data = np.load(filename)
        self.omega = data['omega']
        self.psi = data['psi']

    def get_omega(self, xi_psi):
        omega = 0*self.omega
        for i in range(self.nc_tot):
            for j in range(self.nc_tot):
                dij = self.oker[:, i]*xi_psi[i, j, :, :]
                omega[i, j, :] = dij.sum(axis=1)*self.dt
        return omega / (1j*HBAR)
