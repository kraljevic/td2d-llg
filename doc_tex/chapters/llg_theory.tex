\chapter{LLG theory}%
\label{ch:llg_theory}

In this chapter we give a short description on how we solve
the dynamics equation for the local magnetic moments in
the \verb|Spins| class. Additionally, 
we give explanation of each of the keyword parameters in
the \verb|Spins| class, and how they relate to different
terms in the dynamics equation. 

\section{Landau-Lifshitz-Gilbert equation (LLG)}

The dynamics of local moments is
governed by the Landau-Lifshitz-Gilbert (LLG) equation
\begin{equation}
  \frac{\partial\mathbf{S}_{i}}{\partial t} =
  -\frac{\gamma}{1 + {\lambda}^{2}}
  \left[\mathbf{S}_{i} \times \mathbf{H}_{\textrm{eff}}^{i} +
  \lambda \mathbf{S}_{i} \times
  \left(\mathbf{S}_{i} \times \mathbf{H}_{\textrm{eff}}^{i}\right)
  \right]
\end{equation}
Here, $\mathbf{S}_{i}$ is the classical magnetic moment 
(spin) on the $i$-th site, represented as a unit vector,
$\lambda$ is
the Gilbert damping, and $\gamma$ is the gyromagnetic ratio. The
effective magnetic field on each site
$\mathbf{H}_{\textrm{eff}}^{i}$
% [<] Eq. Heff
\begin{equation}
  \mathbf{H}_{\textrm{eff}}^{i} = - \frac{1}{\mu_{s}}
  \frac{\partial H}{\partial \mathbf{S}_{i}},
\end{equation}
% [>] Eq. Heff
is derived from the classical spin Hamiltonian $H$ which 
consists of several terms
% [<] Eq. Hamiltonian
\begin{equation}
  H = H_{\textrm{exc}} + H_{\textrm{sd}} +
      H_{\textrm{B}} + 
      H_{\textrm{ani}} +
      H_{\textrm{dem}}.
\end{equation}
% [>] Eq. Hamiltonian
Note that the current version of TD2D does not implement
the Dzyaloshinskii-Moriya interaction (DMI), but that will
change in the future versions.
The first term is the Heisenberg exchange interaction
between two classical spins
\begin{equation}
  H_{\textrm{exc}} = - \sum_{ij} J_{\textrm{exc}}
                \mathbf{S}_{i} \cdot \mathbf{S}_{j}.
\end{equation}
Next, there is an exchange interaction between electron spin
density and classical spins
\begin{equation}
 H_{\textrm{sd}} = - J_{\textrm{sd}} \sum_{i}
              \langle \hat{\mathbf{s}}_{i} \rangle \cdot
              \mathbf{S}_{i},
\end{equation}
and interaction between classical spins and an external
magnetic field
\begin{equation}
  H_{\textrm{B}} = -\sum_{i} \mu_{\textrm{S}}\,
                    \mathbf{S}_{i} \cdot
              \mathbf{H}_{\textrm{ext}}^{i}.
\end{equation}
Here, the total magnetic moment $\mu_{\textrm{S}}$ is
expressed in the units of Bohr's magneton $\mu_{\textrm{B}}$,
and we will additionally assume that it is equal to it 
$\mu_{\textrm{S}} = \mu_{\textrm{B}}$.
The magnetic anisotropy is considered in its simplest form,
that is as a single atom uniaxial type
\begin{equation}
  {H}_{\textrm{ani}} = - J_{\textrm{ani}} \sum_{i}
                  {(\mathbf{S}_{i} \cdot
                  \mathbf{e}_{\textrm{ani}})}^{2}.
\end{equation}
There is also one additional term related to
demagnetization which has the same form as anisotropy,
but an opposite sign for the coupling constant (an 
easy-plane anisotropy)
\begin{equation}
  {H}_{\textrm{dem}} = J_{\textrm{dem}} \sum_{i}
                  {(\mathbf{S}_{i} \cdot
                  \mathbf{e}_{\textrm{dem}})}^{2}.
\end{equation}
The $\mathbf{e}_{\textrm{ani}}$ and
$\mathbf{e}_{\textrm{dem}}$ are 3D vectors, which determine
the direction of easy-axis anisotropy and the normal vector 
of the easy-plane in the case of easy-plane anisotropy.

We can derive the contribution of each of these terms to
the effective magnetic field.
The first term is for the Heisenberg exchange 
interaction 
% [<] B_exc
\begin{equation}
 \mathbf{H}_{\textrm{exc}}^{i} = 
 \frac{1}{\mu_{\textrm{B}}}
   J_{\textrm{exc}}
   \sum_{j}\mathbf{S}_{j}.
\end{equation}
% [>] B_exc
Here the sum in $j$ goes only over the nearest neighbors of
the $i$-th spin. The effective field created by electron
spin density is
% [<] Eq. Bsd
\begin{equation}
 \mathbf{H}_{\textrm{sd}}^{i} = \frac{1}{\mu_{\textrm{B}}}
   J_{\textrm{sd}}
   \langle{\hat{\mathbf{s}}}_{i}\rangle.
\end{equation}
% [>] Eq. Bsd
The last two terms are related to magnetic anisotropy
% [<] Eq. Bani
\begin{equation}
  \mathbf{H}_{\textrm{ani}}^{i} =
  \frac{2}{\mu_{\textrm{B}}}
    J_{\textrm{ani}}\left(\mathbf{S}_{i}
    \cdot{\boldsymbol e}_{\textrm{ani}}\right)
    {\boldsymbol e}_{\textrm{ani}}.
\end{equation}
% [>] Eq. Bani
and demagnetization
\begin{equation}
  \mathbf{H}_{\textrm{dem}}^{i} =
  -\frac{2}{\mu_{\textrm{B}}}
    J_{\textrm{dem}}\left(\mathbf{S}_{i}
    \cdot{\boldsymbol e}_{\textrm{dem}}\right)
    {\boldsymbol e}_{\textrm{dem}}.
\end{equation}
External magnetic field can be included as it is
$\mathbf{H}^{i}_{\textrm{ext}}$.
Finally, the complete expression for the effective
field at a site $i$ can be written as a sum of all the 
previous effective fields.
% [<] Eq. HEFF
\begin{eqnarray}
 \label{eqHEFF}
 \mathbf{H}_{\textrm{eff}}^{i}
    & = & \frac{1}{\mu_{B}}
        \left[J_{\textrm{exc}} \sum_j \mathbf{S}_{j}+
        J_{\textrm{sd}}
        \langle{\hat{\mathbf{s}}}_{i}\rangle +
        \mu_{B}\mathbf{H}_{\textrm{ext}}^{i} +
        2J_{\textrm{ani}}\left(\mathbf{S}_{i}\cdot 
        {\boldsymbol e}_{\textrm{ani}}\right)
        {\boldsymbol e}_{\textrm{ani}}
        -
        2J_{\textrm{dem}}\left(\mathbf{S}_{i}\cdot 
        {\boldsymbol e}_{\textrm{dem}}\right)
        {\boldsymbol e}_{\textrm{dem}}
        \right] \nonumber.
\end{eqnarray}
All these parameters can be set using the \verb|Spins|
class as described in the introductory chapters. 
The $J_{\textrm{exc}}$ can be set using \verb|jexc| keyword
argument in case of a uniform exchange coupling. For
exchange coupling that depends on both sites $i$ and $j$,
a function \verb|jexc_func| can be provided as a keyword
argument to the \verb|Spins| class. In a similar way,
the $J_{\textrm{sd}}$ can be provided as a 
\verb|jsd_to_llg| and \verb|jsd_to_llg_func|, and 
$J_{\textrm{ani}}$ and $J_{\textrm{dem}}$ can be defined
using \verb|ani| and \verb|demag| parameters, while
$\mathbf{e}_{\textrm{ani}}$ and 
$\mathbf{e}_{\textrm{dem}}$ are defined through 
\verb|ani_vec| and \verb|demag_vec|. Additionally, a user
can define a space-varying anisotropy and demagnetisation
fields through \verb|ani_func| and \verb|demag_func| which
would return  a 3D vector corresponding to and effective
field
$J_{\textrm{ani}}\cdot\mathbf{e}_{\textrm{ani}}$ and
$J_{\textrm{dem}}\cdot \mathbf{e}_{\textrm{dem}}$ at each
site, respectively. If the external magnetic field 
$\mathbf{H}^{i}_{\textrm{ext}}$
is constant and uniform, it can be defined using 
its direction (\verb|bf_vec| parameter),
and its intensity (\verb|bf| parameter).
Additionally, for magnetic field changing in time and
space, a function \verb|bf_func| can be supplied as an
input argument (as explained in Chapter~\ref{ch:spins}).


% [<] Sub. Heun scheme
\section{Time integration: the Heun scheme}

Time integration is implemented with a two-step Heun
scheme. In the first step, we calculate the effective field
on each site, and then we compute the change of local spins 
% [<] Delta S
\begin{eqnarray}
  \Delta \mathbf{S}_{i} & = &
   - \frac{\gamma}{\left(1 + {\lambda}^{2}\right)}
     \left[\mathbf{S}_{i} \times \mathbf{H}_{\textrm{eff}}^{i}
   + 
     \lambda \mathbf{S}_{i} \times
     \left( \mathbf{S}_{i} \times \mathbf{H}_{\textrm{eff}}^{i}
     \right) \right] \Delta t,
\end{eqnarray}
% [>] Delta S
which is then applied to the initial spins
% [<] DS
\begin{equation}
   \label{eqDS}
\mathbf{S}_{i}' = \mathbf{S}_{i} + \Delta \mathbf{S}_{i},
\end{equation}
% [>] DS
and new spins ($\mathbf{S}_{i}'$) are normalized. After this
step, the effective field is recomputed
$\mathbf{H}_{\textrm{eff}}^{i}{'}$, and using this new field and $\mathbf{S}_{i}'$, a new
change in the spin direction is calculated $ \Delta
\mathbf{S}'_{i}$. The final spin after a single time step $\Delta
t$ is obtained as an average of the two spin changes
% [<] Delta S
\begin{equation}
\mathbf{S}^{t+ \Delta t}_{i} = \mathbf{S}_{i} +
\frac{1}{2} \left[ \Delta \mathbf{S}_{i} +
                   \Delta \mathbf{S}_{i}' \right]
\end{equation}
after normalization. The described procedure is equivalent
to a Runge-Kutta method of the second order (RK2).
\section{Note on units and time scales}

All parameters entering equations for the effective field
($J_{\textrm{exc}}$, $J_{\textrm{sd}}$,
$J_{\textrm{ani}}$, $J_{\textrm{dem}}$) are expressed in 
[eV]. The Bohr's magneton
$\mu_{B}$ is in [eV/T], so the effective magnetic field
$\mathbf{H}_{\textrm{eff}}^{i}$ is given in Teslas [T]. Spins
$\mathbf{S}_{i}$ and Gilbert damping $\lambda$ are unitless, and
since gyromagnetic ratio is implemented in the code in
units of [rad/fs$\cdot$T] time is expressed in [fs].
