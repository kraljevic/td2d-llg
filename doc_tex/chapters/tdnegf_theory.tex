\chapter{TD-NEGF theory}
\label{ch:tdnegf_theory}

\section{Solving for two time lesser Green function} 
\label{sec:lesser_GF} 
The lesser Green function (GF) $\bold{G}^<(t,t')$ may be
solvable so let us make an attempt on that
%
\begin{equation}\label{eq:gtt}
\bold{G}^<(t,t') = \int_{-\infty}^{\infty}dt_1 \int_{-\infty}^{\infty}dt_2 \bold{G}^r(t,t_1)\bold{\Sigma}^<(t_1,t_2)\bold{G}^a(t_2,t')
\end{equation}
%
Let us use the Fourier expansion of GF 
\begin{equation}\label{eq:gr_fou}
\bold{G}^r(t,t_1) =  \int_{-\infty}^{\infty} \int_{-\infty}^{\infty}\frac{dEdE_1}{(2\pi\hbar)^2} \bold{G}^r(E,E_1)e^{-iEt/\hbar}e^{iE_1t_1/\hbar}
\end{equation}
\begin{equation}\label{eq:ga_fou}
\bold{G}^a(t_2,t') =  \int_{-\infty}^{\infty} \int_{-\infty}^{\infty}\frac{dE'dE_2}{(2\pi\hbar)^2} \bold{G}^a(E',E_2)e^{iE't'/\hbar}e^{-iE_2t_2/\hbar}
\end{equation}
\begin{equation}\label{eq:self_fou}
\bold{\Sigma}^<(t_1,t_2) = \int_{-\infty}^{\infty}\frac{dE}{2\pi\hbar}\bold{\Sigma}^<(E)e^{-iE(t_1-t_2)/\hbar}
\end{equation}
Applying this transformation to Eq.~\eqref{eq:gtt} results in
\begin{multline}\label{eq:gless_fou}
\bold{G}^<(t,t') =   \int_{-\infty}^{\infty}\int_{-\infty}^{\infty} \int_{-\infty}^{\infty} \frac{d\bar{E}dEdE'}{(2\pi\hbar)^3} \\ \bold{G}^r(E,\bar{E})\bold{\Sigma}^<(\bar{E})\bold{G}^a(\bar{E},E')e^{-iEt/\hbar}e^{iE't'/\hbar}
\end{multline}
Now we know that for 2D lattices we have
%
\begin{equation}
\bold{\Sigma^<}(\bar{E}) = \sum_{\alpha=L}^{R}\sum_{C=1}^{N_c^\alpha} W^{<}_{\alpha C}(\bar{E})\ket{\xi_{\alpha C}(E)}\bra{\xi_{\alpha C}(E)},
\end{equation}
%
Where $W^<_{\alpha C}(E) = if_{\alpha}(E)g_{\alpha C}(E)$ where $f_{\alpha}(E)$ is the Fermi-function of lead $\alpha$ and $g_{\alpha C}(E)$ are obtained by spectral decomposition of the level width matrix i.e.,
\begin{equation}
\bold{\Gamma}_\alpha(E) = \sum_{C=1}^{N_{\alpha C}} g_{\alpha C}(E)\ket{\xi_{\alpha C}(E)}\bra{\xi_{\alpha C}(E)}
\end{equation}
We note that here we are doing the 
\emph{spectral decomposition} of this matrix which makes $\ket{\xi_{\alpha C}(E)}$ orthogonal vectors. From here on we suppress writing the energy dependence in $\ket{\xi_{\alpha C}(E)}$ vectors and explicitly write it down only at required places . Hence we will have in the Eq.~\eqref{eq:gless_fou} 
%
\begin{equation}
\begin{split}
\bold{G}^<(t,t') &=  \sum_{\alpha=L}^{R}\sum_{C=1}^{N_c^\alpha} \int_{-\infty}^{\infty}\int_{-\infty}^{\infty} \int_{-\infty}^{\infty} \frac{d\bar{E}dEdE'}{(2\pi\hbar)^3} \\ &\bold{G}^r(E,\bar{E})\ket{\xi_{\alpha C}}\bra{\xi_{\alpha C}}\bold{G}^a(\bar{E},E')W^{<}_{\alpha C}(\bar{E})e^{-iEt/\hbar}e^{iE't'/\hbar}. \\
&=  \sum_{\alpha=L}^{R}\sum_{C=1}^{N_c^\alpha} \int_{-\infty}^{\infty} \frac{d\bar{E}}{2\pi\hbar} \bold{G}^r(t,\bar{E})\ket{\xi_{\alpha C}}\bra{\xi_{\alpha C}}\bold{G}^a(\bar{E},t')W^{<}_{\alpha C}(\bar{E})
\end{split}
\end{equation}
%
We therefore define the following vectors
%
\begin{equation}\label{eq:ket_def}
\ket{\Psi_{\alpha C}(t,\bar{E})} = \bold{G}^r(t,\bar{E})\ket{\xi_{\alpha C}},
\end{equation}
%
\begin{equation}\label{eq:bra_def}
\bra{\Psi_{\alpha C}(t,\bar{E})} = \bra{\xi_{\alpha C}}\bold{G}^a(\bar{E},t),
\end{equation}
%
Hence we obtain a simple expression for $\bold{G}^<(t,t')$, which is given as
%
\begin{equation}\label{eq:GTT}
\boxed{\bold{G}^<(t,t') = i\sum_{\alpha=L}^{R}\sum_{C=1}^{N_c^\alpha}  \int_{-\infty}^{+\infty} \frac{dE}{2\pi\hbar} \ket{\Psi_{\alpha C}(t,E)} \bra{\Psi_{\alpha C}(t',E)}f_{\alpha}(E)g_{\alpha C}(E)}
\end{equation}
%
Now the question to ask is if we can derive an equation of motion for $\ket{\Psi_{\alpha C}(t,E)}$ which we now refer to as the wave function (WF). If yes, then we can easily construct $\bold{G}^<(t,t')$ which will allows us to calculate even many-body quantum averages. For finding the time-dependent density matrix of the system we have
%
\begin{equation}\label{eq:dmt}
\boxed{\boldsymbol{\rho}(t) = \frac{\hbar\bold{G}^<(t,t)}{i}  = \sum_{\alpha=L}^{R}\sum_{C=1}^{N_c^\alpha}  \int_{-\infty}^{+\infty} \frac{dE}{2\pi} \ket{\Psi_{\alpha C}(t,E)} \bra{\Psi_{\alpha C}(t,E)}f_{\alpha}(E)g_{\alpha C}(E)}
\end{equation}
%
Therefore, finding the density matrix will only amount to solving for $\ket{\Psi_{\alpha C}(t,E)}$ and the integrating it with its outer product.

\section{Equation of Motion of GF and WF} 
\label{sec:eom_gf_wf} 
%=====================================
The equation of motion of the retarded GF is given as the following
%
\begin{equation}
i\hbar\frac{\partial \bold{G}^r(t,t')}{\partial t} = \bold{H}(t)\bold{G}^r(t,t') + \int_{-\infty}^{+\infty} dt''\bold{\Sigma}^r(t-t'') \bold{G}^r(t'',t')  + \delta(t-t').
\end{equation}
%
We now consider the Fourier expansion in the variable $t'$ and multiply the equation with $\ket{\xi_{\alpha C}(E)}$ from the right side to obtain
%
\begin{equation}\label{eq:wf_equation}
i\hbar\frac{\partial \ket{\Psi_{\alpha C}(t,E)}}{\partial t} = \bold{H}(t)\ket{\Psi_{\alpha C}(t,E)} + \int_{-\infty}^{+\infty} \bold{\Sigma}^r(t-t'')\ket{\Psi_{\alpha C}(t'',E)}dt'' 
+ e^{-iEt/\hbar}\ket{\xi_{\alpha C}(E)}.
\end{equation}
%
At this stage we employ the following property of retarded self-energy.
\begin{equation}
\bold{\Sigma}^r(t,t'') = \theta(t-t'')\{ \bold{\Sigma}^>(t,t'') - \bold{\Sigma}^<(t,t'')\},
\end{equation}
%
Inserting this into the above equation we obtain
%
\begin{multline}\label{eq:wf_equation}
i\hbar\frac{\partial \ket{\Psi_{\alpha C}(t,E)}}{\partial t} = \bold{H}(t)\ket{\Psi_{\alpha C}(t,E)} \\ + \int_{-\infty}^{+\infty} \theta(t-t'')\{ \bold{\Sigma}^>(t,t'') - \bold{\Sigma}^<t,t'')\}\ket{\Psi_{\alpha C}(t'',E)}dt''\\
+ e^{-iEt/\hbar}\ket{\xi_{\alpha C}(E)},
\end{multline}
%
which implies
%
\begin{multline}\label{eq:wf_equation}
i\hbar\frac{\partial \ket{\Psi_{\alpha C}(t,E)}}{\partial t} = \bold{H}(t)\ket{\Psi_{\alpha C}(t,E)} \\+ \int_{-\infty}^{t}\{ \bold{\Sigma}^>(t,t'') - \bold{\Sigma}^<(t,t'')\}\ket{\Psi_{\alpha C}(t'',E)}dt'' \\
+ e^{-iEt/\hbar}\ket{\xi_{\alpha C}(E)}.
\end{multline}
%
This presents an equation of motion for the energy-dependent ket vectors $\ket{\Psi_{\alpha C}(t,E)}$. If we can solve for $\ket{\Psi_{\alpha C}(t,E)}$ then it is possible to get access to $\bold{G}^<(t,t')$ with an integration over energy. This can be done in the same spirit as TD-KWANT using MPI. Moving further let us look at the following quantity
%
\begin{equation}\label{eq:less_great}
\begin{split}
\bold{\Sigma}^>(t,t'') - \bold{\Sigma}^<(t,t'') &= \sum_{\alpha=L}^R\ \int_{-\infty}^{\infty}\frac{dE}{2\pi\hbar} [\bold{\Sigma}^>_\alpha(E) - \bold{\Sigma}^<_\alpha(E)] e^{-iE(t-t'')/\hbar} \\
&=  \sum_{\alpha=L}^R \ \int_{-\infty}^{\infty}\frac{dE}{2\pi\hbar}[-i(1-f_{\alpha}(E))\bold{\Gamma}_\alpha(E) -i f_{\alpha}(E)\bold{\Gamma}_\alpha(E)]e^{-iE(t-t'')/\hbar} \\
&= (-i)\sum_{\alpha=L}^{R}  \int_{-\infty}^\infty \frac{dE}{2\pi\hbar} \bold{\Gamma}_\alpha(E)e^{-iE(t-t'')/\hbar}\\
&= (-i)\sum_{\alpha=L}^{R}\bold{\Gamma}_\alpha(t,t'').
\end{split}
\end{equation}
%
Plugging this result back into Eq.~\eqref{eq:wf_equation} we obtain the following result
%
\begin{multline}\label{eq:wf_gam}
i\hbar\frac{\partial \ket{\Psi_{\alpha C}(t,E)}}{\partial t} = \bold{H}(t)\ket{\Psi_{\alpha C}(t,E)} \\ -i \sum_{\alpha=L}^{R}\int_{-\infty}^{t}\bold{\Gamma}(t,t'')\ket{\Psi_{\alpha C}(t'',E)}dt'' \\
+ e^{-iEt/\hbar}\ket{\xi_{\alpha C}(E)}.
\end{multline}


\section{Case of Square Lattice leads} 
\label{sec:sq_lattice} 
%=====================================
In the case of square lattice leads we know that the spectral decomposition of $\bold{\Gamma}(E)$ leads to eigen-vectors that are independent of energy. It is also known that the diagonal elements are semi-circles. This leads to an analytic solution to the $\bold{\Gamma}(t,t'')$. Let us demonstrate that first
%
\begin{align*}
\bold{\Gamma}_{\alpha}(E) = \sum_{C=1}^{N_c^\alpha}g_{\alpha C}(E) \ket{\xi_{\alpha C}}\bra{\xi_{\alpha C}} \hspace{1cm} \mathrm{where} \hspace{1cm} g_{\alpha C}(E) = \sqrt{4\gamma^2 - (E-\epsilon_{\alpha C})^2}.
\end{align*}
%
Then we have
%
\begin{equation}
\begin{split}
\bold{\Gamma}(t,t'') &= \sum_{\alpha=L}^{R} \sum_{C=1}^{N_c^\alpha} \int_{-\infty}^{\infty} \frac{dE}{2\pi\hbar} g_{\alpha C}(E) e^{-iE(t-t'')/\hbar}\ket{\xi_{\alpha C}}\bra{\xi_{\alpha C}}\\ &= \sum_{\alpha=L}^{R} \sum_{C=1}^{N_c^\alpha}   I_{\alpha C}(t-t'') \ket{\xi_{\alpha C}}\bra{\xi_{\alpha C}}
\end{split}
\end{equation}
%
So the goal is to be able to evaluate $I_{\alpha C}(t-t'')$. Rest of the section is dedicated to precisely this. We first substitute $E = \epsilon_{\alpha C} + 2\gamma \cos \theta$ and $T = t-t''$
\begin{align*}
I_{\alpha C}(T) &= \frac{2\gamma^2}{\pi\hbar}e^{-i\epsilon_{\alpha C}T/\hbar}\int_{0}^\pi d\theta \sin^2\theta e^{-i2T\gamma  \cos\theta/\hbar } \\
& = \frac{2\gamma^2}{\hbar}e^{-i\epsilon_{\alpha C}T/\hbar}\bigg[ J_{0}(x)\int_{0}^\pi d\theta \sin^2\theta + \sum_{m=1}^{\infty}(-i)^mJ_{m}(x)\int_{0}^\pi d\theta \sin^2\theta e^{im\theta} \\
&+ (-i)^{-m}J_{-m}(x)\int_{0}^\pi d\theta \sin^2\theta e^{-im\theta} \bigg] \\
& = \frac{2\gamma^2}{\hbar}e^{-i\epsilon_{\alpha C}T/\hbar}\bigg[ J_{0}(2T\gamma/\hbar)\int_{0}^\pi d\theta \sin^2\theta + 2\sum_{m=1}^{\infty}(-i)^mJ_{m}(x)\int_{0}^\pi d\theta \sin^2\theta \cos(m\theta) \bigg]
%& =  \frac{2\gamma^2}{\pi\hbar}e^{-i\epsilon_{\alpha C}T/\hbar} \sum_{m=-\infty}^{\infty} (-i)^m J_{m}(2T\gamma/\hbar )\int_{0}^\pi d\theta \sin^2\theta e^{im\theta}\\
%& = i\frac{e^{-i\epsilon_{\alpha C}T/\hbar}}{\hbar} \sum_{m=-\infty}^{\infty} (-i)^m\mathcal{P}(m) J_{m}(2T\gamma/\hbar) \\
%& =  i\frac{e^{-i\epsilon_{\alpha C}T/\hbar}}{\hbar} \bigg\{\sum_{m=0}^{\infty} (-i)^m\mathcal{P}(m) J_{m}(2T\gamma/\hbar) + \sum_{m=1}^{\infty} (-i)^{-m}\mathcal{P}(-m) J_{-m}(2T\gamma/\hbar) \bigg\}\\
%& = i\frac{e^{-i\epsilon_{\alpha C}T/\hbar}}{\hbar} \bigg\{\sum_{m=0}^{\infty} (-i)^m\mathcal{P}(m) J_{m}(2T\gamma/\hbar) + \sum_{m=1}^{\infty} (-i)^{m}\mathcal{P}(-m) J_{m}(2T\gamma/\hbar) \bigg\}\\
%& =  i\frac{e^{-i\epsilon_{\alpha C}T/\hbar}}{\hbar} \bigg\{\sum_{m=0}^{\infty} (-i)^m[\mathcal{P}(m) + [1-\delta_{m0}]\mathcal{P}(-m)]J_{m}(2T\gamma/\hbar) \bigg\}
\end{align*}
%
But we have
%
\begin{equation}
\int_{0}^\pi d\theta \sin^2\theta \cos(m\theta) = -\frac{\pi}{4}\delta_{m2}
\end{equation}
%
Therefore we have
%
\begin{equation}
\boxed{I_{\alpha C}(T) = \frac{\gamma^2}{\hbar}\bigg[ J_{0}(2T\gamma/\hbar) + J_2(2T\gamma/\hbar)\bigg]e^{-i\epsilon_{\alpha C}T/\hbar}}
\end{equation}
%
Therefore we get a simple analytical result that
%
\begin{equation}
\begin{split}
\bold{\Gamma}(t,t'') &=   \frac{\gamma^2}{\hbar}\bigg[ J_{0}(2T\gamma/\hbar) + J_2(2T\gamma/\hbar)\bigg]\sum_{\alpha=L}^{R}\sum_{C=1}^{N_c^\alpha} e^{-i\epsilon_{\alpha C}(t-t'')/\hbar}\ket{\xi_{\alpha C}}\bra{\xi_{\alpha C}} 
\end{split}
\end{equation}
%
Plugging this in Eq.~\eqref{eq:wf_equation} we obtain
%
\begin{multline}
i\hbar\frac{\partial \ket{\Psi_{\alpha C}(t,E)}}{\partial t} = \bold{H}(t)\ket{\Psi_{\alpha C}(t,E)} + \\ \sum_{\alpha_1=L}^{R}\sum_{C_1=1}^{N_c^{\alpha_1}} \sum_{m=0,2} \Omega_{\alpha_1 C_1}^{m,\alpha C}(t,E)\ket{\xi_{\alpha_1 C_1}}
\\ + e^{-iEt/\hbar}\ket{\xi_{\alpha C}}
\end{multline}
%
where we have defined an auxiliary function 
%
\begin{multline}
\Omega_{\alpha_1 C_1}^{m,\alpha C}(t,E) =  \frac{\gamma^2}{i \hbar}\int_{-\infty}^{t} J_{m}(2[t-t'']\gamma/\hbar ) e^{-i\epsilon_{\alpha_1 C_1}(t-t'')/\hbar}\braket{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t'',E)}dt''
\end{multline}
%
Let us find the equation of motion of this function
%
\begin{multline}
\frac{\partial \Omega^{m, \alpha C}_{\alpha_1 C_1}(t,E)}{\partial t} = \frac{\gamma^2J_{m}(0)}{i\hbar} \braket{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t,E)} \\ -i\frac{[\epsilon_{\alpha_1 C_1}]}{\hbar}\Omega_{\alpha_1 C_1}^{m,\alpha C}(t,E) \\ + \int_{-\infty}^{t} \frac{\gamma^2}{i\hbar}  J_{m}'(2[t-t'']\gamma/\hbar ) e^{-i\epsilon_{\alpha_1 C_1}(t-t'')/\hbar}\braket{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t'',E)}dt''
\end{multline}
%
Now we use the following property of Bessel function derivative
%
\begin{equation}
J'_m(x) = \frac{dJ_{m}(x)}{dx} = \frac{1}{2} \bigg[ J_{m-1}(x) - J_{m+1}(x)\bigg]
\end{equation}
%
This gives us the following result
%
\begin{multline}
\frac{\partial \Omega^{m,\alpha C}_{\alpha_1 C_1}(t,E)}{\partial t} = \frac{\gamma^2J_{m}(0)}{i\hbar} \braket{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t,E)}\\ -i\frac{[\epsilon_{\alpha_1 C_1}]}{\hbar}\Omega_{\alpha_1 C_1}^{m,\alpha C}(t,E) \\ + \frac{\gamma}{\hbar} \bigg[\Omega^{m-1,\alpha C}_{\alpha_1 C_1}(t,E) - \Omega^{m+1,\alpha C}_{\alpha_1 C_1}(t,E) \bigg]
\end{multline}
%
Further we note that $J_{m}(0) = \delta_{m0}$ which simplifies the problem. In practice we will have to truncate the number of omega scalars to some value of $m = m_\mathrm{max}$. Then we will be able to obtain a set of closed equations of motion. We can now summarize the equations of motion as
%
\begin{equation}\label{eq:rk_1}
\begin{split}
i\hbar\frac{\partial \ket{\Psi_{\alpha C}(t,E)}}{\partial t} &= \bold{H}(t)\ket{\Psi_{\alpha C}(t,E)} \\ &+ \sum_{\alpha_1=L}^{R}\sum_{C_1=1}^{N_c^{\alpha_1}}\vec{\mathcal{P}}\cdot \vec{\bold{\Omega}}_{\alpha_1 C_1}^{\alpha C}(t,E) \ket{\xi_{\alpha_1 C_1}} \\
&\hspace{6cm}+ e^{-iEt/\hbar}\ket{\xi_{\alpha C}}
\end{split}
\end{equation}
where we have truncated the sum up to $m = m_\mathrm{max}$ and introduced the vector 
% 
\begin{equation}
\vec{\mathcal{P}} = \vec{\bold{e}}_0 + \vec{\bold{e}}_2.
\end{equation}
%
We have also defined a vector using omega scalars given by
\begin{equation}
\vec{\bold{\Omega}}^{\alpha C}_{\alpha_1 C_1}(t,E) = \sum_{m=0}^{m_\mathrm{max}} \Omega^{m,\alpha C}_{\alpha_1 C_1}(t,E) \bold{e}_m
\end{equation}
%
which gives us 
\begin{equation}\label{eq:rk_2}
\hspace{-1cm}
\begin{split}
i\hbar\frac{\partial \vec{\bold{\Omega}}^{\alpha C}_{\alpha_1 C_1}(t,E)}{\partial t} &= \gamma^2\braket{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t,E)}\vec{\bold{e}}_0  + [\epsilon_{\alpha_1 C_1} ]\vec{\bold{\Omega}}_{\alpha_1 C_1}^{\alpha C}(t,E) + i\gamma \bold{D}\vec{\bold{\Omega}}^{\alpha C}_{\alpha_1 C_1}(t,E)
 \end{split}
\end{equation}
%
where $\bold{D}$ is a tridiagonal sparse matrix given by
\begin{equation}
\bold{D} = \begin{pmatrix} 
0 & -2 & 0 & 0 & 0 \\
1 & 0 & -1 & 0 & 0 \\
0 & 1 & 0 & -1 & 0  \\
0 & 0 & 1 & 0 & -1 \\
0 & 0 & 0 & 1 & 0 \\
\end{pmatrix},
\end{equation}
which is shown for the case of $m_\mathrm{max} = 4$. 
%======================
\subsection{Simplifying Notation}
%======================
A further simplification of these equations can be achieved by lumping the indices $\alpha C$ in to just one index which we denote by lower case alphabets ``$a$''. This allows us to write the equations in the following way
\begin{equation}\label{eq:rk_3}
\begin{split}
i\hbar\frac{\partial \ket{\Psi_{a}(t,E)}}{\partial t} &= \bold{H}(t)\ket{\Psi_{a}(t,E)} + \sum_{b=1}^{N_c^\mathrm{tot}}\vec{\mathcal{P}}\cdot \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E) \ket{\xi_{b}} + e^{-iEt/\hbar}\ket{\xi_{a}},
\end{split}
\end{equation}
%
where we have the indices running on
\begin{equation}
N_c^\mathrm{tot} = \sum_{\alpha=L}^{R} N_{c}^\alpha
\end{equation}
and the other equation is 
\begin{equation}\label{eq:rk_4}
\begin{split}
i\hbar\frac{\partial \vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t,E)}{\partial t} &= \gamma^2 \braket{\xi_{b}|\Psi_{a}(t,E)}\vec{\bold{e}}_0  + \mathcal{L}_b\vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E) + i\gamma \bold{D}\vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t,E).
 \end{split}
\end{equation}
%
Note that we have not yet given a definition for $\mathcal{L}_b(t)$. We will give this shortly. First we have to define a mapping between in the indices $\alpha,C$ and ``$a$''. Which is done in the following way
%
\begin{equation}
a(\alpha,C) = \sum_{\beta=1}^{\alpha -1} N_c^\beta + C 
\end{equation}
%
Now that the mapping is well defined we have 
\begin{equation}
\mathcal{L}_{a(\alpha,C)} = \epsilon_{\alpha C},
\end{equation}
One bad thing about Eq.~\eqref{eq:rk_3} is that it contains an oscillatory function and we can get rid of it by redefining 
\begin{equation}
\ket{\Psi_a(t,E)} \to \ket{\Psi_a(t,E)}e^{-iEt/\hbar} \hspace{0.2cm} \mathrm{and} \hspace{0.2cm} \vec{\bold{\Omega}}^a_{\hspace{0.1cm}b}(t,E) \to  \vec{\bold{\Omega}}^a_{\hspace{0.1cm}b}(t,E)e^{-iEt/\hbar}
\end{equation}
which transforms the Equations to 
\begin{equation}\label{eq:rk_5}
\boxed{
\begin{split}
i\hbar\frac{\partial \ket{\Psi_{a}(t,E)}}{\partial t} &= \bigg\{\bold{H}(t)-\id E\bigg\}\ket{\Psi_{a}(t,E)} + \mathcal{J}\sum_{b=1}^{N_c^\mathrm{tot}}\vec{\mathcal{P}}\cdot \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E) \ket{\xi_{b}} + \mathcal{K}\ket{\xi_{a}},
\end{split}}
\end{equation}
and
\begin{equation}\label{eq:rk_6}
\boxed{
\begin{split}
i\hbar\frac{\partial \vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t,E)}{\partial t} &= \gamma^2\mathcal{J}\braket{\xi_{b}|\Psi_{a}(t,E)}\vec{\bold{e}}_0  + (\mathcal{L}_b-E)\vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E) + i\gamma \bold{D}\vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t,E).
 \end{split}}
\end{equation}
where we have introduced $\mathcal{J}$ in order to couple or decouple these equations. i.e., when $\mathcal{J} = 0$ then the two equations will decouple. $\mathcal{K}$ is introduced to do further testing of equations of motion. Under similar notation the Density matrix is also simplified to the following
%
\begin{equation}\label{eq:dmt_fi}
\boxed{\boldsymbol{\rho}(t) = \sum_{a=1}^{N_c^\mathrm{tot}}  \int_{-\infty}^{+\infty} \frac{dE}{2\pi} \ket{\Psi_a(t,E)} \bra{\Psi_a(t,E)}W_a(E)}
\end{equation}
%
where we have
\begin{equation}
W_{a(\alpha,C)}(E) = f_{\alpha}(E)g_{\alpha C}(E)
\end{equation}
 This completes the problem of square lattice leads. Let us count the number of equations in this approach. Let us say we have the following parameters
%
\begin{itemize}
\item $N_x = 100$ : Number of sites in the $x$-direction.
\item $N_y = 100$ : Number of sites in the $y$-direction.
\item $N_\alpha = 2$ : Number of leads.
\item $m_\mathrm{max} = 20$ : Number of modes expanded (This is way too much, it should be good with a value of $~10$). 
\end{itemize}
%
This gives us the total number of equations to be
%
\begin{equation}
N_\mathrm{eq} = 2_s(N_\alpha N_c^\alpha N_x N_y + (2m_\mathrm{max} + 1 )(N_\alpha N_c^\alpha)^2)
\end{equation}
%
For the specific example chosen we have  $N_\mathrm{eq} = 7.28 \times 10^6$ which is a decent number of equations to solve. Note that in the first stage of code development we aim to solve Eq.~\eqref{eq:rk_3} and Eq.~\eqref{eq:rk_4}.


\section{Lead Currents} 
\label{sec:curr} 
%=====================================
Equation~\eqref{eq:dmt} gives us access to density matrix of the system however we do not get a direct access to the currents that are being injected into the leads. To obtain those, we take a time derivative of this equation to get and equation of motion for the density matrix which will tell us what exits the system.
%
\begin{equation}
\begin{split}
i\hbar \frac{\partial \boldsymbol{\rho}(t)}{\partial t} &= \sum_{a=1}^{N_c^\mathrm{tot}} \int_{-\infty}^{\infty} \frac{dE}{2\pi} \bigg\{i\hbar\frac{\partial \ket{\Psi_{a}(t,E)}}{\partial t}\bra{\Psi_{a}(t,E)} + i\hbar \ket{\Psi_{a}(t,E)}\frac{\bra{\Psi_{a}(t,E)}}{\partial t}\bigg\}W_a(E) \\
&= [\bold{H}(t)-E]\boldsymbol{\rho}(t) + \sum_{a=1}^{N_c^\mathrm{tot}} \sum_{b=1}^{N_c^\mathrm{tot}} \int_{-\infty}^{\infty}\frac{dE}{2\pi} \vec{\mathcal{P}}\cdot \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)
\ket{\xi_{b}} \bra{\Psi_{a}(t,E)}W_a(E) \\ 
& + \sum_{a=1}^{N_c^\mathrm{tot}} \int_{-\infty}^{\infty}\frac{dE}{2\pi}  \ket{\xi_{a}}\bra{\Psi_{a}(t,E)}W_a(E) \\
&-\boldsymbol{\rho}(t)[\bold{H}(t)-E] - \sum_{a=1}^{N_c^\mathrm{tot}} \sum_{b=1}^{N_c^\mathrm{tot}}\int_{-\infty}^{\infty}\frac{dE}{2\pi} [\vec{\mathcal{P}}\cdot \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)]^* \ket{\Psi_{a}(t,E)}\bra{\xi_{b}}W_a(E) \\ 
& - \sum_{a=1}^{N_c^\mathrm{tot}} \int_{-\infty}^{\infty}\frac{dE}{2\pi}  \ket{\Psi_{a}(t,E)}\bra{\xi_{a}}W_a(E) \\
&= [\bold{H}(t),\bold{\rho}(t)] + i \sum_{a=1}^{N_c^\mathrm{tot}} \sum_{b=1}^{N_c^\mathrm{tot}}\bigg\{ \bold{\Pi}_{ab}(t) + \bold{\Pi}_{ab}^\dagger(t)\bigg\},
\end{split}
\end{equation}
%
where we have
%
\begin{equation}
\begin{split}
\bold{\Pi}_{ab}(t) &= \frac{1}{i} \int_{-\infty}^{\infty}\frac{dE}{2\pi} \vec{\mathcal{P}}\cdot \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)\ket{\xi_{b}} \bra{\Psi_{a}(t,E)}W_a(E) \\ 
& + \frac{1}{i} \int_{-\infty}^{\infty}\frac{dE}{2\pi}  \ket{\xi_{a}}\bra{\Psi_{a}(t,E)}W_a(E)\end{split}
\end{equation}
These matrices will help us in calculating the currents injected into the lead but for that we will need a function which takes in the index ``$a$'' and returns the corresponding  $\alpha$ and $C$ which we denote as $\alpha(a)$ and $C(a)$. Therefore we have charge current in lead $\alpha$ as
\begin{equation}
\boxed{I_{\alpha}(t) = \sum_{a=1}^{N_c^\mathrm{tot}} \sum_{b=1}^{N_c^\mathrm{tot}} \Tr [\bold{\Pi}_{ab}(t)]\delta_{\alpha,\alpha(a)}}
\end{equation}
and the spin current is computed as
\begin{equation}
\boxed{I_{\alpha}^{S_\beta}(t) = \sum_{a=1}^{N_c^\mathrm{tot}} \sum_{b=1}^{N_c^\mathrm{tot}}  \Tr [\boldsymbol{\sigma}_\beta \bold{\Pi}_{ab}(t)]\delta_{\alpha,\alpha(a)}}
\end{equation}
This provides as a recipe for calculating all the spin and charge currents that will be injected into the leads.


\section{Initial conditions} 
\label{sec:initial_con} 
%=====================================
The initial conditions of the WF do not matter as they are soon forgotten and system will reach a steady state if the Hamiltonian is time-independent. In such a scenario it is possible to write the steady state density matrix of the system as
\begin{equation}\label{eq:ss_dm}
\begin{split}
\boldsymbol{\rho} &= \frac{1}{2\pi i}\int_{-\infty}^{\infty} dE \bold{G}^<(E) \\
&= \sum_{\alpha=L}^R\sum_{C=1}^{N_c^\alpha}\frac{1}{2\pi }\int_{-\infty}^{\infty} dE \bold{G}^r(E)\ket{\xi_{\alpha C}(E)}\bra{\xi_{\alpha C}(E)}\bold{G}^a(E)f_{\alpha}(E)g_{\alpha C}(E) \\
&= \sum_{b=1}^{N_c^{tot}}\frac{1}{2\pi }\int_{-\infty}^{\infty} dE \bold{G}^r(E)\ket{\xi_{b}(E)}\bra{\xi_{b}(E)}\bold{G}^a(E)W_{b}(E)
\end{split}
\end{equation}
where $\bold{G}^r(E)$ is the energy dependent retarded GF of the system given by
\begin{equation}
\bold{G}^r(E) = \frac{1}{E  - \bold{H}_0 -\bold{\Sigma}^r(E)}
\end{equation}
where $\bold{H_0}$ is the Hamiltonian that we would to reaching steady state before any time-dependence develops in the system. This has a very close relation to Eq.~\eqref{eq:GTT} and helps us in setting initial conditions of the WF in such a way that at $t=0$ the system is already in steady-state.
 With a little bit of exercise (shown in appendix) it is easy to show that required initial condition to reproduce the density matrix in Eq.~\eqref{eq:ss_dm} is given by
\begin{equation}\label{eq:wf_ic}
\boxed{\ket{\Psi_{b}(t=0,E)} = \bold{G}^r(E)\ket{\xi_{b}}}
\end{equation}
This has an advantage. Now we do not have to wait for the system to reach a steady state before we hit the system with any time dependence. Furthermore the initial conditions for the omega vectors is given by looking at Eq.~\eqref{eq:rk_4} and demanding steady state
\begin{equation}\label{eq:rk_4_ic}
\begin{split}
&\gamma^2\braket{\xi_{b}|\Psi_{a}(t=0,E)}\vec{\bold{e}}_0  + \mathcal{L}_b\vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t=0,E) + i\gamma \bold{D}\vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t=0,E) = 0 \\
& \mathcal{L}_b\vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t=0,E) + i\gamma \bold{D}\vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t=0,E) = -\gamma^2\braket{\xi_{b}|\Psi_{a}(t=0,E)}\vec{\bold{e}}_0  \\
& \bigg[\mathcal{L}_b + i\gamma \bold{D} \bigg]\vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t=0,E) = -\gamma^2\braket{\xi_{b}|\Psi_{a}(t=0,E)}\vec{\bold{e}}_0  \\
& \vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t=0,E) = -\gamma^2\bigg[\mathcal{L}_b + i\gamma \bold{D}\bigg]^{-1} \braket{\xi_{b}|\Psi_{a}(t=0,E)}\vec{\bold{e}}_0 \\
& \boxed{\vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t=0,E) = -\gamma^2\bigg[\mathcal{L}_b+ i\gamma\bold{D}\bigg]^{-1} \bra{\xi_{b}}\bold{G}^r(E)\ket{\xi_a} \vec{\bold{e}}_0}
\end{split}
\end{equation}
