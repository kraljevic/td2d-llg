
\chapter{Appendix} 
\label{ch:appendix} 

In the appendix certain extended derivations are available. 

\section{Restructuring the system Hamiltonian to
         incorporate time dependent voltages in leads}
Let us first start with the case of incorporating
time-dependent voltages in the leads. If leads have any
time-dependence then it is not clear how to populate the
states of leads when reservoirs are attached to them, to
avoid this we make a Unitary transformation to our system.
Let us look at the Hamiltonian of the system with voltage
pulse
%
\begin{equation}
\bold{H}_\mathrm{tot}(t) = \begin{pmatrix}
\bold{H}(t) & \bold{V}_{CL} & \bold{V}_{CR} \\
\bold{V}_{CL}^\dagger & \bold{H}_L(t)& 0 \\
 \bold{V}_{CR}^\dagger & 0 & \bold{H}_R(t) \\
\end{pmatrix}
\end{equation}
%
where we have ($\alpha = L,R$)
\begin{equation}
\bold{H}_\alpha (t) = \bold{H}_\alpha + \id \Delta_\alpha(t)
\end{equation}
In this case we do a Unitary transformation which says
%
\begin{equation}
\bold{U} = \begin{pmatrix}
\id & 0 & 0 \\
0 & \bold{U}_L(t) & 0 \\
0 & 0 & \bold{U}_R(t)
\end{pmatrix}
\end{equation}
%
We have then 
%
\begin{equation}
\bold{U}^\dagger \bold{H}_\mathrm{tot}(t)\bold{U} = \begin{pmatrix}
\bold{H}(t) & \bold{V}_{CL}U_L(t) & \bold{V}_{CR}U_R(t) \\
\bold{U}_L^\dagger(t)\bold{V}_{CL}^\dagger & \bold{U}_L(t)^\dagger\bold{H}_L(t)\bold{U}_L(t) & 0\\
\bold{U}_R^\dagger(t)\bold{V}_{CR}^\dagger & 0 & \bold{U}_R(t)^\dagger\bold{H}_R\bold{U}_R(t) \\
\end{pmatrix}
\end{equation}
%
Now we can demand that the transformation be such that
\begin{equation}
\bold{U}^\dagger_\alpha(t)\bold{H}_\alpha(t)\bold{U}_\alpha(t) = \bold{H}_\alpha
\end{equation}
This amounts to having
\begin{equation}
\begin{split}
&\bold{U}^\dagger_\alpha \bold{H}_\alpha \bold{U} + \Delta_\alpha(t) \id = \bold{H}_\alpha \\
&[\bold{U}_\alpha(t),\bold{H}_L] = \bold{U}_\alpha(t)\Delta_{\alpha}(t) \\
&i\hbar\frac{d\bold{U}_\alpha(t)}{dt} = \bold{U}_\alpha(t)\Delta_\alpha(t) 
\end{split}
\end{equation}
%
This tells us that the operator is given as
%
\begin{equation}
\bold{U}_\alpha(t) = \exp\bigg( -\frac{i}{\hbar}\int_{-\infty}^t\Delta_\alpha(t')dt'\bigg)\id
\end{equation}
%
So therefore the Hamiltonian we can work with instead is given as
%
\begin{equation}\label{eq:tot_ham}
\bold{H}_\mathrm{tot}(t) = \begin{pmatrix}
\bold{H}(t) & \bold{V}_{CL}(t) & \bold{V}_{CR}(t) \\
\bold{V}_{CL}^\dagger(t) & \bold{H}_L & 0 \\
 \bold{V}_{CR}^\dagger(t) & 0 & \bold{H}_R \\
\end{pmatrix}
\end{equation}
%
where we have 
%
\begin{equation}
\boxed{\bold{V}_{C\alpha}(t) = \bold{V}_{C\alpha}\exp\bigg[-\frac{i}{\hbar}\int_{-\infty}^t\Delta_\alpha(t')dt' \bigg]}.
\end{equation}
% 
So to incorporate the time dependence of leads we make the coupling between the system and the leads time-dependent. This will require some restructuring of the Hamiltonian of the central region. Let us present Eq.~\eqref{eq:tot_ham} in a way such that it helps in restructuring of the Hamiltonian
\begin{equation}
\bold{H}_\mathrm{tot}(t) = 
{\tiny
\begin{pmatrix}
\bold{H}(t) & | & \widetilde{\bold{V}}_{CL}(t) & 0 & 0 & 0 &
  \cdots & | & \widetilde{\bold{V}}_{CR}(t) 
  & 0 & 0 & 0 & \cdots \\
-- & | &-- & -- &  --  &  --   & -- &  | 
       &  --  & -- &  --  &  -- & -- \\
\widetilde{\bold{V}}_{CL}^\dagger(t) & | 
& \bold{H}_{0L} & \bold{V}_L & 0 & 0 & \cdots 
& | & 0 & 0 & 0 & 0 & \cdots \\
0 & | &\bold{V}_{L}^\dagger & \bold{H}_{0L} & \bold{V}_L & 0 & \cdots & |  & 0 & 0 & 0 & 0 & \cdots \\
0 & | &0 & \bold{V}_{L}^\dagger & \bold{H}_{0L} & \bold{V}_L &  \cdots & | & 0 & 0 & 0 & 0 & \cdots \\
0 & | &0 & 0 & \bold{V}_{L}^\dagger & \bold{H}_{0L}  & \bold{V}_L & |  & 0 & 0 & 0 & 0 & \cdots \\
0 & | &\vdots & \vdots &  \vdots  &  \vdots   &  \ddots  &  | &  \vdots  &  \vdots  &  \vdots  &  \vdots  & \ddots \\
-- & | &-- & -- &  --  &  --   & -- &  | &  --  & -- &  --  &  -- & -- \\
\widetilde{\bold{V}}_{CR}^\dagger(t) & | &0 & 0 & 0 & 0 & 0 & | &\bold{H}_{0R} & \bold{V}_R & 0 & 0 & \cdots  \\
0 & | &0 & 0 & 0 & 0 & 0 & | & \bold{V}_R^\dagger &\bold{H}_{0R} & \bold{V}_R & 0 & \cdots  \\
0 & | &0 & 0 & 0 & 0 & 0 & | & 0 & \bold{V}_R^\dagger &\bold{H}_{0R} & \bold{V}_R & \cdots  \\
0 & | &0 & 0 & 0 & 0 & 0 & | & 0 & 0 & \bold{V}_R^\dagger &\bold{H}_{0R} & \bold{V}_R   \\
 \vdots& |   &  \vdots  &  \vdots  &  \vdots  & \vdots &\ddots & | & \vdots & \vdots &  \vdots  &  \vdots   &  \ddots 
\end{pmatrix}}
\end{equation}
where we have 
\begin{equation}
\begin{split}
\bold{V}_{C\alpha}(t) &= \begin{pmatrix}
\widetilde{\bold{V}}_{C\alpha}(t) & 0 & 0 & 0 & 0 & \cdots 
\end{pmatrix} \\
&= \begin{pmatrix}
\bold{V}_{C\alpha}e^{-i\int_{-\infty}^t\Delta_{\alpha}(t')dt'/\hbar} & 0 & 0 & 0 & 0 & \cdots 
\end{pmatrix}
\end{split}
\end{equation}
The restructured Hamiltonian hence looks like the following
\begin{equation}
{\tiny
\bold{H}_\mathrm{tot}(t) = \begin{pmatrix}
\bold{H}(t) & \widetilde{\bold{V}}_{CL}(t) & \widetilde{\bold{V}}_{CR}(t) & | & 0 & 0 & 0 & \cdots & | & 0 & 0 & 0  & \cdots \\
\widetilde{\bold{V}}_{CL}^\dagger(t) & \bold{H}_{0L} & 0 & | & \bold{V}_L & 0 & 0 & \cdots & | & 0 & 0 & 0  & \cdots \\
\widetilde{\bold{V}}_{CR}^\dagger(t) & 0 & \bold{H}_{0R} & | & 0 & 0 & 0 & 0 & | & \bold{V}_R & 0  & 0 &\cdots  \\
-- &-- & -- & | & -- &  --  &  --   & -- &  | &  --  & -- &  --  &  -- \\
0 &\bold{V}_{L}^\dagger & 0 & | & \bold{H}_{0L} & \bold{V}_L & 0 & \cdots & |  & 0 & 0 &  0 & \cdots \\
0 &0 & 0 & | &\bold{V}_{L}^\dagger & \bold{H}_{0L} & \bold{V}_L &  \cdots & | & 0 & 0 & 0 & \cdots \\
0 &0 & 0 & |& 0 & \bold{V}_{L}^\dagger & \bold{H}_{0L}  & \bold{V}_L & |  & 0 & 0 & 0 &  \cdots \\
0 &\vdots & 0 & |  & \vdots &  \vdots  &  \vdots   &  \ddots  &  | &  \vdots  &  \vdots  &  \vdots  & \ddots \\
-- &-- & -- & | & -- &  --  &  --   & -- &  | &  --  & -- &  --  &  --  \\
0 & 0 & \bold{V}_R^\dagger & | & 0 & 0 & 0 & 0 & | & \bold{H}_{0R} & \bold{V}_R & 0 & \cdots  \\
0 &0  & 0 & | & 0 & 0 & 0 & 0 & | & \bold{V}_R^\dagger &\bold{H}_{0R} & \bold{V}_R & \cdots  \\
0 &0  & 0 & |  & 0 & 0 & 0 & 0 & | & 0 & \bold{V}_R^\dagger &\bold{H}_{0R} & \bold{V}_R   \\
 \vdots& \vdots & \vdots & |  & \vdots  &    \vdots  & \vdots &\ddots & | & \vdots & \vdots &  \vdots    &  \ddots 
\end{pmatrix}}
\end{equation} 
Hence this shows us that we need to redefine
the system Hamiltonian as the top corner block of this
matrix. Therefore the restructured time-dependent
Hamiltonian that needs to be written down is given as
\begin{equation}
\bold{H}_\mathrm{new}(t) = \begin{pmatrix}
\bold{H}(t) & \widetilde{\bold{V}}_{CL}(t) & \widetilde{\bold{V}}_{CR}(t) \\
\widetilde{\bold{V}}_{CL}^\dagger(t) & \bold{H}_{0L} & 0 \\
\widetilde{\bold{V}}_{CR}^\dagger(t) & 0 & \bold{H}_{0R} 
\end{pmatrix}
\end{equation}
Note that this has a nice implication that the $\ket{\xi_a}$ vectors now become very simple to deal with. They will have only two possible forms
\begin{equation}
\ket{\xi_a} = \begin{pmatrix}
0 \\
R_a \\
0
\end{pmatrix}
\hspace{0.3cm} \mathrm{or} \hspace{0.3cm}
\begin{pmatrix}
0 \\
0 \\
R_a
\end{pmatrix}
\end{equation}
So when calculating $\ket{\xi_a}$ vectors. We need to keep
this structure in mind. \emph{However it is not necessary
that it be done this way. Whatever numbering is introduced
using graphs will also work and $\ket{\xi_a}$ vectors must
be returned accordingly.}

\section{Steady-state density matrix derivation}
%====================================
Let us now derive the equilibrium Density matrix from
lesser Green function expression. So that all pre-factors
are carefully incorporated. We begin with the lesser Green
function

%
\begin{equation}
  \bold{G}^<(t,t') = \int_{-\infty}^{\infty}dt_1 
  \int_{-\infty}^{\infty}dt_2 \bold{G}^r(t,t_1)
  \bold{\Sigma}^<(t_1,t_2)\bold{G}^a(t_2,t')
\end{equation}
%
Let us use the Fourier expansion of GF 
\begin{equation}
  \bold{G}^r(t,t_1) = 
   \int_{-\infty}^{\infty} \int_{-\infty}^{\infty}
   \frac{dEdE_1}{(2\pi\hbar)^2} 
   \bold{G}^r(E,E_1)e^{-iEt/\hbar}e^{iE_1t_1/\hbar}
\end{equation}
\begin{equation}
\bold{G}^a(t_2,t') = \int_{-\infty}^{\infty} 
   \int_{-\infty}^{\infty}\frac{dE'dE_2}{(2\pi\hbar)^2}
   \bold{G}^a(E',E_2)e^{iE't'/\hbar}e^{-iE_2t_2/\hbar}
\end{equation}
\begin{equation}
  \bold{\Sigma}^<(t_1,t_2) = \int_{-\infty}^{\infty}
  \frac{dE}{2\pi\hbar}\bold{\Sigma}^<(E)e^{-iE(t_1-t_2)/\hbar}
\end{equation}
Applying this transformation to Eq.~\eqref{eq:gtt} results in
\begin{multline}
\bold{G}^<(t,t') =   \int_{-\infty}^{\infty}\int_{-\infty}^{\infty} \int_{-\infty}^{\infty} \frac{d\bar{E}dEdE'}{(2\pi\hbar)^3} \bold{G}^r(E,\bar{E})\bold{\Sigma}^<(\bar{E})\bold{G}^a(\bar{E},E')e^{-iEt/\hbar}e^{iE't'/\hbar}
\end{multline}
However we note that in case of steady-state we have
\begin{equation}
\begin{split}
&\bold{G}^r(E,\bar{E}) = 2\pi\hbar \bold{G}^r(E)\delta(E-\bar{E}) \\
&\bold{G}^a(\bar{E},E') = 2\pi\hbar \bold{G}^a(\bar{E})\delta(\bar{E}-E') \\
\end{split}
\end{equation}
This tells us 
\begin{multline}
\bold{G}^<(t,t') =   \int_{-\infty}^{\infty}\int_{-\infty}^{\infty} \int_{-\infty}^{\infty} \frac{d\bar{E}dEdE'}{2\pi\hbar} \\ \bold{G}^r(E)\bold{\Sigma}^<(\bar{E})\bold{G}^a(\bar{E}) \\ 
\delta(E-\bar{E})\delta(\bar{E}-E')e^{-iEt/\hbar}e^{iE't'/\hbar}
\end{multline}
which tells us 
\begin{equation}
\bold{G}^<(t,t') = \int_{-\infty}^{\infty}\frac{dE}{2\pi\hbar}\bold{G}^a(E)\bold{\Sigma}^<(E)\bold{G}^r(E)e^{-iE(t-t')/\hbar}
\end{equation}
which tells us that 
\begin{equation}
\boldsymbol{\rho}(t) = \frac{\hbar}{i}\bold{G}^<(t,t) = \sum_{\alpha C} \int_{-\infty}^{\infty}\frac{dE}{2\pi}\bold{G}^r(E)\ket{\xi_{\alpha C}}\bra{\xi_{\alpha C}}\bold{G}^a(E) f_{\alpha}(E)g_{\alpha C}(E)
\end{equation}
Now let us look at the definition in Eq.~\eqref{eq:ket_def} and \eqref{eq:bra_def} and derive some relations
\begin{equation}
\begin{split}
\ket{\Psi_{\alpha C}^\mathrm{ss}(t,E)} &= \int_{-\infty}^{\infty}\frac{dE'}{2\pi \hbar}\bold{G}^r(E',E)e^{-iE't/\hbar}\ket{\xi_{\alpha C}} \\
&= \int_{-\infty}^{\infty}dE'\bold{G}^r(E')\delta(E'-E)e^{-iE't/\hbar}\ket{\xi_{\alpha C}} \\
&= \bold{G}^r(E)\ket{\xi_{\alpha C}}e^{-iEt} \\
& = \ket{\Psi^\mathrm{ss}_{\alpha C}(E)}e^{-iEt} \to \ket{\Psi^\mathrm{ss}_{\alpha C}(E)}
\end{split}
\end{equation}
In the last step we have employed the redefine WF which we introduced in the equation of motion. Similarly we have
\begin{equation}
\bra{\Psi_{\alpha C}^\mathrm{ss}(t,E)} = \bra{\xi_{\alpha C}}\bold{G}^a(E)e^{iEt} = \bra{\Psi^\mathrm{ss}_{\alpha C}(E)}e^{iEt} \to  \bra{\Psi^\mathrm{ss}_{\alpha C}(E)}
\end{equation}
Therefore, it is clear that the steady state density matrix is given by
\begin{equation}
\boldsymbol{\rho}^\mathrm{ss} = \sum_{\alpha C} \int_{-\infty}^\infty \frac{dE}{2\pi} \ket{\Psi_{\alpha C}^\mathrm{ss}(E)}\bra{\Psi_{\alpha C}^\mathrm{ss}(E)}f_{\alpha C}(E)g_{\alpha C}(E)
\end{equation}
where we have
\begin{equation}
\ket{\Psi_{\alpha C}^\mathrm{ss}(E)} = \bold{G}^r(E)\ket{\xi_{\alpha C}}
\end{equation}
and $\bold{G}^r(E)$ is the retarded Green function calculated by inverting the renormalized Hamiltonian with lead self energy
\begin{equation}
\bold{G}^r(E) = (E - \bold{H}^\mathrm{ss} - \bold{\Sigma}^r(E))^{-1}
\end{equation}
Furthermore, we now revert back to our simplified notation and write down the steady-state density matrix as
\begin{equation}
\boxed{\boldsymbol{\rho}^\mathrm{ss} = \sum_{a=1}^{N_c^\mathrm{tot}} \int_{-\infty}^\infty \frac{dE}{2\pi} \ket{\Psi_{a}^\mathrm{ss}(E)}\bra{\Psi_{a}^\mathrm{ss}(E)}W_a(E)}
\end{equation}
This provides explanation on why we choose the WF initial condition in Eq.~\eqref{eq:wf_ic}.
