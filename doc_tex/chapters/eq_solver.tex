\chapter{Computing the equilibrium density matrix}%
\label{ch:eq}
In this chapter we explain the method used to compute the
equilibrium density matrix.

\section{Denis method}
The retarded Green function usually has spikes on the real
axis. These spikes have their origin centered in either
band crossing or in quasi bound states that can be formed
in the system attached with leads. Therefore, it is not
feasible to integrate them on real axis. One approach is to
analytically continue Green functions into the complex
plane where they are smooth and then by using complex
contours and residue theorem the integration be done.

Here we employ the method developed in
Ref.~\cite{nikolic2000} to obtain the equilibrium density
matrix of a system. Often we will call it Denis's density
matrix (due to author's name). The approach taken is the
following. What we want to evaluate is 
%
\begin{equation}\label{eq:dm}
\bm{\rho}_\mathrm{eq}(\mu) = 
 -\frac{1}{\pi} \int_{E_\mathrm{min}}^{+\infty} 
  \mathrm{Im} \bold{G}^r(E)f(\mu, T, E)  dE 
\end{equation}
%
$f(\mu, T, E)$ is the Fermi function of the system and
$\bold{G}^r(E)$ is the retarded Green function of the
system and $E_\mathrm{min}$ is the lower bound of the
energy integration. We first replace the Fermi-function by
a new function which is given by
%
\begin{multline}\label{eq:new_fermi}
\mathcal{F}(\mu, \mu_\mathrm{Re}, \mu_\mathrm{Im}, T,
T_\mathrm{Re}, T_\mathrm{Im}, E) 
= f(i\mu_\mathrm{Im}, iT_\mathrm{Im}, E)  \\ 
\times \bigg[ f(\mu, T, E) 
- f(\mu_\mathrm{Re}, T_\mathrm{Re}, E )\bigg]
\end{multline}
%
Here the parameters $\mu$, $\mu_\mathrm{Re}$,
$\mu_\mathrm{Im}$, $T$, $T_\mathrm{Re}$, $T_\mathrm{Im}$
are limited to the real domain while $E$ can be
analytically continued to the complex domain. We now
introduce some constraints to the parameters which are
given as
%
\begin{subequations}\label{eq:const}
%
\begin{equation}
T_\mathrm{Re} > 0 \hspace{0.2cm} T_\mathrm{Im} > 0
\end{equation}
%
\begin{equation}
\mu_\mathrm{Re} \leq E_\mathrm{min} - p k_bT_\mathrm{Re}
\end{equation}
%
\begin{equation}
\mu_\mathrm{Im} \geq  p k_bT_\mathrm{Im}
\end{equation}
\end{subequations}
%
\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{figs/poles.pdf}
\caption{Display of poles in the complex plane of the
  replaced Fermi function $\mathcal{F}$ in
  Eq.~\eqref{eq:new_fermi}. Here, the black color
  represents $|F| \to 0$ and the orange shows $|F| \to
  1$. The values of the various parameters are arbitrarily
  chosen to display the poles. The sharp spikes in the
  complex planes are the poles of the function (You may use
  the parameters to reproduce this result).}
\label{fig:fig1}
\end{figure}
For these conditions it is easy to show that for real
$E\geq E_\mathrm{min}$, $\mathcal{F}$ does not deviate from
$f$ more than $\delta = e^{-p}$. Therefore for all
practical purposes one is allowed to replace $f$ by
$\mathcal{F}$ in Eq.~\eqref{eq:dm}.  Then, we note that the
poles and residue of this function can be evaluated. For
the first term in Eq.~\eqref{eq:new_fermi} the poles and
residue are given as \begin{subequations}
%
\begin{equation}
z_\mathrm{Im}^{(n)} = i\mu_\mathrm{Im} +
    \pi k_bT_\mathrm{Im}(2n+1)
\end{equation}
%
\begin{equation}
R^{(n)}_\mathrm{Im} = -ik_bT_\mathrm{Im} \times 
 [f(\mu, T, z_\mathrm{Im}^{(n)} ) - 
 f(\mu_\mathrm{Re}, T_\mathrm{Re}, z_\mathrm{Im}^{(n)} )]
\end{equation}
%
\end{subequations}
For the second term it is given by
\begin{subequations}
%
\begin{equation}
z^{(n)} = \mu + i\pi k_bT(2n+1)
\end{equation}
%
\begin{equation}
R^{(n)}= -k_bT f(i\mu_\mathrm{Im}, iT_\mathrm{Im}, z^{(n)})
\end{equation}
%
\end{subequations}
while for the last term it is given by
\begin{subequations}
%
\begin{equation}
z^{(n)}_\mathrm{Re} = \mu_\mathrm{Re} + i\pi k_bT_\mathrm{Re}(2n+1)
\end{equation}
%
\begin{equation}
R^{(n)}_\mathrm{Re}= k_bT_\mathrm{Re}f(i\mu_\mathrm{Im}, 
  iT_\mathrm{Im}, z^{(n)}_\mathrm{Re})
\end{equation}
%
\end{subequations}
%
These poles are shown in Fig.~\ref{fig:fig1} as sharp
spikes in the complex plane. Note that in this derivation
it was assumed that none of the poles overlap which is
allowed because $T_\mathrm{Re}$ and $T_\mathrm{Im}$ give
you enough freedom as free parameters. Therefore the final
density matrix from Eq.~\eqref{eq:dm} is given by
%
\begin{multline}\label{eq:dm_denis}
\bm{\rho}_\mathrm{eq} = 
  -\frac{1}{\pi} \sum_{n=1}^{N_\mathrm{Im}} 
  \mathrm{Im}\bigg[2\pi i 
  R_\mathrm{Im}^{(n)}\bold{G}(z^{n}_\mathrm{Im}) \bigg]  \\
-\frac{1}{\pi} \sum_{n=1}^{N}\mathrm{Im}\bigg[ 2\pi
i R^{(n)}\bold{G}(z^{n})\bigg] \\
-\frac{1}{\pi} \sum_{n=1}^{N_\mathrm{Re}}
\mathrm{Im} \bigg[2\pi i
R^{(n)}_\mathrm{Re}\bold{G}(z^{n}_\mathrm{Re})\bigg] \\
\end{multline}
%
The advantage of this approach is that, provided
Eq.~\eqref{eq:const} is satisfied the residues
$R^{(n)}_\mathrm{Im}$, $R^{(n)}$ and $R^{(n)}_\mathrm{Re}$
die exponentially as more poles are added. Therefore with a
finite number of poles the equilibrium Green function can
be evaluated. 

\section{How to choose the optimal parameters}

\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{figs/min.pdf}
\caption{%
  Left panel shows the plot of $N_{tot}$ on the plane of
  $T_\mathrm{Re}$ and  $T_\mathrm{Im}$ where the Green dot
  represents the minimum and the white dashed line is a cut
  with $T_\mathrm{Re} = T_\mathrm{Im}$. The right panel
  shows $N_{tot}$ along the cut where $T_\mathrm{Re} =
  T_\mathrm{Im}$ while the Red dotted line is the
  analytically calculated position of minima using
  Eq.~\eqref{eq:minima}} \label{fig:fig2}
\end{figure}
We note that $T_\mathrm{Re}$ and $T_\mathrm{Im}$ are free
parameters. Then we can use the ``equality" of
Eq.~\eqref{eq:const} to decide $\mu_\mathrm{Re}$ and
$\mu_\mathrm{Im}$ and the total number of poles (of all
three kinds) are then given by 
%
\begin{subequations}
\begin{equation}
N_\mathrm{Im} = \frac{\mu - \mu_\mathrm{Re} + 
pk_bT_\mathrm{Re} + pk_bT}{2\pi k_bT_\mathrm{Im}}
\end{equation}
%
\begin{equation}
N = \frac{2\mu_\mathrm{Im}}{2\pi k_bT}
\end{equation}
%
\begin{equation}
N _\mathrm{Re}= \frac{2\mu_\mathrm{Im}}{2\pi k_bT_\mathrm{Re}}
\end{equation}
\end{subequations}
% 
We now need to optimize over the parameters $T_\mathrm{Re}$
and $T_\mathrm{Im}$ in order to minimize the total number
of poles i.e., $N_\mathrm{tot} = N_\mathrm{Im} + N +
N_\mathrm{Re}$. To do this optimization we will consider
$T_{Re}$ and $\mathrm{T}_\mathrm{Im}$ as continuous
functions and take derivative of the expression. 
%
\begin{subequations}
\begin{equation}
\frac{\partial N_\mathrm{tot}}{\partial T_\mathrm{Re}} = 0
\end{equation}
\begin{equation}
\frac{\partial N_\mathrm{tot}}{\partial T_\mathrm{Im}} = 0
\end{equation}
\end{subequations}
%
This can be done by hand and we obtain the following result
with the optimization
%
\begin{equation}\label{eq:minima}
  T_\mathrm{Re} = T_\mathrm{Im} = 
  \sqrt{\frac{T}{2}\bigg[ T +
  \frac{\mu - E_\mathrm{min}}{pk_b}\bigg]}
\end{equation}
%
We note that for the chosen values of $T_\mathrm{Re}$ and
$T_\mathrm{Im}$ from the above equation, the values of
$N_\mathrm{Im}$,   $N$ and  $N_\mathrm{Re}$ may not be an
integer, therefore we find the  result from the above
equations and then consider an integer value from them
(ceiling it). At this stage the solution is still not
complete as this does not guarantee that none of the poles
$z^{(n)}_\mathrm{Im}$, $z^{(n)}$ and $z^{(n)}_\mathrm{Re}$
do not overlap. 
%
\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{figs/pole_change.pdf}
\caption{%
   Figure showing the adjustments made to the parameters
   $T_\mathrm{Im} \to T_\mathrm{Im}'$ and $\mu_{Re} \to
   \mu_{Re}'$ in order to remove any overlap of the poles.
   The top panels show the position of poles before
   adjustment. Here we had assumed $p=21$, $E_\mathrm{min}
   = -3$, $T = 300$ and $\mu=0.2$. Also,  $\mu^{\pm} = \mu
   \pm 4\pi k_bT$ and $\mu_\mathrm{Re, Im}^\pm =
   \mu_\mathrm{Re, Im}\pm 4\pi k_bT_\mathrm{Im, Re}$.}
   \label{fig:fig3}
\end{figure}
%
To achieve this we wil now make small adjustments to
$T_\mathrm{Im}$ and $\mu_\mathrm{Re}$ i.e., $T_\mathrm{Im}
\to T_\mathrm{Im}'$ and $\mu_\mathrm{Re} \to
\mu_\mathrm{Re}'$. The process is as follows first we find
an $m_\mathrm{max}$ such that $\pi k_b
T_\mathrm{Im}(2m_\mathrm{max}+1) > \mu$ but just greater
than $\mu$ (note the usage of $T_\mathrm{Im}$ and not
$T_\mathrm{Im}'$) after that the value of  $T_\mathrm{Im}'$
can be found as
%
\begin{equation}
  T_\mathrm{Im}' = \frac{\mu}{2\pi k_bm_\mathrm{max}}
\end{equation}
%
In the event of $m_\mathrm{max} = 0$ we resort to choosing
$T_\mathrm{Im}' = 2T_\mathrm{Im}$. Then we perform an
adjustment to $\mu_\mathrm{Re}$ by finding an
$n_\mathrm{max}$ such that $\pi
k_bT'_\mathrm{Im}(2n_\mathrm{max}+1) > \mu_\mathrm{Re}$.
Then $\mu_\mathrm{Re}'$ is given by
%
\begin{equation}
\mu_\mathrm{Re}' = 2\pi k_bT'_\mathrm{Im}n_\mathrm{max}
\end{equation}
With this way we ensure that the poles never overlap
otherwise we would have huge error in our evaluation. We
also note that once $T_\mathrm{Im} \to T'_\mathrm{Im}$ we
can also update the total number of poles under
consideration (This is only necessary in the case of
$m_\mathrm{max} = 0$).
%

\section{Example: Single TB spinless site example}


Let us consider the case of single site system attached
with two leads and evaluate its Green function. The
Hamiltonian of this system is given by $\hat{H} =
\epsilon_0$ where we assume $\epsilon_0 = \pm|\epsilon_0|$.
Let us write the expression for the Green function
%
\begin{equation}
\bold{G}^r(E) = [E + i\eta - \epsilon_0 - 
\bold{\Sigma}^r_L(E) - \bold{\Sigma}^r_R(E) ]^{-1} 
\end{equation}
%
where $\eta \to 0$ and $\bold{\Sigma}^r_p(E)$ is the
retarded self-energy of 1D lead-p given by \begin{equation}
\bold{\Sigma}^r_p(E) =  
\begin{cases}
\frac{E}{2} - \frac{i}{2} \sqrt{4 - E^2} & 
  \mathrm{if} \hsf |E| < 2 \\
\frac{E}{2} - \frac{1}{2}\sqrt{E^2-4} & 
\mathrm{if} \hsf E > 2 \\
\frac{E}{2} + \frac{1}{2}\sqrt{E^2-4} & 
\mathrm{if} \hsf E < -2 \\
\end{cases}  
\end{equation}
%
Further more this can be used to evaluate the Green
function as
%
\begin{equation}
\bold{G}^r(E) =  
\begin{cases}
\frac{1}{\mp|\epsilon_0| + i\sqrt{4-E^2}}& 
\mathrm{if} \hsf |E| < 2 \\
\frac{1}{i\eta \mp |\epsilon_0| + \sqrt{E^2 - 4}  } 
& \mathrm{if}  \hsf E > 2 \\
\frac{1}{i\eta \mp |\epsilon_0| - \sqrt{E^2 - 4}} 
& \mathrm{if} \hsf E < -2 \\
\end{cases}  
\end{equation}
%
\begin{figure}
\centering
\includegraphics[width=0.4\linewidth]{figs/dm.pdf}
\caption{%
  Top most panel shows the density matrix evaluated by two
  methods. Black line (analytical) is evaluated by
  Eq.~\eqref{eq:spinless_rho} and orange dotted line is
  evaluated by Denis's method in Eq.~\eqref{eq:dm_denis}.
  The second panel shows the poles which are being summed
  over in Eq.~\eqref{eq:dm_denis} and the last panel shows
  the absolute value of the residues at those poles (dark
  blue means smaller). Here, in all panels we have chosen
  the onsite potential $\epsilon_0 = 1$ eV and the
  precision value $p=25$. Note that the jump around $\mu=2$
  eV is due to a quasi bound state which is why
  $E_{\min}$ = -3 eV was chosen.}\label{fig:fig4}
\end{figure}
%
\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{figs/rhoij.pdf}
\caption{%
  Figure shows the density matrix evaluated by two methods.
  The black is analytical calculation from
  Eq.~\eqref{eq:spinfull_rho} and the orange dotted line is
  calculation from Denis's density matrix. The left panel
  shows $\rho_{ii}(\mu)$ where $i = 1, 2$ and right panel
  shows $\mathrm{Im}\rho_{21}$ (Real part is 0). Here we
  have chosen $J_{sd} =  1$ eV, $p = 25$ and
  $E_{\min} = - 3$ eV.}\label{fig:fig5}
\end{figure}
%
Now that we have the expression for the retarded Green
function it can be analytically continued in the complex
plane by setting $E \to z$ where $z = x+ i y$. We note that
for evaluation of the density matrix what we need is
$\mathrm{Imag}~\bold{G}^r(E)$ which is given as
%
\begin{equation}
\mathrm{Imag}\bold{G}^r(E) =  
\begin{cases}
-\frac{\sqrt{4-E^2}}{\epsilon_0^2 + 4-E^2}& 
 \mathrm{if} \hsf |E| < 2 \\
-\frac{\pi |\epsilon_{0}|}{\epsilon_{BS}}
\delta(E - \epsilon_{BS})\theta[1 \pm 1]  & 
\mathrm{if}  \hsf E > 2 \\
\frac{\pi |\epsilon_0|}{\epsilon_{BS}}
\delta(E - \epsilon_{BS})\theta[1 \mp 1] & 
\mathrm{if} \hsf E < -2 \\
\end{cases}  
\end{equation}
%
where $\epsilon_{BS} =  \sqrt{4+|\epsilon_0|^2}$ is the
energy where a bound state forms. Furthermore, the density
matrix of the system is hence given by
\begin{equation}\label{eq:spinless_rho}
\begin{split}
\bm{\rho}(E_F) &= -\frac{1}{\pi}\int_{-\infty}^{+\infty} 
\mathrm{Img}\bold{G}^r(E) f(E-E_F)dE \\
   &= \frac{1}{\pi}\int_{-2}^{+2} 
   \frac{\sqrt{4-E^2}}{\epsilon_0^2 + \sqrt{4-E^2}} f(E- E_F) dE\\
   & \hspace{2cm}\pm 
   \frac{|\epsilon_0|}{\epsilon_{BS}}f(\epsilon_{BS}-E_F)
\end{split}
\end{equation}
%
\section{Example: Single TB spinfull site }
%
 Let us now consider the situation of having a single spin
 in the system which points in the $y$-direction which
 makes the Hamiltonian complex. Then we have
%
 \begin{equation}
 \bold{G}^r(E) = \bigg[ J_{sd}
 \begin{pmatrix}
 0 & -i \\
i & 0  \\
 \end{pmatrix}
 + [E - \bold{\Sigma}^r(E)] \id_2 \bigg]^{-1}  
 \end{equation}
 %
It can be shown analytically that this leads to the
following result for the density matrix of the system
%
\begin{equation}\label{eq:spinfull_rho}
\begin{split}
\bm{\rho}(E_F) 
&=-\frac{1}{\pi}\int_{-\infty}^{\infty}
\mathrm{Img}\bold{G}^r(E)f(E-E_F)dE \\
&= \bm{\rho}_\mathrm{band} + \bm{\rho}_\mathrm{BS} \\
\end{split}
\end{equation}
%
where we have 
%
\begin{equation}
\bm{\rho}_\mathrm{band}= \bigg[\frac{1}{\pi}\int_{-2}^{+2}
\frac{\sqrt{4-E^2}}{J_{sd}^2 + 4 -  E^2} f(E-E_F) dE \bigg]  \id
\end{equation}
and
\begin{equation}
\begin{split}
\bm{\rho}_\mathrm{BS} &= 
\frac{1}{2}\int_{-\infty}^{+\infty} dE f(E-E_F) \\
& \times \frac{J_{sd}}{\sqrt{4+J_{sd}^2}} 
\bigg[\delta\bigg(E + \sqrt{4+J_{sd}^2}\bigg) - 
\delta\bigg(E - \sqrt{4+J_{sd}^2} \bigg)
\bigg]  \\
&\times (\id  + \hat{\sigma}^y)
\end{split}
\end{equation}
In this form it is also clear that the spin density truly
comes from the bound states and not from the bands
themselves. Now we try to match the diagonal elements in
Eq.~\eqref{eq:spinfull_rho}. These two examples provide a
concrete way of testing the evaluation of equilibrium
density matrix in Eq.~\eqref{eq:dm}. The results showing
match of analytical formulae above and Denis's equilibrium
are shown in Fig.~\ref{fig:fig4} and Fig.~\ref{fig:fig5}.

%\section{Python function}
%I have written a python function which can be called to
%evaluate the equilibrium density matrix of the system using
%this approach. The function takes in a Kwant system and
%then evaluates the density matrix. It can be called from
%the  \texttt{equilibrium.py} module of  \texttt{td2d}
%package as \begin{verbatim}
%from td2d.equilibrium import Equilibrium as eq
%
%rho = eq.rho_denis(sys, sys_args, spin, 
%                   mu, temp, Emin, p=p)
%\end{verbatim}
%The various elements of this function are 
%\begin{itemize}
%\item  \texttt{sys} : Kwant system.
%\item  \texttt{sys\_args} : list of argument of Kwant system. 
%\item  \texttt{spin} : True or False based on spin.
%\item  \texttt{mu} : Fermi energy of the system.
%\item  \texttt{temp} : Temperature of the system.
%\item  \texttt{Emin} : The lower bound of your system (always include the bound states).
%\item  \texttt{p} : The precision of your calculation.
%\end{itemize}
