\chapter{The Spins class}\label{ch:spins}


\section{Creating a uniform 2D array of spins}
We start by creating a tight-binding system to which we
want to bind the classical spins (magnetic moments). In 
order to simplify the building process, TD2D uses KWANT 
tight-binding object, so one usually has to start with 
building their system using {KWANT}. However, TD2D already
offers a simple function to build a simple 2D ribbon using
{KWANT}.

\begin{lstlisting}{language=Python}
from td2d.kwant_systems import make_ribbon
ribbon = make_ribbon(length=5, width=4)
\end{lstlisting}
After we created the ribbon, we import the classical
\verb|Spins| class from TD2D
\begin{lstlisting}{language=Python}
from td2d import Spins
\end{lstlisting}
This is the class that will hold our classical magnetic
moments. After importing it, we can build the classical
spins by providing the KWANT tight-binding object as a
single required argument to which we want to bind the
spins. Note that the present version only works for 2D
systems, so do not try to create a multilayer system and
pass it to the \verb|Spins| class

\begin{lstlisting}{language=Python}
cspins = Spins(ribbon)
\end{lstlisting}

Currently, the spins are created on every site of the KWANT
tight binding system. If we want to see how the created
system looks like, we can plot it using

\begin{lstlisting}{language=Python}
cspins.plot(filename='spins_3d.png')
\end{lstlisting}
or
\begin{lstlisting}{language=Python}
cspins.plot(plot_type='2d', filename='spins_3d.png')
\end{lstlisting}
\noindent Spins can be plotted using Matplotlibs
Axes3D object, (set by default, option \\
\verb|plot_type='3d'|), although they do not provide quite
nice looking plots (see below). Another option is
using only in-plane $xy$ projections of local moments,
while their $z$ projection is represented by color
(\verb|plot_type='2d'|). Graphs of the local moments can
be also produced using external libraries, such as Mayavi.
Local moment orientations are kept inside of the
\verb|Spins| class in the \verb|s| attribute as a $(N, 3)$
numpy array and can be saved or exported to Mayavi. 
Here $N$ is the number of sites, and the
second axis is for three components $x$, $y$, and $z$
coordinate of each spin. To save the local moments
\begin{lstlisting}{language=Python}
import numpy as np
np.savetxt('cspins.txt', cspins.s)
np.savez('cspins.npz', cspins=cspins.s)
\end{lstlisting}
The ordering of the local moment sites is
the same as the ordering of lattice sites in the KWANT
ribbon object that we passed when we created the
\verb|Spins| object. To check their actual positions, you
can use their \verb|pos_spins| attribute, which contains a
$(N, 2)$ numpy array of spin positions in the $x$ and $y$
direction.

\begin{lstlisting}{language=Python}
print(cspins.pos_spins)
\end{lstlisting}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.49\textwidth]{figs/plot.pdf}
\end{center}
\caption{\label{fig:plotted}
 Local moments plotted using Mayavi and default Matplotlib
 3D plotting function.}
\end{figure}

\section{Creating a custom spin configuration}


By default, when \verb|Spins| object is created, 
all classical moments are pointing in the same
direction (along the $z$ axis). There are two ways to
control the initial spin orientation. The first is to 
provide a tuple with three floats to \verb|spin_config| 
keyword argument, like in the following example

\begin{lstlisting}{language=Python}
from td2d import Spins
from td2d.kwant_systems import make_ribbon

 ribbon = make_ribbon(length=10, width=4)
 cspins = Spins(ribbon, spin_config=(1, 1, 1))
 cspins.plot(plot_type='3d', filename='spins.png')
\end{lstlisting}
The created ribbon is shown in Fig.~\ref{fig:tspins}. Note
that although the provided tuple was not a unit vector,
when local spins are created, they are normalized. In 
other words, \verb|spin_config| is providing just a 
direction vector for all spins, and it will always create
a uniform configuration.

\begin{figure}[htb]
  \begin{center}
  \includegraphics[width=0.35\textwidth]{figs/{tilted_spins}.pdf}
  \end{center}
  \caption{\label{fig:tspins}
  Uniform configuration of tilted spins obtained with
  \texttt{spin\_config = (1, 1, 1)} keyword argument.}
\end{figure}

\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.35\textwidth]{figs/domain_wall.pdf}
\end{center}
\caption{\label{fig}
A simple domain wall created by providing a configuration
function to TD2D \texttt{Spins} class \texttt{config}
keyword argument.
}
\end{figure}

In most of the use cases, we will not deal with uniform 
configurations of local spins. Instead, we will need to 
configure some noncollinear spin texture. The TD2D package
is allowing us to do that in a way that is very similar to
KWANT methods for building the system, but now for 3D
vectors. In order to make a custom spin configuration, 
one has to define a python function which accepts a single
KWANT \verb|Site| object and returns a 3D vector. For 
example, here is how one would define a simple magnetic
domain wall using TD2D. Firstly, one would define a
function
\begin{lstlisting}{language=Python}
import numpy as np
from math import cosh, tanh

def domain_wall(site, x_zero=5, width=2):
     x, y = site.pos
     spin = np.array([1.0/cosh((x_zero-x)/width), 
                      0, 
                      tanh((x_zero-x)/width)])
     return spin
\end{lstlisting}
then provide this function to \verb|config| keyword
argument of the TD2D \verb|Spins| object.
\begin{lstlisting}{language=Python}
from td2d import Spins
from td2d.kwant_systems import make_ribbon

ribbon = make_ribbon(length=10, width=4)
cspins = Spins(ribbon, config=domain_wall)
cspins.plot(plot_type='3d', filename='domain_wall.png')
\end{lstlisting}


Note that provided function has only one required argument,
but a user can set arbitrary number of keyword arguments to
it. This building mechanism is somewhat similar to KWANT
mechanism of building a tight-binding system. In this case, 
the \verb|Spins| class iterates through all the 
\verb|Site| objects of the provided KWANT
tight-binding system, calls the provided \verb|config| 
function on each site, and computes the value of spin for
that site. Finally, we can see how the constructed domain 
wall looks like (see the figure on the next page).

\section{Solving the LLG equation}%

TD2D package offers a basic solver for the dynamics of
local magnetic moments, based on the Landau-Lifshitz-Gilbert
(LLG) equation. The \verb|Spins| class provides a way to
specify several terms in this equation responsible for
generation of effective magnetic fields. For more details
about the LLG equation and its parameters, and how they 
relate to arguments provided to the \verb|Spins| class, see
Chapter~\ref{ch:llg_theory}.
Currently, the \verb|Spins| class with its LLG solver
supports
\begin{itemize}
 \item Space and time varying magnetic anisotropy.
 \item Space and time varying exchange interaction.
 \item Space and time varying external magnetic field.
 \item Space and time varying demagnetization field.
 \item Two types of space and time varying exchange
       interactions: one to drive the classical moments in
       LLG equation, and another to create effective onsite
       potential necessary for time-dependent nonequilibrium
       electron dynamics.
\end{itemize}

In the next few subsections, we show examples on how to
propagate local magnetic moments with nonequilibrium
electron spin density, external magnetic field,
magnetic anisotropy etc. At the end of this chapter,
we show how to define a custom function for the exchange
coupling.

\section{Propagation with nonequilibrium spin density}

After we created the local magnetic moments
(see the previous section) we need to define
nonequilibrium electron spin density that will be used to
propagate these moments. The spin density is a
$(N, 3)$ numpy array (same dimension as the local 
\verb|s| attribute in the \verb|Spins| object). First, without performing any
electron dynamics, we can create a fake spin density
(for example along the $x$ direction) just to
demonstrate how propagation using \verb|Spins| object
works (we assume the \verb|Spins| object is the same one
as in the last subsection, namely the \verb|cspins| object).
\begin{lstlisting}{language=Python}
ribbon = make_ribbon(length=5, width=4)
cspins = Spins(ribbon, jsd_to_llg=0.1, jsd_to_negf=0.1)

s_cd = np.zeros(cspins.s.shape)
\end{lstlisting}
Then we set the $x$ component of this fake spin
density to some nonzero value
\begin{lstlisting}{language=Python}
s_cd[:, 0] = 0.1
\end{lstlisting}

Again, remember that \verb|s_cd| has the same shape as the
\verb|s| attribute, that is $(N, 3)$, so the zeroth
component corresponds to the $x$-direction. Also, we have set
the two components of the $J_{\textrm{sd}}$ coupling
constant when we defined the \verb|Spins| class. One,
\verb|jsd_to_llg| is the coupling from \verb|s_cd| to
 \verb|s| in the LLG, while another \verb|jsd_to_negf| is
used when one wants to compute onsite energies in a KWANT
tight-binding system
coming from the local magnetic moments (see below).
%
Finally, we use the nonequilibrium spin density to
propagate the local magnetic moments
%
\begin{lstlisting}{language=Python}
cspins.llg(s_cd, time=0)
\end{lstlisting}
%
This will propagate our local moments for a single time
step (by default time step is set to 0.1 fs, and can be
modified when \verb|Spins| class object is created by
changing the \verb|dt| keyword argument). If
we want to propagate for more time steps, we create a loop
%
\begin{lstlisting}{language=Python}
for time in np.arange(0, 10, 0.1):
    print(time)
    cspins.llg(s_cd, time)
\end{lstlisting}

A full example on how to propagate and save spins
(in a \verb|.txt| file for example) is given in the
file \verb|examples/llg/propagate.py|, also shown here

\begin{lstlisting}{language=Python}
from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np

# First we create a finalized KWANT tight-binding system
ribbon = make_ribbon(length=5, width=4)

# Now we create classical spins
# and set a uniform Jsd coupling
cspins = Spins(ribbon, jsd_to_llg=0.1, jsd_to_negf=0.1)

# We can create a uniform (fake) nonequilibrium spin density.
# It needs to have the same shape as classical spins
s_cd = np.zeros(cspins.s.shape)

# We set it nonzero along the x axis
s_cd[:, 0] = 0.1

# Next we propagate the classical spins
times = np.arange(0, 10, 0.1)

with open('moments_vs_time.txt', "w") as f:

for time in times:
    out_str = '%.2f ' % time
    print(out_str)

    cspins.llg(s_cd, time)

    for s in cspins.s:
        out_str += '%0.5e %0.5e %0.5e ' % (s[0], s[1], s[2])

    out_str += '\n'
    f.write(out_str)
\end{lstlisting}
%
Note that with a simple \verb|if| statement, one can
additionally save (or plot) local spins on every $n$-th time
step. Also, the \verb|cspins.llg()| function is called
with \verb|time| argument. This
argument will become important when we want to deal with
properties which are changing in time, as we will show
later.


\section{Modifying the global anisotropy}

In the next example, we will create a tilted spin
configuration, place it in a uniform anisotropy field along
the $y$ direction, set the Gilbert damping to some
custom value, and propagate the local spins.

\begin{lstlisting}{language=Python}
from td2d import Spins
from td2d.kwant_systems import make_ribbon
import numpy as np


ribbon = make_ribbon(length=5, width=4)
cspins = Spins(ribbon, spin_config=(1, 1, 1), ani=0.1,
               ani_vec=(0., 1, 0), g_lambda=0.2)

scd = np.zeros(cspins.s.shape)
times = np.arange(0, 100, 0.1)

for time in times:
    print(time)
    cspins.llg(scd, time)
    cspins.plot(filename='fig_%03.0f.png' % (10*time))
\end{lstlisting}

Note that uniform spin configuration is set with
\verb|spin_config=(1, 1, 1)|. Although the provided direction
is not a unit vector, as we said previously, TD2D will
normalize it. Next, the parameter \verb|ani=0.1| sets the
uniform value for the anisotropy constant to 0.1, and
lastly the \verb|ani_vec=(0, 1, 0)| and \verb|g_lambda=0.2|
sets the anisotropy direction along the $y$ axis, with
Gilbert damping set to 0.2.

\section{External magnetic field, demagnetization and
         exchange coupling}
In a similar way as with anisotropy, one can set the direction
and strength of an external magnetic field
%
\begin{lstlisting}{language=Python}
cspins = Spins(ribbon, bf_vec=(0, 1, 0), bf=2.5)
\end{lstlisting}
%
or Heisenberg exchange interaction
%
\begin{lstlisting}{language=Python}
cspins = Spins(ribbon, jexc=0.1)
\end{lstlisting}
%
or demagnetization field
%
\begin{lstlisting}{language=Python}
cspins = Spins(ribbon, demag_vec=(0, 1, 0), demag=0.001)
\end{lstlisting}
%
by default, all these are set to zero. The default directions
for the anisotropy and external magnetic field is along the
$z$ axis, while for demagnetization field is along the
$y$ axis, therefore if this is the preferred direction
the user needs to set only the corresponding scalar value
\verb|ani|, \verb|bf|, or \verb|demag|. Also, since all
these are keyword arguments, one can turn on anisotropy
and magnetic field (or demagnetisation) at the same time.

The TD2D is using KWANT lattice object to find neighbors of
a moment (neighbors of a moment corresponds to moments
which are connected to neighbors of a site to which the
current moment is connected). In other words, the nearest
neighbor coupling set in a KWANT
tight-binding system will correspond to nearest-neighbor
coupling between moments
in TD2D, and all couplings are the same. In the next example,
we will show how one can create custom fields and exchange
couplings.


\section{Create space-varying fields}
Let us imagine a simple example of two magnetic materials
creating an interface. They are both 2D and have equal
(square) lattices. There is a specific Heisenberg exchange
coupling between them on the interface, and they have
different intrinsic exchange coupling constants, as well as
different values and directions of anisotropies. In order
to simulate these two materials, one would have to define a
ribbon system, and two additional functions for the
exchange coupling and anisotropy which vary in the real
space. In the following example we show how to do that.
Lets assume that the ribbon length is ten sites, and that
the first five sites are created from the first material,
and last five sites are created from the second material.
First, we would have to define an anisotropy function which
points in one direction for the first five rows of atoms,
and then points in some other direction for the last five
rows.
%
\begin{lstlisting}{language=Python}
def custom_anisotropy(time, site):

    ani1 = 0.5 # Anisotropy strength of the first material
    ani2 = 0.1 # Anisotropy strength of the second material

    # Anisotropy directions in the first and second material
    vec1 = np.array([0, 0, 1])
    vec2 = np.array([0, 1, 0])

    x, y = site.pos

    if x < 5:
       return ani1*vec1
    else:
       return ani2*vec2
\end{lstlisting}
%
Note that this function has a simple interface: the first
argument it takes is a time parameter, while the second is
a KWANT \verb|Site| object from which one can determine the
position of a site for which we want to set the anisotropy. The
function returns a vector which is an effective magnetic
field coming from the anisotropy part (see the section on
how we solve the LLG equation (still missing)). In a
similar way, one can redefine other onsite properties,
like external magnetic field or demagnetization field on
each moment. The modifying function would have the same
interface (two arguments, one for time and one for
\verb|Site| object), and would return effective magnetic
field for these terms (again a 3D vector represented as a
numpy array of three floats).

When it comes to modifying the exchange coupling, the
interface of the user defined function would be slightly
different. The exchange interaction depends on two
moments, so one needs two site objects,
while the redefined function needs to return only a scalar
value (the exchange coupling strength).
%
\begin{lstlisting}{language=Python}
def custom_exchange(time, site1, site2):

    jexc_1 = 0.1   # Exchange in the first material
    jexc_2 = 0.2   # Exchange in the second material

    jexc_12 = 0.5  # Exchange on the interface

    x1, y1 = site1.pos
    x2, y2 = site2.pos

    if x1 < 5 and x2 < 5:
        return jexc_1
    elif x1 > 4 and x2 > 4:
        return jexc_2
    else:
        return jexc_12
\end{lstlisting}
%
The created fields can be included into \verb|Spins| class
with
\begin{lstlisting}{language=Python}
cspins = Spins(ribbon, ani_func=custom_anisotropy,
               jexc_func=custom_exchange)
\end{lstlisting}
The final spin configuration is shown in the following figure
%
\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.49\textwidth]{figs/interface.pdf}
\end{center}
\caption{\label{fig:intf}
A configuration of the interface.
}
\end{figure}
%
The full code example for this system is given in
\verb|examples/llg/space_varying_fields.py|. Note that
similarly to anisotropy, one can define space varying 
demagnetisation field and pass it \verb|demag_func|, or
space varying magnetic field and pass it to \verb|bf_func|.
Again, as with anisotropy, the return of these
functions should be an effective magnetic field coming from
these terms. Since the first argument of all these functions
is time, beside space, one can create 
time-varying fields --- as we show in the next example.


\section{Time-varying fields --- An example of switching 
         on and off magnetic field}

As we already showed in the previous example, the first
argument of the redefined fields is time (time in LLG code
is given in units of femtoseconds) so one can define
varying fields as in the present example, where we define a
magnetic field function which is switched on only between
10 and 20 femtoseconds, otherwise is zero.
%
\begin{lstlisting}{language=Python}
def custom_bfield(time, site):

    if 10 <= time <= 20:
        return np.array([0, 1000, 0])
    else:
        return np.array([0, 0, 0])
\end{lstlisting}
%
Again, this function is passed when we create the classical
spins object
%
\begin{lstlisting}{language=Python}
cspins = Spins(ribbon, spin_config=(1, 0, 0), g_lambda=0.2,
               bf_func=custom_bfield)
\end{lstlisting}
%
Note that in case of magnetic field, the effective field is
equal to the magnetic field, so in the previous example,
the field between 10 and 20 femtosecond is set to 1000 T
along the $y$ direction.

\section{Changing the coupling between electron spin
         density and local moments}
TD2D provides two functions to describe coupling between
electron spins and local magnetic moments. One is the
coupling from electron spin density to magnetic moments in
the LLG solver
(set by \verb|jsd_to_llg| argument), the other is the
coupling necessary to compute the onsite energy 
modification coming from local magnetic moments 
\verb|jsd_to_negf|, see the next section.
By default, these two variables set a uniform (in time and
space) coupling on each site. However, similarly to local
field properties, both these functions can be overwritten
by some custom user-defined function which varies in time
or in space. In this section we provide an example on how
to do that for \verb|jsd\_to\_llg|
variable. We will define a custom function which sets
nonzero coupling between electron spin density and local
moments only in a narrow ring region. The custom function
has a similar interface as in the previous examples with
anisotropy and magnetic field, except it returns a scalar
value for the $J_{\textrm{sd}}$ coupling.

\begin{lstlisting}{language=Python}
def custom_to_llg(time, site):

    """Couple only spins in a ring with Jsd coupling """
    x, y = site.pos

    in_outer = (x-10)**2 + (y-10)**2 < 9**2
    in_inner = (x-10)**2 + (y-10)**2 < 7**2

    if in_outer and not in_inner:
        return 0.3
    else:
        return 0.0
\end{lstlisting}
%
The default \verb|jsd_to_llg| function can be overwritten
with 
\begin{lstlisting}{language=Python}
cspins = Spins(ribbon, spin_config=(0, 0, 1),
               jsd_to_llg_func=custom_to_llg)
\end{lstlisting}
%
The full example code is given in 
\verb|examples/llg/custom_jsd.py|. The final result is
shown in the following figure
%
\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.49\textwidth]{figs/ring.pdf}
\end{center}
\caption{\label{fig:ring}
Ribbon with $J_{\textrm{sd}}$ coupling set only in the
ring region.}
\end{figure}

Since the coupling was nonzero only in the ring region, 
only moments in this region align with the nonequilibrium
electron spin density (set along the $x$ direction in this
example).

\section{Computing the onsite energies}
Another functionality of the \verb|Spins| class is the
computation of the onsite energy modification that each
magnetic moment creates on a site to which it is attached.
This energy is 
\begin{equation}
  \Delta U_{i}(t) = J_{\textrm{sd}}
  \,{\boldsymbol \sigma}\cdot\mathbf{S}_i(t)
\label{eq:delta_u}
\end{equation}
where $J_{\textrm{sd}}$ is the coupling
strength, ${\boldsymbol \sigma}$ is the vector of Pauli
matrices and $\mathbf{S}_i(t)$ is the local magnetic
moment (spin).

Similarly to \verb|jsd_to_llg|, as we already explained,
there is a default \verb|jsd_to_negf| parameter witch sets
a uniform $J_{\textrm{sd}}$ coupling on each site. 
Again, this can be overwritten with some custom function
which is to be passed to \verb|jsd_to_negf_func| similarly
as in the previous example with \verb|jsd_to_llg_func|.
Note that although we use the same label $J_{\textrm{sd}}$
in Eq.~\ref{eq:delta_u} and in the LLG equation in
Chapter~\ref{ch:llg_theory},
the label in Eq.~\ref{eq:delta_u} refers to a coupling from 
local moments to electrons (since it influences electron
dynamics), whereas that in
Chapter~\ref{ch:llg_theory} refers to coupling from 
electrons to local moments (since it influences dynamics of 
local moments). This distinction is clear
in the definition of the two corresponding parameters 
of the \verb|Spins| class
(\verb|jsd_to_negf| and \verb|jsd_to_llg|).

To compute the onsite energies, the \verb|Spins| class
offers the \verb|onsite| function. The function also
requires a single argument which is time (since
\verb|jsd_to_negf_func| can also be time varying).
\begin{lstlisting}{language=Python}
cspins.onsite(time)
\end{lstlisting}
The function returns a $(N, 2, 2)$ numpy array of onsite
energies. That is $N$-length array of $(2\times2)$ matrices 
for $N$ sites. This array can be further used to modify
the electronic Hamiltonian, since the \verb|Device| class
offers an attribute \verb|delta_onsite| to directly modify
the onsite energies in a spinfull system. Therefore, 
a single line
\begin{lstlisting}{language=Python}
some_device.delta_onsite = cspins.onsite(time)
\end{lstlisting}
is sufficient to couple local magnetic moments in the
\verb|cspins| object to tight-binding sites in the 
\verb|some_device| object (which is an instance of the
\verb|Device| class). This modification needs to happen at
every time step, meaning that it needs to be inside of the
time-propagation loop. We will see more examples of this
in Chapter~\ref{ch:tdnegf_llg}.
