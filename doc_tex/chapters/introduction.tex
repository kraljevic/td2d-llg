\chapter{Introduction to TD2D}
TD2D is a Python package that simulates time-dependent
quantum transport in open 2D systems, with an optional
coupling to classical magnetic moments. TD2D can be viewed
as an extension of a widely used KWANT package,
since it is mostly based and operates with objects defined
in {KWANT}. For a reader who is not familiar with KWANT, we
suggest reading the KWANT manual first, before
working with 
TD2D. In this part we give a basic introduction to
TD2D. We start by presenting its main classes and how they
interact with each other. In the following chapters, we
will go into details of explaining each class separately.


\section{Organisation of the TD2D package}

\subsection{Main classes and the workflow}
The following figure gives us a schematic view of the TD2D
package. It consists mostly out of two main classes
which are the \verb|Device| class, and the \verb|Spins|
class.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.500\linewidth]{figs/scheme.pdf}
\caption{The main classes of the TD2D package and how they
are related to each other. Black arrows show how data
flows between class instances. Red arrows shows which
class instance requires which in order to get build.
}
\end{figure}

Since TD2D is an extension of the KWANT package, we do
not build our system from scratch. Instead, we start from
KWANT tight-binding system, that is an instance of the
\verb|FiniteSystem| class in {KWANT}. This system is
called \verb|TB System| in our previous diagram. TD2D
currently assumes this system is attached to at least
one lead, and that all leads are made from square lattices.
Beside this limitation, there are no limitations on the
nature of tight-binding Hamiltonian in the main scattering
region.%
\footnote{The only restriction on the Hamiltonian in the
main scattering region is that each site has to have either
a single orbital (a spinless site) or two orbitals (a
spinfull site). Heterogeneous systems in which different 
sites have different numbers of orbitals are not supported.
Also, systems containing only a single orbital per site 
can not be coupled to local magnetic moments.}

As we said before, we start by building a {KWANT}
\verb|FiniteSystem| object with at least one lead attached 
to it.  Since TD2D allows for self-consistent simulation of both
classical magnetic moments and quantum spin-polarized
electrons, the two main classes---the
\verb|Spins| class and the \verb|Device| 
class---serve that purpose. Both these classes are
constructed using a \verb|FiniteSystem| object, that is 
a \verb|TB System| in our notation 
(see the red arrows (1) and (2) in the TD2D schematic
figure). By providing the same instance of the KWANT
system to builders of these two classes, we allow for the
same site indexing in both classes. We also use KWANT
to build time-dependent Hamiltonian of our \verb|TB System|,
its solvers for the lead selfenergies, its site indexing,
etc.

The \verb|Device| class is basically an extension of the
KWANT \verb|FiniteSystem| class with few extra attributes and
functions to allow for simpler manipulation of
time-dependent Hamiltonians. Since KWANT can create
parametrized tight-binding systems, it is important to
specify how TD2D implements time-varying parameters. Beside
a tuple of constant argument values, how they are usually
supplied in KWANT, TD2D allows for argument values which
are functions of a single variable---time, and these
functions are supplied to the \verb|Device| class
during its construction (as an argument list). We give an
example how this is done in Chapter~\ref{ch:device}. 
This also applies to time-dependent voltage signals
on the leads (see the two green rectangles for
$\textrm{V(t)}$ and ${\textrm{Args(t)}}$ in the TD2D
schematics). These
parameters will then be passed to KWANT at every time step
in order to extract the time-dependent Hamiltonian. The
purpose of the \verb|Device| class is intermediary, to
facilitate creation of the time-dependent Hamiltonians, and
to pass those Hamiltonians to time evolution solvers. 

The time-evolving state of a tight-binding system
can be described either by its wave function or by its
density matrix. The \verb|Device| class does not contain
this data. Instead, we keep it in the 
TD-NEGF solver class. The purpose of this class is to 
keep and to evolve the wave function of our device in time.
There are several different
implementations of this class. This is partly because there
are several ways to implement the time evolution (both
mathematically and in the code). In order to create a 
TD-NEGF solver object, we need to supply a \verb|Device|
object as an input argument 
(see the red arrow (3) in the TD2D schematics). 
Additionally, the TD2D package provides an
equilibrium density matrix solver. Again, a \verb|Device|
object is required as an input argument to create this 
solver.
The purpose of both equilibrium and nonequilibrium solvers
is to compute the corresponding density matrices at each
time step. The \verb|Device| object will supply them
the time-dependent Hamiltonian to do so. Additionally, the 
equilibrium density matrix solver can be used outside of
the time loop, to compute the density matrices
in static systems.

In the last step, there are objects which represent
different measurement operators. They use the supplied
density matrices to compute the values of local
quantities (e.g.\ spin and charge density, spin
and charge currents, etc). Some of these objects need to
be initialized with an instance the \verb|Device| class,
as we will show later (Chapter~\ref{ch:device}) when we
describe the \verb|Device| class in more details.

Since TD2D works with both electrons and classical magnetic
moments, the \verb|Spins| class is responsible for time
evolution of the later. It
needs to be constructed using the \verb|TB System|
(red arrow (2) in the TD2D schematics) independently from
the \verb|Device| class. TD2D can be viewed as a collection
of two independent packages. The \verb|Spins| class has its
own methods for manipulating and propagating local
magnetic moments, based on the
Landau-Lifshitz-Gilbert (LLG) equation solver, while
the \verb|Device| class propagates electrons in an
open tight-binding system using the time-dependent
nonequilibrium Green function approach (TD-NEGF).
Although the two classes (\verb|Spins| and
\verb|Device|) can be propagated independently from each
other, we can also couple them in a
self-consistent loop (also shown in the TD2D schematics). 
Then, a \verb|Spins| object would 
modify the local onsite energy ($\Delta U(t)$) of the
electronic sites, and electrons would influence the spin
dynamics through their nonequilibrium (current-driven) 
spin density 
$\langle \hat{\mathbf{s}}_{\textrm{neq}}(t)\rangle -
\langle\hat{\mathbf{s}}_{\textrm{eq}}\rangle$.

\section{What is in the rest of this manual}

In the next chapter (Chapter~\ref{ch:spins}) we 
describe how to create and use objects of the \verb|Spins| 
class. Chapter~\ref{ch:device} provides a similar 
description for the \verb|Device| class.
This separation between the \verb|Spins| class and 
the \verb|Device| class is for users who want to perform 
either only classical micromagnetics using LLG or only
quantum electron dynamics using {TD-NEGF}.
In Chapter~\ref{ch:tdnegf_llg}
we describe how to combine the two in a self-consistent
way, what we refer as TD-NEGF+LLG method.
The corresponding theoretical details on how the
\verb|Spins| and \verb|Device| class are propagated in time
are provided in
Chapters~\ref{ch:llg_theory} and~\ref{ch:tdnegf_theory},
respectively. These two chapters
(Chapter~\ref{ch:llg_theory} and
Chapter~\ref{ch:tdnegf_theory}) additionally explain some 
of the input arguments of the two classes, and how they 
enter into corresponding equations of motion. The
introductory chapters on the \verb|Spins| and \verb|Device|
classes (Chapters~\ref{ch:spins} and~\ref{ch:device})
will gradually introduce all these parameters through a
series of simple examples.

