import tinyarray as ta

HBAR = 0.658211951       # Plancks constant [eV * fs]
KB = 8.6173324e-5        # Boltzman constant [eV / K]

MU_BOHR = 5.788381e-05   # Bohr magneton [eV / T]
GAMMA = 1.760859644e-4   # Gyromagnetic ratio [rad / (fs*T)]

UNIT = ta.array([[1., 0.],
                 [0., 1.]])

SIG_X = ta.array([[0, 1],
                  [1, 0]])

SIG_Y = ta.array([[0., -1.j],
                  [1.j, 0.]])

SIG_Z = ta.array([[1.0, 0.0],
                  [0.0, -1.0]])
