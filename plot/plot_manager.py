#!/usr/bin/env python3
from matplotlib.colors import hex2color
from .plot_2d import pl_2d
from .plot_3d import pl_3d
from .plot_mayavi import plot_spins as pl_mayavi
import warnings

mayavi_avail = True
try:
    import mayavi.mlab as ml
    ml.options.offscreen = True
    bgcolor = 'white'
    bgcolor_mayavi = hex2color(bgcolor)
    mfig = ml.figure(size=(1280, 1280), bgcolor=bgcolor_mayavi)
    ml.view(azimuth=-45, elevation=45,
            distance=10, reset_roll=True,
            figure=mfig)
    from xvfbwrapper import Xvfb
    vdisplay = Xvfb(width=1024, height=720)
    vdisplay.start()
except ModuleNotFoundError:
    warnings.warn("Mayavi not installed. Printing option "
                  " with mayavi switched off")
    mayavi_avail = False


def plot_spins(self, plot_type='2d', filename='spins.png',
               plot_remote=False, ax=None, time=None,
               **kwargs):

    x = self.pos_spins[:, 0]
    y = self.pos_spins[:, 1]

    sx = self.s[:, 0]
    sy = self.s[:, 1]
    sz = self.s[:, 2]

    if plot_type == '2d':
        pl_2d(x=x, y=y, sx=sx, sy=sy, sz=sy, filename=filename,
              ax=ax, time=time)
    elif plot_type == '3d':
        pl_3d(x=x, y=y, sx=sx, sy=sy, sz=sz, filename=filename,
              ax=ax, time=time)
    elif plot_type == 'mayavi':
        pl_mayavi(x=x, y=y, sx=sx, sy=sy, sz=sz, ml=ml,
                  mfig=mfig, filename=filename, ax=ax,
                  time=time, **kwargs)
    else:
        raise ValueError('Unknown plot type ' + plot_type + '?')
