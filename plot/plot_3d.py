#!/usr/bin/env python3
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
Axes3D


def pl_3d(x, y, sx, sy, sz, filename=None, ax=None,
          time=None):

    if ax is None:
        fig = plt.figure(figsize=(3.375, 3.0))
        ax = fig.add_axes([0.0, 0.0, 1.00, 1.00],
                          projection='3d')

    ax.quiver(x, y, 0.0*x, sx, sy, sz, alpha=1.0,
              cmap='coolwarm', lw=2.,
              length=0.9, pivot='middle')

    ax.set_axis_off()
    ax.set_zlim(-3.1, 3.1)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.dist = 8.
    if time is not None:
        fig.text(0.05, 0.90, 'Time = %6.2f fs' % time, va='bottom',
                 ha='left', transform=ax.transAxes)
    if filename is not None:
        plt.savefig(filename, dpi=400)
