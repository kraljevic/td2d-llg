import matplotlib.pyplot as plt


def plot_spins(x, y, sx, sy, sz, ml, mfig,
               time=None, filename=None, ax=None,
               azimuth=-38, elevation=45, distance=40):

    arrows = ml.quiver3d(x, y, 0*y, sx, sy, sz, scalars=sz,
                         mode='arrow', scale_factor=0.8,
                         colormap='bwr', figure=mfig,
                         vmin=-1, vmax=1)

    arrows.glyph.color_mode = 'color_by_scalar'
    arrows.glyph.glyph_source.glyph_position = 'tail'

    ml.points3d(x, y, 0*y, color=(0.0, 0.0, 0.0),
                scale_factor=0.1, resolution=10, figure=mfig)

    ml.view(azimuth=azimuth, elevation=elevation,
            distance=distance,
            focalpoint=(x.mean(), y.mean(), 0.),
            figure=mfig)

    img_3d = ml.screenshot(antialiased=True, figure=mfig)

    if ax is None:
        fig = plt.figure(figsize=(3.0, 3.0))
        ax = fig.add_axes([0.0, 0.0, 1.0, 1.0])
        ax.set_xticks([])
        ax.set_yticks([])

    if time is not None:
        ax.text(0.1, 0.9, 'Time = %6.2f fs' % time, color='white',
                transform=ax.transAxes)
    ax.imshow(img_3d)
    if filename is not None:
        plt.savefig(filename, dpi=400)
    ml.clf(mfig)
    if ax is None:
        plt.close(fig)
