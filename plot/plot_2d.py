#!/usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np


def pl_2d(x, y, sx, sy, sz, filename=None,
          ax=None, time=None):

    if ax is None:
        fig = plt.figure(figsize=(3.375, 3.0))
        ax = fig.add_axes([0.05, 0.10, 0.80, 0.80])
        cax = fig.add_axes([0.85, 0.10, 0.03, 0.80])

    norm = np.sqrt(sx**2 + sy**2)

    css = ax.quiver(x, y, sx/norm, sy/norm,
                    sz, alpha=0.8, cmap='coolwarm', pivot='middle')
    css.set_vmin = -1
    css.set_vmax = 1
    css.set_clim(-1, 1)
    cb = plt.colorbar(css, cax=cax, orientation='vertical',
                      ticks=[-1, 0, 1])
    ax.text(1.0, 1.05, r'$\mathbf{M}_z$', va='center', ha='left',
            transform=ax.transAxes)
    cb.set_ticks([-1, 0, 1])

    ax.set_xticks([])
    ax.set_yticks([])
    if time is not None:
        ax.text(0.0, 1.02, 'Time = %6.2f fs' % time, va='bottom',
                ha='left', transform=ax.transAxes)

    if filename is not None:
        plt.savefig(filename, dpi=400)
