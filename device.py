from .kwant_wrapper import get_xi
from kwant.builder import FiniteSystem
from .kwant_wrapper import get_ham
from functools import reduce
import kwant
import numpy as np

adj = np.matrix.getH


class Device:

    def __init__(self, sys, args=None, mu_leads=None):
        """A simple class to extend the KWANT's
        ``FiniteSystem`` object by adding additional
        usefull attributes.

        Parameters
        ----------

        sys : an instance of KWANT's :class:`FiniteSystem`
            A finalized tight-binding system.
        args : list of constants or functions (optional)
            Arguments of the parametrized Hamiltonian.
            By default, this parameter is `None`, which
            corresponds to non-parametrized Hamiltonian.
            In case of a parametrized Hamiltonians,
            it can be either a list of constant numbers,
            (which corresponds to a parametrised Hamiltonian
            constant in time), or it can be a mixed list of
            numbers and functions of a single parameter
            (time) to describe a time-dependent, parametrized
            Hamiltonian.
        mu_leads : list of constants or functions (optional)
            Similar as ``args`` parameter, but now for
            potentials on the leads.

        Attributes
        ----------

        xi : numpy 1D array
            An array of :math:\\xi\\rangle` vectors obtained
            by decomposing incoming modes in the leads into
            separate channels.

        xi_cent : numpy 1D array
            An array of energy positions where are the
            semi-circle maximums positoned for channels in a
            square lattice lead after solving the eigenvalue
            problem.

        delta_onsite : numpy 1D or 2D array
            An onsite modification of the Device Hamiltonian.
            This attribute is used particularly so that
            interaction with classical magnetic moments could
            be easly implemented withouth modifyig the
            initial (possibly parametrized) Hamiltonian of
            the ``sys`` object. If the Hamiltonian is
            spinfull, then this should be a (N, 2, 2) numpy
            array, for a spinless Hamiltonian, it's a 1D
            array of N elements. Here, N is the number of
            sites.
        """

        assert isinstance(sys, FiniteSystem), (
            'The Device class requires an instance of '
            'kwant.builder.FiniteSystem class to '
            'initialize.')
        xi, xi_cent, lead_info = get_xi(sys, args=(args))
        self.xi = xi
        self.xi_cent = xi_cent
        self.lead_info = lead_info
        self.nlead = len(sys.leads)
        self.dim_h, self.nc_tot = np.shape(xi)
        self.sys = sys
        n_orb = self.dim_h / len(self.sys.sites)
        self.nsites = len(self.sys.sites)
        self.spinfull = (n_orb == 2)
        if self.spinfull:
            self.__delta_onsite = np.zeros(
                (len(self.sys.sites), 2, 2))
        else:
            self.__delta_onsite = np.zeros((self.dim_h),
                                           dtype=float)

        if args:
            err_str = 'Arguments must be provided as a list'
            assert isinstance(args, list), err_str
            for element in args:
                is_func = hasattr(element, '__call__')
                is_int = isinstance(element, int)
                is_float = isinstance(element, float)
                is_complex = isinstance(element, complex)
                accept = (is_func or is_int or
                          is_float or is_complex)
                estr = 'Args should be numbers or functions'
                assert accept, estr
        self.args = args

        if mu_leads is None:
            self.mu_leads = np.zeros((self.nlead))
        else:
            assert isinstance(mu_leads, list), (
                'Leads potentials "mu_leads" must be a '
                ' list of floats or ints')
            assert len(mu_leads) == self.nlead, (
                'Lead potentials "mu_leads" must be of '
                'the same length as the number of leads = '
                '%d ' % self.nlead)
            for val in mu_leads:
                assert (isinstance(val, float) or
                        isinstance(val, int)), (
                        'Lead potentials in "mu_leads" must '
                        'be floats or integers')

            self.mu_leads = mu_leads

    @property
    def delta_onsite(self):
        return self.__delta_onsite

    @delta_onsite.setter
    def delta_onsite(self, value):
        value = np.array(value)
        if self.spinfull:
            err_msg = ('The delta_onsite should be an array'
                       'of shape (%d, 2, 2)' % self.nsites)
            if not value.shape == (self.nsites, 2, 2):
                raise ValueError(err_msg)
        else:
            err_msg = ('The delta_onsite should be an array'
                       'of shape (%d) ' % self.nsites)
            if not value.shape == (self.nsites):
                raise ValueError(err_msg)
        self.__delta_onsite = value

    def get_args(self, time):
        '''Get time-dependent Hamiltonian parameters

        Based on the args list provided during the
        initialization, return the parametrized
        Hamiltonian arguments for a given time.
        '''

        if self.args:
            carg = []
            for arg in self.args:
                if hasattr(arg, '__call__'):
                    carg.append(arg(time))
                else:
                    carg.append(arg)
            return np.array(carg)
        else:
            return None

    def get_mu(self, time):
        '''Return time-parametrized potentials on the leads

        Depending on the ``mu_leads`` parameter provided
        during the initialization, return lead potentials
        at a given time (time is in femtoseconds).
        '''

        if self.mu_leads:
            mu_args = []
            for mu in self.mu_leads:
                if hasattr(mu, '__call__'):
                    mu_args.append(mu(time))
                else:
                    mu_args.append(mu)
            return np.array(mu_args)
        else:
            np.zeros(self.nlead)

    def trans_kw(self, energy):
        smatrix = kwant.smatrix(self.sys,
                                energy=energy,
                                args=[0, 0])
        return smatrix.transmission(1, 0)

    def trans(self, energy):

        se_L = self.se_lead(energy, time=0, lead_num=0)
        se_R = self.se_lead(energy, time=0, lead_num=0)
        gamma_L = 1j*(se_L - np.conj(se_L.T))
        gamma_R = 1j*(se_R - np.conj(se_R.T))
        gr = self.green(energy, time=0)
        ga = np.conj(gr.T)

        trans = reduce(np.dot, [gamma_R, gr, gamma_L, ga])

        diag = np.diag(trans)
        return diag.sum()

    def selfenergy(self, energy, time):
        se_total = 0.
        for lead_index, lead in enumerate(self.sys.leads):
            se_lead = self.se_lead(energy, time, lead_index)
            se_total = se_total + se_lead
        return se_total

    def se_lead(self, energy, time, lead_num=0):
        args = self.get_args(time)
        lead = self.sys.leads[lead_num]
        se_lead = np.zeros((self.dim_h, self.dim_h),
                           dtype=complex)
        intf = self.sys.lead_interfaces[lead_num]
        args = list(args)
        se = lead.selfenergy(energy, args=args)
        norb = int(se.shape[0]/len(intf))

        for i, atom_i in enumerate(intf):
            for j, atom_j in enumerate(intf):
                lai, hai = norb*atom_i, norb*atom_i + norb
                laj, haj = norb*atom_j, norb*atom_j + norb
                li, hi = norb*i, norb*i + norb
                lj, hj = norb*j, norb*j + norb
                se_lead[lai:hai, laj:haj] = se[li:hi, lj:hj]
        return se_lead

    def get_h(self, time):
        '''Return parametrized Hamiltonian for a given time

        The ``delta_onsite`` attribute is added on the
        main diagonal of the returned Hamiltonian. This
        attribute allows to easly include modifications
        created by the classical moments, without modifiying
        the parametric Hamiltonian.
        '''
        args = self.get_args(time)
        ham = get_ham(self.sys, args=args)
        if self.spinfull:
            for index, onsite in enumerate(self.delta_onsite):
                ham[2*index:2*index+2,
                    2*index:2*index+2] += onsite
        else:
            ham = ham + np.diag(self.delta_onsite)
        return ham

    def green(self, energy, time, eta=0.e-4):
        ham = self.get_h(time)
        en_eye = (energy + 1j*eta)*np.eye(self.dim_h)
        heff = ham + self.selfenergy(energy, time)
        gf = np.linalg.inv(en_eye - heff)
        return gf

    def gamma(self, energy, time, lead_num=0):
        se = self.se_lead(energy, time, lead_num)
        gamma = 1j * (se - adj(se))
        return gamma
