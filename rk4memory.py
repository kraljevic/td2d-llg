from .device import Device
import numpy as np
from .constants import HBAR
from .constants import KB
from .kwant_wrapper import lead_and_channel_finder as lc_find
from .fitting import MemoryFitter as Fitter


def fermi_fn(x):
    return 1. / (1 + np.exp(x))


class RK4memory:

    def __init__(self, device, emin=-2.1, emax=2.1,
                 estep=0.01, temp_k=100.0, efermi=0.0,
                 dt=0.1, gamma=1.0,
                 verbose=False):
        '''A simple propagation solver for TD-NEGF based on
        RK4 method

        Parameters
        ----------
        device : an instance of the Device class
            The device whose state we want to evolve in time.
            The solver needs this so it can extract the
            system hamiltonian and other usefull parameters.
        emin : float (optional)
            The bottom of the band. The solver needs this
            so it can compute the energy integral for the
            rho matrices. Default value -2.1.
        emax : float (optional)
            The top of the band. The solver needs this
            so it can compute the energy integral for the
            rho matrices. Default value 2.1.
        estep : float (optional)
            The energy step used in integration of the
            rho matrices. Default value 0.01
        temp_k : float (optional)
            The temperature of the system in Kelvins (K).
            Default value 100.
        efermi : float (optional)
            The Fermi energy at which to perform the time
            propagation. Default value 0.0.
        nt : integer (optional)
            The dimension of omega vectors. It determines
            the maximum time interval where the solver
            produces acceptable results.
        gamma : float (optional)
            Hopping integral from the system to the leads.
            Leads are always assumed to be square lattice
            with uniform hopping to the system.
        weight : float (optional)
            Weight of the energy points depending upon
            the integration scheme used to do the energy
            integral. Default value is 0.01.
        verbose : boolean (optional)
            Print computation times necessary for profiling.
        '''

        assert isinstance(device, Device), (
            'The suplied object under device argument is '
            'not and instance of the Device class.')

        self.m = 27
        self.m_old = 100
        self.emin = emin
        self.emax = emax
        self.estep = estep
        self.egrid = np.arange(emin, emax+estep, estep)
        self.ne_grid = len(self.egrid)
        self.temp_k = temp_k
        self.efermi = efermi
        self.dt = dt
        self.dim_h = device.dim_h
        self.nc_tot = device.nc_tot
        self.gamma = gamma
        self.device = device
        self.verbose = verbose
        self.w = estep * np.ones(self.ne_grid)
        self.fitter = Fitter(self.device)
        self.psi = np.zeros((self.dim_h, self.nc_tot,
                             self.ne_grid), dtype=complex)

        self.z = np.zeros((self.m, self.nc_tot,
                          self.nc_tot, self.ne_grid),
                          dtype=complex)
        self.omega = np.zeros((self.nc_tot, self.nc_tot,
                              self.ne_grid), dtype=complex)
        self.omega_old = np.zeros((self.m_old, self.nc_tot,
                                   self.nc_tot,
                                   self.ne_grid))

    def propagate_new(self, time):

        # Evolve Psi, Omega for dt/2 using RK2
        dt = self.dt

        # Step 1
        dp1 = self.evolve_psi(time, self.psi, self.omega)
        do1 = self.evolve_omg(time, self.psi, self.z,
                              self.omega)
        # Step 2
        time_t = time + dt/4.
        psi_t = self.psi + dp1*dt/4.
        omega_t = self.omega + do1*dt/4.

        dp2 = self.evolve_psi(time_t, psi_t, omega_t)
        do2 = self.evolve_omg(time_t, psi_t, self.z, omega_t)

        # Step 3
        time_t = time + dt/4
        psi_t = self.psi + dp2*dt/4.
        omega_t = self.omega + do2*dt/4.

        dp3 = self.evolve_psi(time_t, psi_t, omega_t)
        do3 = self.evolve_omg(time_t, psi_t, self.z, omega_t)

        # Step 4
        time_t = time + dt/2
        psi_t = self.psi + dp3*dt/2.
        omega_t = self.omega + do3*dt/2.

        dp4 = self.evolve_psi(time_t, psi_t, omega_t)
        do4 = self.evolve_omg(time_t, psi_t, self.z, omega_t)

        delta_psi = (dp1 + 2*dp2 + 2*dp3 + dp4)/6.
        psi_half = self.psi + delta_psi*dt/2

        delta_omega = (do1 + 2*do2 + 2*do3 + do4)/6.
        omega_half = self.omega + delta_omega*dt/2

        # ==================================
        # Evolve zk for dt using RK2
        # ==================================
        dz1 = self.evolve_memory(self.z, psi_half)*dt
        z_half = self.z + dz1/2.
        dz2 = self.evolve_memory(z_half, psi_half)*dt
        z_half = self.z + dz2
        self.z = z_half

        # Step 1
        dp1 = self.evolve_psi(time, psi_half, omega_half)
        do1 = self.evolve_omg(time, psi_half, self.z,
                              omega_half)
        # Step 2
        time_t = time + dt/4.
        psi_t = psi_half + dp1*dt/4.
        omega_t = omega_half + do1*dt/4.

        dp2 = self.evolve_psi(time_t, psi_t, omega_t)
        do2 = self.evolve_omg(time_t, psi_t, self.z, omega_t)

        # Step 3
        time_t = time + dt/4
        psi_t = psi_half + dp2*dt/4.
        omega_t = omega_half + do2*dt/4.

        dp3 = self.evolve_psi(time_t, psi_t, omega_t)
        do3 = self.evolve_omg(time_t, psi_t, self.z, omega_t)

        # Step 4
        time_t = time + dt/2
        psi_t = psi_half + dp3*dt/2.
        omega_t = omega_half + do3*dt/2.

        dp4 = self.evolve_psi(time_t, psi_t, omega_t)
        do4 = self.evolve_omg(time_t, psi_t, self.z, omega_t)

        delta_psi = (dp1 + 2*dp2 + 2*dp3 + dp4)/6.
        self.psi = psi_half + delta_psi*dt/2

        delta_omega = (do1 + 2*do2 + 2*do3 + do4)/6.
        self.omega = omega_half + delta_omega*dt/2

    def propagate(self, time):

        dt = self.dt
        dp1 = self.evolve_psi(time, self.psi, self.omega_old,
                              old=True)
        # do1, dz1 = self.evolve_omg(time, self.psi, self.z,
        #                            self.omega, dt=dt)
        do1 = self.evolve_omg_old(self.psi, self.omega_old)

        # RK step 2
        time_t = time + dt/2.
        psi_t = self.psi + dp1*dt/2.
        omega_t = self.omega_old + do1*dt/2.
        # omega_t = self.omega + do1*dt/2.
        # zk_t = self.z + dz1*dt/2

        dp2 = self.evolve_psi(time_t, psi_t, omega_t,
                              old=True)
        do2 = self.evolve_omg_old(psi_t, omega_t)
        # do2, dz2 = self.evolve_omg(time_t, psi_t,
        #                            zk_t, omega_t, dt)

        # RK step 3
        time_t = time + dt/2.
        psi_t = self.psi + dp2*dt/2.
        omega_t = self.omega_old + do2*dt/2.
        # omega_t = self.omega + do2*dt/2.
        # zk_t = self.z + dz2*dt/2

        dp3 = self.evolve_psi(time_t, psi_t, omega_t,
                              old=True)
        do3 = self.evolve_omg_old(psi_t, omega_t)
        # do3, dz3 = self.evolve_omg(time_t, psi_t,
        #                            zk_t, omega_t, dt)

        # RK step 4
        time_t = time + dt
        psi_t = self.psi + dp3*dt
        omega_t = self.omega_old + do3*dt
        # omega_t = self.omega + do2*dt
        # zk_t = self.z + dz2*dt

        dp4 = self.evolve_psi(time_t, psi_t, omega_t,
                              old=True)
        do4 = self.evolve_omg_old(psi_t, omega_t)
        # do4, dz4 = self.evolve_omg(time_t, psi_t,
        #                            zk_t, omega_t, dt)

        delta_psi = (dp1 + 2*dp2 + 2*dp3 + dp4)/6.
        self.psi = self.psi + delta_psi*dt

        delta_omega = (do1 + 2*do2 + 2*do3 + do4)/6
        self.omega_old = self.omega_old + delta_omega*dt

        # delta_omega = (do1 + 2*do2 + 2*do3 + do4)/6
        # self.omega = self.omega + delta_omega*dt

        # delta_z = (dz1 + 2*dz2 + 2*dz3 + dz4)/6.
        # self.z = self.z + delta_z*dt

#        dz = self.evolve_memory(self.z, self.psi)*dt
#        self.z = self.z + dz

        return

    def evolve_psi(self, time, psi, omega, old=False):

        ham = self.device.get_h(time)
#        dim_h = self.device.dim_h
        xic = np.array(self.device.xi)

        t1 = np.einsum('ij,jkl', ham, psi, optimize='greedy')

#        t2 = -np.einsum('ij,jkm,ml', np.identity(dim_h),
#                        psi, np.diag(self.egrid),
#                        optimize='greedy')
        if old:
            t3 = np.einsum('ib,kbl', xic,
                           omega[0, :, :, :] +
                           omega[2, :, :, :],
                           optimize='greedy')
        else:
            t3 = np.einsum('ib,kbl', xic, omega,
                           optimize='greedy')

        t4 = np.zeros(self.psi.shape, dtype=complex)
        for i, en in enumerate(self.egrid):
            for ch in range(self.nc_tot):
                econst = np.exp(-1j*en*time/HBAR)
                t4[:, ch, i] = econst*self.device.xi[:, ch]
#        t4 = np.repeat(xic[:, :, np.newaxis],
#                       self.ne_grid, axis=2)

#        psi_rhs = t1 + t2 + t3 + t4
        psi_rhs = t1 + t3 + t4

        psi_rhs *= (-1j/HBAR)
        return psi_rhs

    def evolve_memory(self, zk, psi):

        l = self.fitter.lik
        a = self.fitter.aik
        xi = self.device.xi
        dz = np.zeros((self.m, self.nc_tot, self.nc_tot,
                       self.ne_grid), dtype=complex)
        for i in range(self.nc_tot):
            for j in range(self.nc_tot):

                xi_psi = np.dot(np.conj(xi[:, i]),
                                psi[:, j, :])
                for im in range(self.m):
                    term1 = l[i, im]*zk[im, i, j, :]
                    term2 = a[i, im]*xi_psi
                    dz[im, i, j, :] = term1 + term2
        return dz

    def evolve_omg(self, time, psi, zk, omega):

        t1 = np.array(omega)
        for index, xi_cent in enumerate(self.device.xi_cent):
            t1[:, index, :] *= xi_cent

        t3 = zk.sum(axis=0)

        omg_rhs = t1 + t3
        t4 = np.einsum('sk,sjl', np.conj(self.device.xi),
                       psi)
        t4 *= self.gamma**2
        omg_rhs = omg_rhs + t4
        omg_rhs *= (-1j/HBAR)
        return omg_rhs

    def semi_circle(self, center):
        """Return a semi-circle representing diagonalised
        selfenergy (an energy of a eigen-channel)
        """

        semi = np.zeros(self.ne_grid)
        for index, en in enumerate(self.egrid):
            up_limit = en > (center + 2*self.gamma)
            dw_limit = en < (center - 2*self.gamma)

            if up_limit or dw_limit:
                semi[index] = 0.
            else:
                val = 4.*self.gamma**2 - (en - center)**2
                semi[index] = np.sqrt(val)
        return semi

    def channel_weight(self, time):
        """
        Returns the weight W(E) which multiplies to
        |psi><psi| in energy integration.

        Returns
        -------
        channel_fn: 2D numpy array
            Contains the 2D array weight of the all channels
            as a function of energy. The first index runs on
            channels and the second one on the energy grid.
        """

        # Allocate space for the output channel weight
        ch_fn = np.zeros((self.nc_tot, self.ne_grid))

        for ch in range(self.nc_tot):

            args = self.device.get_args(time)
            mu_leads = self.device.get_mu(time)
            sys = self.device.sys
            # Find the lead and respective channel number
            ld_no, ld_ch_no = lc_find(ch, sys, args)

            # Find the chemical potential of the lead the
            # "ch" channel belongs to
            mu = mu_leads[ld_no]

            # Now populate the output function with it
            x = (self.egrid - mu) / (KB * self.temp_k)
            fermi_term = fermi_fn(x)

            cent = self.device.xi_cent[ch]
            circle_term = self.semi_circle(cent)

            ch_fn[ch, :] = fermi_term * circle_term

        return ch_fn

    def rho(self, time):
        """
        Function to evaluate the nonequilibrium
        density matrix
        """
        psi_conj = np.conj(self.psi)
        ch_fn = self.channel_weight(time)
        rho = np.einsum('icg,jcg,cg,g', self.psi, psi_conj,
                        ch_fn, self.w, optimize='greedy')
        rho = rho * (1./(2*np.pi))
        return rho

    def save_npz(self, filename):
        np.savez(filename, psi=self.psi, omega=self.omega)

    def load_npz(self, filename):
        data = np.load(filename)
        self.omega = data['omega']
        self.psi = data['psi']

    def evolve_omg_old(self, psi, omega):
        """
        Function that returns the RHS of omega EOM.
        """
        t1 = np.array(omega)
        for index, xi_cent in enumerate(self.device.xi_cent):
            t1[:, :, index, :] *= xi_cent

#        t2 = np.array(omega)
#        for index, en in enumerate(self.egrid):
#            t2[:, :, :, index] *= -en

        t3 = 0*omega
        for index in range(len(t3)):
            if index == 0:
                t3[index] = -2*omega[1]
            elif index == (len(t3)-1):
                t3[index] = omega[index-1]
            else:
                t3[index] = omega[index-1] - omega[index+1]

#        omg_rhs = t1 + t2 + 1j*t3
        omg_rhs = t1 + 1j*t3

        t4 = np.einsum('sk,sjl', np.conj(self.device.xi), psi)
        omg_rhs[0, :, :, :] += t4
        omg_rhs *= (-1j/HBAR)
        return omg_rhs
