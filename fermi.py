#!/usr/bin/env python3
import numpy as np
from .constants import KB


def temperature_average(function, fermi_energy=0.0,
                        temp=300.0, elimit=7.0,
                        ne=50, **func_kwargs):
    """Compute the average over a derivative of the
       Fermi-Dirac distribution.

    Parameters
    ----------
    functions : (list of functions)
        Functions to average over. Each function needs to
        have an interface of the form
        function(energy, **func_kwargs)
    fermi_energy : float
    ne : integer
        Number of points in energy sampling.
    elimit : float
        The integration is performed from
        fermi_energy - elimit*KB*T to
        fermi_energy + elimit*KB*T
    Keywords
    --------
    func_kwargs : dictionary
        Optional keyword arguments for the list of functions

    RETURNS
    -------
    averaged values : list of floats
    List of values obtained by averaging the input list of
    functions over a derivative of Fermi-Dirac distribution.
    """

    emin = fermi_energy - elimit*KB*temp
    emax = fermi_energy + elimit*KB*temp

    energies = np.linspace(emin, emax, ne)
    dl_energy = abs(energies[1] - energies[0])

    avgs = 0.0
    for eind, energy in enumerate(energies):
        df = df_de(energy, temp, fermi_energy)
        avgs += np.array(function(energy, **func_kwargs) *
                         df * dl_energy)
    return avgs


def df_de(energy, temp, fermi_energy):
    """ Returns a negative first derivative of the Fermi-Dirac
    distribution
        F_T(E) = 1/(4kB*T) sech^2[(E - Ef)/(2kB*T)]
    """

    # Convert float and int values to numpy array
    if type(energy) is float or type(energy) is int:
        energy = np.array([energy])

    if temp == 0.0:
        raise ValueError("Temperature should not be zero!")

    delta_e = (energy - fermi_energy) / (2*KB*temp)
    norm = 1.0 / (4*KB*temp)
    return norm / (np.cosh(delta_e))**2
