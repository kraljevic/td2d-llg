
Documentation of the core modules and classes
=============================================


In this part we present the documentation of the main
modules and classes used by TD2D


.. autoclass:: td2d.device.Device
   :members:

.. autoclass:: td2d.rk4numpy.RK4Numpy
   :members:

.. autoclass:: td2d.operators.ChargeCurrent
    :members:

.. autoclass:: td2d.operators.SpinDensity
    :members:
