######################
Introduction to TD2D
######################

In this part we give a basic introduction to the TD2D package.
We start by presenting the main classes of the package and
how they interact with each other. Next, we will go into
details of explaining each class separately.

.. toctree::


Main organisation of the TD2D package
=====================================

Classes, dataflow and workflow
------------------------------

The following figure gives us a schematic view of the TD2D
package. Its main classes are ``TBSystem``,
``Manager``, ``Device``, ``Spins``, ``TDNEGF_Solver``,
``EQ_Solver``, and ``Measurement`` class. The number of
classes might look a bit overwhelming, but when combined
together, they form a logical structure. 

.. figure:: ../figs/td2d_scheme.pdf
   :align: center
   :width: 60 %
    
   The main classes of the TD2D package and how they are
   related to each other. Black arrows show how data flows
   between class instances. Red arrows shows which class
   instance requires which in order to get build, while the
   blue arrows show the control flow.
    
The TD2D package is an extension of the KWANT package, and
therefore we don't start by building our system from scratch,
Instead, we start from KWANT tight-binding system, that is an
instance of the ``FiniteSystem`` class in KWANT. In other words,
the ``TBSystem`` class is not an actual class defined in 
TD2D, we use this as a synonym for the KWANT ``FiniteSystem``
class. TD2D currently assumes that the system is attached
to at least on lead, and that all leads are made from the
square lattices. This limitation is imposed on us by the
assumptions of the time-dependent equations
implemented in our ``TDNEGF_Solver`` class. Although this
limitation restricts the systems that can be studied
to only those where electrons are injected
from the square lattice leads, there are no limitations
on the nature of tight-binding Hamiltonian in the main
scattering region.

As we said, we start by building a ``FiniteSystem`` KWANT
object with leads attached to it. The two main classes of the
package are the ``Spins`` class and the ``Device`` class.
Since TD2D allows for self-consistent simulation of both
classical magnetic moments and quantum spin-polarized
electrons in an open system, these two classes serve that
purpose. Note that they are both constructed from the
KWANT's ``FiniteSystem``, that is ``TBSystem`` in our
notation (see red arrows (1) and (2) in the TD2D schematic
figure). By providing the same instance of the KWANT
system to builders of these two classes, we allow for the
same site indexing in both classes. We also use KWANT 
functionality in creating systems with parametric
Hamiltonians, its solvers for the lead selfenergies, its
ability to search for neighboring sites etc.

The Device class is basically an extension of the
``FiniteSystem`` class with few extra attributes and
functions to allow for simpler manipulation of 
time-dependent Hamiltonians.  Since KWANT can create 
parametrized tight-binding systems,
it is important to specify how TD2D implements
time-varying parametric Hamiltonians. Beside a tuple of
constant argument values (how they are
usually supplied in KWANT) TD2D allows for 
argument values which are functions of a single variable
(time), and these functions need to be supplied to the
``Device`` class during its construction (as an argument
list). This also applies
to the time-dependent voltage signals that are to be applied
to the system leads (see the
two green rectangles for :math:`{\rm V(t)}` and :math:`{\rm
Args(t)}` in the TD2D schematics). The purpose of the
device class it intermediary, to facilitate creation of
the time-dependent Hamiltonians, and passing those
Hamiltonians to the ``TDNEGF_Solver`` and ``EQ_Solver``
objects.

The time-evolving internal state of the tight-binding system
can be described either by its wave function, 
or by it's density matrix.   
The ``Device`` class doesn't contain this data, but instead
we implement it in the ``TDNEGF_Solver`` class. This is partly
because there are several ways to implement the solver (both
mathematically, and in the code), and, as we said before,
each method will require different representation of the
system state. Implementing this solver within the ``Device``
class would complicate the ``Device`` interface, and
that is why we decided to implement this solver as an
independent class and not as some internal method of the
``Device`` class. The solver object is attached to the
``Device`` object during its 
construction (see the arrow (3) in the TD2D schematics).
Additionally, the TD2D package (by default) creates an
``EQ_Solver`` object and attaches it to the ``Device`` class.
The purpose of both equilibrium and nonequilibrium solver
is to compute the corresponding density matrix for each
time step when the ``Device`` class supplies the
time-dependent Hamiltonians to them.

Then, there are objects which are instances of the
``Measurement`` class. Basically, they all extend the basic
``Measurement`` class and implement some operation on the
density matrix to extract some measurable quantity, and 
to save it to the output file. There are some default
measurements objects necessary for the consistency of the
entire package (e.g. local electronic spin density operator).
Others can be defined by the user, depending on which
operator he wants to measure.

Since TD2D works with both electrons and classical magnetic
moments, the ``Spins`` class is responsible for the later. It
can be constructed from the ``TBSystem`` (red arrow (2) in
the TD2D schematics) independently from the ``Device`` class,
therefore TD2D can be viewed as an aggregation of two
independent packages. The ``Spins`` class has its own methods
for manipulating and propagating local magnetic moments in
time, that is an Landau-Lifshitz-Gilbert (LLG) solver
implemented in it, together with methods to save the spin
data. 

Although, as we said, the two classes (``Spins`` and
``Device``) can be propagated independently from each other, 
they can also be propagated in a self-consistent loop, where
``Spins`` object modifies the local onsite energy
(:math:`{\Delta U(t)}`) of the
sites where the spins are attached to the electronic system,
and electrons can influence the spin dynamics through their
nonequilibrium (current-driven) spin density :math:`\langle
\mathbf{s}_{\rm neq}\rangle(t) - \langle\mathbf{s}_{\rm
eq}\rangle`.

A user who is familiar with both ``Spins`` and ``Device``
class can implement this self consistent loop (together
with measurements) by himself, or he can use a ``Manager``
object which will do this for him, provided that he defined
the instances of either ``Spins`` and ``Device`` class, 
and some ``Measurement`` objects. All three of
these (red arrows (4), (5), and (6) in TD2D schematics) 
are used to construct an instance of the ``Manager`` class.
This class then controls the entire workflow of time
evolution by triggering an update of the provided 
time-dependent arguments for ``Spins`` and ``Device`` 
classes (see the blue arrows (7) and (8) in TD2D schematics),
by forcing them to recompute 
their internal states (and states of the solvers attached to
them). In the end, the ``Manager`` class invokes the list
of ``Measurement`` objects and saves the given output. You
can thing of the ``Manager`` class as an abstraction of a
lab technician who set the time of the clock, measures the
duration of the experiment and presses buttons on the
measuring devices (instances of the ``Measurement`` class)
to record the history of an experiment.

Since we already explained that TD2D consists, of two more
or less independent parts, related to the ``Spins`` and
``Device`` class, in the next two parts we describe how
to time-evolve these two subsystems independently. After
that, we show how to integrate them into a single
self-consistent loop, or through the ``Manager`` class.



How to time-evolve the quantum electronic system
================================================

The main classes related to the time evolution of the
electronic subsystem are ``Device`` and ``TDNEGF_Solver``.
In this part we explain how one create the two, and use them
to obtain the time-dependent nonequilibrium density matrix.
On a parametrized Hamiltonian.


Performing the nonequilibrium measurements
==========================================




