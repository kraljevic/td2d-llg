
######################
Theoretical Background
######################

In this part we give theoretical explanations behind TD2D
time-dependent propagation algorithm. Namely, we explain how 
TD2D computes the non-equilibrium density matrix.

.. toctree::
   

Lead currents
=============

Equation :eq:`dmt` gives us access to density matrix of the system
however we do not get a direct access to the currents that are being
injected into the leads. To obtain those, we take a time derivative
of this equation to get and equation of motion for the density
matrix which will tell us what exits the system.

.. math::
   :nowrap:
   
   \begin{eqnarray}
   i \hbar 
   \frac{\partial \boldsymbol{\rho}(t)}{\partial t} 
   & = & 
   \sum_{a=1}^{N_c^\mathrm{tot}} 
   \int_{-\infty}^{\infty} \frac{dE}{2\pi} 
   \bigg\{
       i\hbar\frac{\partial |{\Psi_{a}(t,E)}\rangle}
                  {\partial t}
        \langle{\Psi_{a}(t,E)}| 
     + i\hbar |{\Psi_{a}(t,E)}\rangle
      \frac{\langle{\Psi_{a}(t,E)}|}{\partial t}\
      bigg\}W_a(E) \nonumber \\
   & = & 
   \bigg[\mathbf{H}(t)-\mathbf{I}E\bigg]
    {\boldsymbol \rho}(t) + 
    \sum_{a=1}^{N_c^\mathrm{tot}} 
    \sum_{b=1}^{N_c^\mathrm{tot}} 
    \int_{-\infty}^{\infty}\frac{dE}{2\pi} 
    \vec{\mathcal{P}}\cdot 
    \vec{\mathbf{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)
    |{\xi_{b}}\rangle \langle{\Psi_{a}(t,E)}|
    W_a(E) \nonumber \\
    &  & 
    + \sum_{a=1}^{N_c^\mathrm{tot}} 
    \int_{-\infty}^{\infty}\frac{dE}{2\pi}  
    |{\xi_{a}}\rangle \langle{\Psi_{a}(t,E)}|W_a(E)\phantom{|} 
    \nonumber \\
    &  &
    -\boldsymbol{\rho}(t)[\bold{H}(t)-\mathbf{I}E] 
    - \sum_{a=1}^{N_c^\mathrm{tot}} 
     \sum_{b=1}^{N_c^\mathrm{tot}}
     \int_{-\infty}^{\infty}\frac{dE}{2\pi} 
     [\vec{\mathcal{P}}\cdot 
            \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)]^* 
            |{\Psi_{a}(t,E)}\rangle\langle{\xi_{b}}| W_a(E)
   \nonumber \\
     &  &
     - \sum_{a=1}^{N_c^\mathrm{tot}} 
     \int_{-\infty}^{\infty}\frac{dE}{2\pi}  
     |{\Psi_{a}(t,E)}\rangle \langle{\xi_{a}}| W_a(E)
   \nonumber \\
    & = &
    [\bold{H}(t),\bold{\rho}(t)] +
    i \sum_{a=1}^{N_c^\mathrm{tot}} 
    \sum_{b=1}^{N_c^\mathrm{tot}}
    \bigg\{ \bold{\Pi}_{ab}(t) + \bold{\Pi}_{ab}^\dagger(t)\bigg\},
    \nonumber
   \end{eqnarray}

where we have

.. math::
   :nowrap:

   \begin{eqnarray}
   \bold{\Pi}_{ab}(t) 
   & = &
   \frac{1}{i} \int_{-\infty}^{\infty}\frac{dE}{2\pi} 
   \vec{\mathcal{P}}\cdot 
   \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)
   |{\xi_{b}}\rangle \langle{\Psi_{a}(t,E)}| W_a(E) \nonumber \\ 
   &  &
   + \frac{1}{i} 
   \int_{-\infty}^{\infty}\frac{dE}{2\pi}  
   |{\xi_{a}}\rangle\langle{\Psi_{a}(t,E)}| W_a(E) \nonumber 
   \end{eqnarray}
    
These matrices will help us in calculating the currents injected
into the lead but for that we will need a function which takes in
the index :math:`a` and returns the corresponding  
:math:`\alpha` and :math:`C` which we denote as 
:math:`\alpha(a)` and :math:`C(a)`. 
Therefore we have charge current in lead :math:`\alpha` as

.. math::

   \boxed{I_{\alpha}(t) 
    = 
   \sum_{a=1}^{N_c^\mathrm{tot}} 
   \sum_{b=1}^{N_c^\mathrm{tot}}
   \textrm{Tr} \left[{\boldsymbol \Pi}_{ab}(t)\right]
   \delta_{\alpha,\alpha(a)}}

and the spin current is computed as

.. math::
   \boxed{I_{\alpha}^{S_\beta}(t) 
   = 
   \sum_{a=1}^{N_c^\mathrm{tot}} 
   \sum_{b=1}^{N_c^\mathrm{tot}}  
   \textrm{Tr} [\boldsymbol{\sigma}_\beta \bold{\Pi}_{ab}(t)]
   \delta_{\alpha,\alpha(a)}}

This provides as a recipe for calculating all the spin and charge
currents that will be injected into the leads.



Equations of motion of GF and WF 
=================================

The equation of motion of the retarded GF is given as the following

.. math::
   i \hbar 
   \frac{\partial \bold{G}^r(t,t')}{\partial t} 
   = 
   \bold{H}(t)\bold{G}^r(t,t') +
   \int_{-\infty}^{+\infty} dt''\bold{\Sigma}^r(t-t'')
   \bold{G}^r(t'',t')  + \delta(t-t').

We now consider the Fourier expansion in the variable :math:`t'`
and multiply the equation with :math:`|{\xi_{\alpha C}(E)}\rangle`
from the right side to obtain


.. math::
   i \hbar
   \frac{\partial |{\Psi_{\alpha C}(t,E)}\rangle}{\partial t}
   = \bold{H}(t)|{\Psi_{\alpha C}(t,E)}\rangle +
   \int_{-\infty}^{+\infty} 
   \mathbf{\Sigma}^r(t-t'')|{\Psi_{\alpha C}(t'',E)}\rangle dt'' 
   + e^{-iEt/\hbar}|{\xi_{\alpha C}(E)}\rangle.\phantom{|}
   :label: wf_equation

At this stage we employ the following property of retarded
self-energy.

.. math::
   \mathbf{\Sigma}^r(t,t'') = 
   \theta(t-t'')\{ \mathbf{\Sigma}^>(t,t'') - 
                   \mathbf{\Sigma}^<(t,t'')\},

Inserting this into the above equation we obtain

.. math::
   i\hbar
   \frac{\partial |{\Psi_{\alpha C}(t,E)}\rangle}{\partial t} 
   = 
   \mathbf{H}(t)
   |{\Psi_{\alpha C}(t,E)}\rangle +
   \int_{-\infty}^{+\infty}
   \theta(t-t'')\{ \mathbf{\Sigma}^>(t,t'') 
   - \mathbf{\Sigma}^<t,t'')\}
   |{\Psi_{\alpha C}(t'',E)}\rangle dt''
   + e^{-iEt/\hbar}|{\xi_{\alpha C}(E)}\rangle,\phantom{|}
   :label: wf_equation

which implies

.. math::
   i\hbar
   \frac{\partial |{\Psi_{\alpha C}(t,E)}\rangle}{\partial t}
   = 
   \mathbf{H}(t)
   |{\Psi_{\alpha C}(t,E)}\rangle + 
   \int_{-\infty}^{t}\{ \bold{\Sigma}^>(t,t'') 
   - \mathbf{\Sigma}^<(t,t'')\}
   |{\Psi_{\alpha C}(t'',E)}\rangle dt''
   + e^{-iEt/\hbar}|{\xi_{\alpha C}(E)}\rangle.\phantom{|}
   :label: wf_equation

This presents an equation of motion for the energy-dependent ket
vectors :math:`|{\Psi_{\alpha C}(t,E)}\rangle`. If we can solve for
:math:`|{\Psi_{\alpha C}(t,E)}\rangle` then it is possible to get
access to :math:`\mathbf{G}^<(t,t')` with an integration over
energy. This can be done in the same spirit as TD-KWANT using MPI.
Moving further let us look at the following quantity 

\begin{equation}\label{eq:less_great}

.. math::
   :nowrap:

   \begin{eqnarray}
   \mathbf{\Sigma}^>(t,t'') - 
   \mathbf{\Sigma}^<(t,t'') 
   & = &
   \sum_{\alpha=L}^R\ 
   \int_{-\infty}^{\infty}\frac{dE}{2\pi\hbar}
   [\mathbf{\Sigma}^>_\alpha(E) - 
    \mathbf{\Sigma}^<_\alpha(E)] e^{-iE(t-t'')/\hbar} \nonumber \\
   & = &
   \sum_{\alpha=L}^R \ 
   \int_{-\infty}^{\infty}\frac{dE}{2\pi\hbar}
   [-i(1-f_{\alpha}(E))\mathbf{\Gamma}_\alpha(E) 
   -i f_{\alpha}(E)
   \mathbf{\Gamma}_\alpha(E)]e^{-iE(t-t'')/\hbar} \nonumber \\
   & = & 
   (-i)\sum_{\alpha=L}^{R}  
   \int_{-\infty}^\infty \frac{dE}{2\pi\hbar} 
   \mathbf{\Gamma}_\alpha(E)e^{-iE(t-t'')/\hbar} \nonumber \\
   & = & 
   (-i)\sum_{\alpha=L}^{R}\mathbf{\Gamma}_\alpha(t,t'').
   \end{eqnarray}

Plugging this result back into Eq. :eq:`wf_equation` we obtain the
following result

.. math::
   i \hbar 
   \frac{\partial |{\Psi_{\alpha C}(t,E)}\rangle}{\partial t} 
   =
   \mathbf{H}(t)
   |{\Psi_{\alpha C}(t,E)}\rangle
   -i \sum_{\alpha=L}^{R} \int_{-\infty}^{t}
   \mathbf{\Gamma}(t,t'')
   |{\Psi_{\alpha C}(t'',E)}\rangle dt'' 
   + e^{-iEt/\hbar}|{\xi_{\alpha C}(E)}\rangle.\phantom{|}
   :label: wf_gam



Case of the square lattice leads
============================

In the case of square lattice leads we know that the spectral
decomposition of :math:`\bold{\Gamma}(E)` leads to eigen-vectors
that are independent of energy. It is also known that the diagonal
elements are semi-circles. This leads to an analytic solution for
:math:`\bold{\Gamma}(t,t'')`. Let us demonstrate that first

.. math::
   \bold{\Gamma}_{\alpha}(E) = 
   \sum_{C=1}^{N_c^\alpha}g_{\alpha C}(E) 
   |{\xi_{\alpha C}}\rangle\langle{\xi_{\alpha C}}|
   \hspace{1cm} \mathrm{where} \hspace{1cm} 
   g_{\alpha C}(E) = \sqrt{4\gamma^2 - (E-\epsilon_{\alpha C})^2}

Then we have

.. math::
   \bold{\Gamma}(t,t'') =
   \sum_{\alpha=L}^{R} 
   \sum_{C=1}^{N_c^\alpha} 
   \int_{-\infty}^{\infty} \frac{dE}{2\pi\hbar} 
   g_{\alpha C}(E) e^{-iE(t-t'')/\hbar}
   |{\xi_{\alpha C}}\rangle\langle{\xi_{\alpha C}}| 
   = \sum_{\alpha=L}^{R} \sum_{C=1}^{N_c^\alpha} 
   I_{\alpha C}(t-t'') 
   |{\xi_{\alpha C}}\rangle\langle{\xi_{\alpha C}}|

So the goal is to be able to evaluate 
:math:`I_{\alpha C}(t-t'')`. Rest of the section is dedicated to
precisely this. We first substitute 
:math:`E = \epsilon_{\alpha C} + 
2\gamma \cos \theta$ and $T = t-t''`

.. math::
   :nowrap:

   \begin{eqnarray}
   I_{\alpha C}(T) & = &
   \frac{2\gamma^2}{\pi\hbar}  
   e^{-i\epsilon_{\alpha C}T/\hbar}
   \int_{0}^\pi d\theta \sin^2 \theta 
   e^{-i2T\gamma \cos\theta/\hbar}
   \nonumber \\
   & = &
   \frac{2\gamma^2}{\hbar}
   e^{-i\epsilon_{\alpha C}T/\hbar}
   \bigg[
     J_{0}(x)\int_{0}^\pi d\theta \sin^2\theta 
     + \sum_{m=1}^{\infty} (-i)^mJ_{m}(x) 
         \int_{0}^\pi d\theta
           \sin^2\theta e^{im\theta} \nonumber \\
    & &
     \phantom{\frac{2\gamma^2}{\hbar}
              e^{-i\epsilon_{\alpha C}T/\hbar}\bigg.}
         \bigg.+ (-i)^{-m}J_{-m}(x)
         \int_{0}^\pi d\theta \sin^2\theta 
     e^{-im\theta}\bigg] \nonumber \\
    & = & 
    \frac{2\gamma^2}{\hbar}
    e^{-i\epsilon_{\alpha C}T/\hbar}
    \bigg[ J_{0}(2T\gamma/\hbar)\int_{0}^\pi d\theta \sin^2\theta 
          + 2\sum_{m=1}^{\infty}(-i)^mJ_{m}(x)\int_{0}^\pi d\theta
             \sin^2\theta \cos(m\theta) \bigg]
   \end{eqnarray}

But we have

.. math::
   \int_{0}^\pi d\theta \sin^2\theta \cos(m\theta) 
   = -\frac{\pi}{4}\delta_{m2}

Therefore we have

.. math::
   \boxed{I_{\alpha C}(T) =
   \frac{\gamma^2}{\hbar}
   \bigg[ J_{0}(2T\gamma/\hbar) + 
          J_2(2T\gamma/\hbar)\bigg]
   e^{-i\epsilon_{\alpha C}T/\hbar}}

Therefore we get a simple analytical result that

.. math::
   \bold{\Gamma}(t,t'') =
   \frac{\gamma^2}{\hbar}
   \bigg[ J_{0}(2T\gamma/\hbar) + 
         J_2(2T\gamma/\hbar)
   \bigg]
   \sum_{\alpha=L}^{R}\sum_{C=1}^{N_c^\alpha} 
   e^{-i\epsilon_{\alpha C}(t-t'')/\hbar}
   |{\xi_{\alpha C}}\rangle\langle{\xi_{\alpha C}}|

Plugging this in Eq. :eq:`wf_equation` we obtain

.. math::
   i\hbar
   \frac{\partial |{\Psi_{\alpha C}(t,E)}\rangle} {\partial t} =
   \bold{H}(t) |{\Psi_{\alpha C}(t,E)}\rangle + 
   \sum_{\alpha_1=L}^{R}\sum_{C_1=1}^{N_c^{\alpha_1}} 
   \sum_{m=0,2} \Omega_{\alpha_1 C_1}^{m,\alpha C}(t,E)
   |{\xi_{\alpha_1 C_1}}\rangle
   + e^{-iEt/\hbar} |{\xi_{\alpha C}}\rangle

where we have defined an auxiliary function 

.. math::
   \Omega_{\alpha_1 C_1}^{m,\alpha C}(t,E) =
   \frac{\gamma^2}{i \hbar}
   \int_{-\infty}^{t} 
   J_{m}(2[t-t''] \gamma/\hbar) 
   e^{-i \epsilon_{\alpha_1 C_1} (t-t'')/\hbar}
   \langle{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t'',E)}dt''

Let us find the equation of motion of this function

.. math::
   :nowrap:

   \begin{eqnarray}
   \frac{\partial \Omega^{m, \alpha C}_{\alpha_1 C_1}(t,E)}
         {\partial t} & = &
   \frac{\gamma^2J_{m}(0)}{i\hbar} 
   \langle{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t,E)} 
   -i\frac{[\epsilon_{\alpha_1 C_1}]}{\hbar}
   \Omega_{\alpha_1 C_1}^{m,\alpha C}(t,E) \nonumber \\
   &  & 
   + \int_{-\infty}^{t} \frac{\gamma^2}{i\hbar} 
   J_{m}'(2[t-t'']\gamma/\hbar ) 
   e^{-i\epsilon_{\alpha_1 C_1}(t-t'')/\hbar}
   \langle{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t'',E)}dt''
   \end{eqnarray}

Now we use the following property of Bessel function derivative

.. math::
   J'_m(x) = \frac{dJ_{m}(x)}{dx} = \frac{1}{2} 
    \bigg[ J_{m-1}(x) - J_{m+1}(x)\bigg]

This gives us the following result

.. math::
   \frac{\partial \Omega^{m,\alpha C}_{\alpha_1 C_1}(t,E)}
   {\partial t} = 
   \frac{\gamma^2J_{m}(0)}{i\hbar} 
   \langle{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t,E)}
   -i\frac{[\epsilon_{\alpha_1 C_1}]}{\hbar}
   \Omega_{\alpha_1 C_1}^{m,\alpha C}(t,E) 
   + \frac{\gamma}{\hbar} 
   \bigg[\Omega^{m-1,\alpha C}_{\alpha_1 C_1}(t,E) 
       - \Omega^{m+1,\alpha C}_{\alpha_1 C_1}(t,E) \bigg]

Further we note that :math:`J_{m}(0) = \delta_{m0}` which 
simplifies the problem. In practice we will have to truncate the
number of omega scalars to some value of 
:math:`m = m_\mathrm{max}`. Then we will be able to obtain a set
of closed equations of motion. We can now summarize the equations
of motion as

.. math:: 
   i\hbar
   \frac{\partial |{\Psi_{\alpha C}(t,E)}\rangle}{\partial t} 
   = 
   \bold{H}(t)|{\Psi_{\alpha C}(t,E)}\rangle +
   \sum_{\alpha_1=L}^{R}\sum_{C_1=1}^{N_c^{\alpha_1}}
   \vec{\mathcal{P}}\cdot 
   \vec{\bold{\Omega}}_{\alpha_1 C_1}^{\alpha C}(t,E) 
   |{\xi_{\alpha_1 C_1}}\rangle 
   + e^{-iEt/\hbar}|{\xi_{\alpha C}}\rangle \phantom{|}
   :label: eq:rk_1

where we have truncated the sum up to 
:math:`m = m_\mathrm{max}` and introduced the vector 

.. math::
   \vec{\mathcal{P}} = \vec{\bold{e}}_0 + \vec{\bold{e}}_2.

We have also defined a vector using omega scalars given by

.. math::
   \vec{\bold{\Omega}}^{\alpha C}_{\alpha_1 C_1}(t,E) 
   = \sum_{m=0}^{m_\mathrm{max}} 
     \Omega^{m,\alpha C}_{\alpha_1 C_1}(t,E) \bold{e}_m

which gives us 

.. math::
   i\hbar
   \frac{\partial \vec{\bold{\Omega}}^{\alpha C}_{\alpha_1 C_1}(t,E)}
        {\partial t} 
    = 
   \gamma^2\langle{\xi_{\alpha_1 C_1}|\Psi_{\alpha C}(t,E)}\rangle
   \vec{\bold{e}}_0  + [\epsilon_{\alpha_1 C_1}]
   \vec{\bold{\Omega}}_{\alpha_1 C_1}^{\alpha C}(t,E) + 
   i\gamma \bold{D}
   \vec{\bold{\Omega}}^{\alpha C}_{\alpha_1 C_1}(t,E)
   :label: rk_2

where :math:`\bold{D}` is a tridiagonal sparse matrix given by

.. math::
   \bold{D} = \begin{pmatrix} 
   0 & -2 & 0 & 0 & 0 \\
   1 & 0 & -1 & 0 & 0 \\
   0 & 1 & 0 & -1 & 0 \\
   0 & 0 & 1 & 0 & -1 \\
   0 & 0 & 0 & 1 & 0  \\
   \end{pmatrix},

which is shown for the case of :math:`m_\mathrm{max} = 4`. 


Simplifying notation
--------------------

A further simplification of these equations can be achieved by 
lumping the indices :math:`\alpha C` in to just one index which 
we denote by lower case alphabets :math:`a`. This allows us to
write the equations in the following way

.. math::
   i\hbar\frac{\partial |{\Psi_{a}(t,E)}\rangle}{\partial t} 
   = 
   \bold{H}(t)
   |{\Psi_{a}(t,E)}\rangle + 
   \sum_{b=1}^{N_c^\mathrm{tot}}
    \vec{\mathcal{P}}\cdot 
    \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E) 
    |{\xi_{b}}\rangle + e^{-iEt/\hbar}|{\xi_{a}}\rangle,\phantom{|}
   :label: rk_3

where we have the indices running on

.. math::
   N_c^\mathrm{tot} = 
   \sum_{\alpha=L}^{R} N_{c}^\alpha

and the other equation is 

.. math::
   i\hbar\frac{\partial 
   \vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t,E)}{\partial t} 
   = 
   \gamma^2 \langle{\xi_{b}|\Psi_{a}(t,E)}\rangle
   \vec{\bold{e}}_0 
   + \mathcal{L}_b\vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E) + 
   i\gamma \bold{D}\vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t,E).
   :label: rk_4

Note that we have not yet given a definition for 
:math:`\mathcal{L}_b(t)`. We will give this shortly. First we have
to define a mapping between the indices :math:`\alpha,C` and
:math:`a`. Which is done in the following way

.. math::
   a(\alpha,C) = \sum_{\beta=1}^{\alpha -1} N_c^\beta + C 

Now that the mapping is well defined we have 

.. math::
   \mathcal{L}_{a(\alpha,C)} = \epsilon_{\alpha C},

One bad thing about Eq.eqref{eq:rk_3} is that it contains an oscillatory function and we can get rid of it by redefining 

.. math::
   |{\Psi_a(t,E)}\rangle \to |{\Psi_a(t,E)}\rangle
   e^{-iEt/\hbar} \hspace{0.2cm}\phantom{|}
   \mathrm{and} \hspace{0.2cm} 
   \vec{\bold{\Omega}}^a_{\hspace{0.1cm}b}(t,E) \to  
   \vec{\bold{\Omega}}^a_{\hspace{0.1cm}b}(t,E)e^{-iEt/\hbar}

which transforms the Equations to 

.. math::
   \boxed{i\hbar
   \frac{\partial |{\Psi_{a}(t,E)}\rangle}{\partial t} 
   = 
   \bigg\{\mathbf{H}(t)- \mathbf{I}E\bigg\}
   |{\Psi_{a}(t,E)}\rangle +
   \mathcal{J}\sum_{b=1}^{N_c^\mathrm{tot}}
   \vec{\mathcal{P}}\cdot 
   \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E) 
   |{\xi_{b}}\rangle + \mathcal{K}|{\xi_{a}}\rangle,
   }\phantom{|}
   :label: rk_5

and

.. math::
   \boxed{
   i \hbar 
   \frac{\partial \vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t,E)}
   {\partial t} 
   = 
   \gamma^2\mathcal{J} \langle{\xi_{b}|\Psi_{a}(t,E)}\rangle
   \vec{\bold{e}}_0  + 
   (\mathcal{L}_b-E)
   \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E) + 
   i\gamma \bold{D}\vec{\bold{\Omega}}^{a}_{\hspace{0.1cm}b}(t,E).
 }
 :label: rk_6

where we have introduced :math:`\mathcal{J}` in order to couple or
decouple these equations. i.e., when :math:`\mathcal{J} = 0` then
the two equations will decouple. :math:`\mathcal{K}` is introduced
to do further testing of equations of motion. Under similar notation
the Density matrix is also simplified to the following

.. math::
   \boxed{\boldsymbol{\rho}(t) 
   = 
   \sum_{a=1}^{N_c^\mathrm{tot}} 
   \int_{-\infty}^{+\infty} \frac{dE}{2\pi} 
   |{\Psi_a(t,E)}\rangle \langle{\Psi_a(t,E)}|W_a(E)}
   \phantom{|}
  :label: dmt_fi

where we have

.. math::
   W_{a(\alpha,C)}(E) = f_{\alpha}(E)g_{\alpha C}(E)

This completes the problem of square lattice leads. Let us count
the number of equations in this approach. Let us say we have the
following parameters

- :math:`N_x = 100` : Number of sites in the :math:`x`-direction.
- :math:`N_y = 100` : Number of sites in the :math:`y`-direction.
- :math:`N_\alpha = 21` : Number of leads.
- :math:`m_\mathrm{max} = 20` : Number of modes expanded (This is
  way too much, it should be good with a value of :math:`~10`). 

This gives us the total number of equations to be

.. math::
   N_\mathrm{eq} = 
   2_s(N_\alpha N_c^\alpha N_x N_y + (2m_\mathrm{max} + 1)
      (N_\alpha N_c^\alpha)^2)

For the specific example chosen we have  
:math:`N_\mathrm{eq} = 7.28 \times 10^6` equations to solve. 


Lead currents
=============

Equation :eq:`dmt` gives us access to density matrix of the system
however we do not get a direct access to the currents that are being
injected into the leads. To obtain those, we take a time derivative
of this equation to get and equation of motion for the density
matrix which will tell us what exits the system.

.. math::
   :nowrap:
   
   \begin{eqnarray}
   i \hbar 
   \frac{\partial \boldsymbol{\rho}(t)}{\partial t} 
   & = & 
   \sum_{a=1}^{N_c^\mathrm{tot}} 
   \int_{-\infty}^{\infty} \frac{dE}{2\pi} 
   \bigg\{
       i\hbar\frac{\partial |{\Psi_{a}(t,E)}\rangle}
                  {\partial t}
        \langle{\Psi_{a}(t,E)}| 
     + i\hbar |{\Psi_{a}(t,E)}\rangle
      \frac{\langle{\Psi_{a}(t,E)}|}{\partial t}\
      bigg\}W_a(E) \nonumber \\
   & = & 
   \bigg[\mathbf{H}(t)-\mathbf{I}E\bigg]
    {\boldsymbol \rho}(t) + 
    \sum_{a=1}^{N_c^\mathrm{tot}} 
    \sum_{b=1}^{N_c^\mathrm{tot}} 
    \int_{-\infty}^{\infty}\frac{dE}{2\pi} 
    \vec{\mathcal{P}}\cdot 
    \vec{\mathbf{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)
    |{\xi_{b}}\rangle \langle{\Psi_{a}(t,E)}|
    W_a(E) \nonumber \\
    &  & 
    + \sum_{a=1}^{N_c^\mathrm{tot}} 
    \int_{-\infty}^{\infty}\frac{dE}{2\pi}  
    |{\xi_{a}}\rangle \langle{\Psi_{a}(t,E)}|W_a(E)\phantom{|} 
    \nonumber \\
    &  &
    -\boldsymbol{\rho}(t)[\bold{H}(t)-\mathbf{I}E] 
    - \sum_{a=1}^{N_c^\mathrm{tot}} 
     \sum_{b=1}^{N_c^\mathrm{tot}}
     \int_{-\infty}^{\infty}\frac{dE}{2\pi} 
     [\vec{\mathcal{P}}\cdot 
            \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)]^* 
            |{\Psi_{a}(t,E)}\rangle\langle{\xi_{b}}| W_a(E)
   \nonumber \\
     &  &
     - \sum_{a=1}^{N_c^\mathrm{tot}} 
     \int_{-\infty}^{\infty}\frac{dE}{2\pi}  
     |{\Psi_{a}(t,E)}\rangle \langle{\xi_{a}}| W_a(E)
   \nonumber \\
    & = &
    [\bold{H}(t),\bold{\rho}(t)] +
    i \sum_{a=1}^{N_c^\mathrm{tot}} 
    \sum_{b=1}^{N_c^\mathrm{tot}}
    \bigg\{ \bold{\Pi}_{ab}(t) + \bold{\Pi}_{ab}^\dagger(t)\bigg\},
    \nonumber
   \end{eqnarray}

where we have

.. math::
   :nowrap:

   \begin{eqnarray}
   \bold{\Pi}_{ab}(t) 
   & = &
   \frac{1}{i} \int_{-\infty}^{\infty}\frac{dE}{2\pi} 
   \vec{\mathcal{P}}\cdot 
   \vec{\bold{\Omega}}_{\hspace{0.1cm}b}^{a}(t,E)
   |{\xi_{b}}\rangle \langle{\Psi_{a}(t,E)}| W_a(E) \nonumber \\ 
   &  &
   + \frac{1}{i} 
   \int_{-\infty}^{\infty}\frac{dE}{2\pi}  
   |{\xi_{a}}\rangle\langle{\Psi_{a}(t,E)}| W_a(E) \nonumber 
   \end{eqnarray}
    
These matrices will help us in calculating the currents injected
into the lead but for that we will need a function which takes in
the index :math:`a` and returns the corresponding  
:math:`\alpha` and :math:`C` which we denote as 
:math:`\alpha(a)` and :math:`C(a)`. 
Therefore we have charge current in lead :math:`\alpha` as

.. math::

   \boxed{I_{\alpha}(t) 
    = 
   \sum_{a=1}^{N_c^\mathrm{tot}} 
   \sum_{b=1}^{N_c^\mathrm{tot}}
   \textrm{Tr} \left[{\boldsymbol \Pi}_{ab}(t)\right]
   \delta_{\alpha,\alpha(a)}}

and the spin current is computed as

.. math::
   \boxed{I_{\alpha}^{S_\beta}(t) 
   = 
   \sum_{a=1}^{N_c^\mathrm{tot}} 
   \sum_{b=1}^{N_c^\mathrm{tot}}  
   \textrm{Tr} [\boldsymbol{\sigma}_\beta \bold{\Pi}_{ab}(t)]
   \delta_{\alpha,\alpha(a)}}

This provides as a recipe for calculating all the spin and charge
currents that will be injected into the leads.
