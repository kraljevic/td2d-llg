######################
Introduction to TD2D
######################

TD2D is a Python package for simulating time-dependent
quantum transport in open 2D systems, 
with an optional coupling to classical magnetic moments.
TD2D can be viewed as an extension (or
an add-on) to a widely used KWANT package, since it is
mostly based and operates with objects defined in KWANT.

In this part we give a basic introduction to TD2D.
We start by presenting its main classes and
how they interact with each other. In the following
chapters, we will go into details of explaining each class
separately.


Organisation of the TD2D package
=====================================

Main classes and workflow
-------------------------

The following figure gives us a schematic view of the TD2D
package. It consists mostly out of two main classes 
which are the ``Device`` class, and the ``Spins`` class.

.. figure:: ../figs/td2d_scheme.png
   :align: center
   :width: 40 %
    
   The main classes of the TD2D package and how they are
   related to each other. Black arrows show how data flows
   between class instances. Red arrows shows which class
   instance requires which in order to get build.

    
Since TD2D is an extension of the KWANT package, we don't
build our system from scratch. Instead, we start from 
KWANT's tight-binding system, that is an
instance of the ``FiniteSystem`` class in KWANT. This
system is represented as ``TB System`` in our previous
diagram. TD2D currently assumes that the system is attached
to at least one lead, and that all leads are made from 
square lattices. This limitation is imposed on us by the
assumptions of equations implemented in our time dependent
solver. Although this restricts devices that can be
simulated to only those where electrons are injected
from square lattice leads, there are no limitations
on the nature of tight-binding Hamiltonian in the main
scattering region.

As we said, we start by building KWANT's ``FiniteSystem``
object with leads attached to it. 
Since TD2D allows for self-consistent simulation of both
classical magnetic moments and quantum spin-polarized
electrons in an open system, the two main classes 
(the ``Spins`` class and the ``Device`` class).
serve that purpose. Note that they are both constructed from
the KWANT's ``FiniteSystem``, that is ``TB System`` in our
notation (see red arrows (1) and (2) in the TD2D schematic
figure). By providing the same instance of the KWANT
system to builders of these two classes, we allow for the
same site indexing in both classes. We also use KWANT 
functionality in creating systems with parametric
Hamiltonians, its solvers for lead selfenergies, and 
its ability to search for neighboring sites.

The Device class is basically an extension of the
``FiniteSystem`` class with few extra attributes and
functions to allow for simpler manipulation of 
time-dependent Hamiltonians.  Since KWANT can create 
parametrized tight-binding systems,
it is important to specify how TD2D implements
time-varying parametric Hamiltonians. Beside a tuple of
constant argument values (how they are
usually supplied in KWANT) TD2D allows for 
argument values which are functions of a single variable
(time), and these functions need to be supplied to the
``Device`` class during its construction (as an argument
list). This also applies
to the time-dependent voltage signals that are to be applied
to the system leads (see the
two green rectangles for :math:`{\rm V(t)}` and :math:`{\rm
Args(t)}` in the TD2D schematics). The purpose of the
device class it intermediary, to facilitate creation of
the time-dependent Hamiltonians, and to pass those
Hamiltonians to time evolution solvers.

The time-evolving internal state of the tight-binding system
can be described either by its wave function, 
or by it's density matrix.   
The ``Device`` class doesn't contain this data, but instead
we implement it in the ``TD-NEGF Solver`` class. There are
several different implementations of this class, as we'll
see later. This is partly because there are several ways to
implement the time evolution (both mathematically,
and in the code), and, as we said before, each method will
require different representation of the
system state.  The ``Device`` class is necessary to 
initialize the time evolution object 
(see the red arrow (3) in the TD2D schematics).
Additionally, the TD2D package (by default) creates an
equilibrium solver object and attaches it to the ``Device``
class.
The purpose of both equilibrium and nonequilibrium solver
is to compute the corresponding density matrix for each
time step when the ``Device`` class supplies the
time-dependent Hamiltonians to them.

In the last step, there are objects which represent
different measurement operators. There are some default
measurements objects necessary for the consistency of the
entire package (e.g. local electronic spin density operator).
Others can be defined by the user, depending on which
operator he wants to measure.

Since TD2D works with both electrons and classical magnetic
moments, the ``Spins`` class is responsible for time
evolution of the later. It
needs to be constructed using the ``TB System`` 
(red arrow (2) in the TD2D schematics) independently from 
the ``Device`` class. TD2D can be viewed as a collection 
of two independent packages. The ``Spins`` class has its 
own methods for manipulating and propagating local
magnetic moments in time, based on the 
Landau-Lifshitz-Gilbert (LLG) time evolution solver.

Although, as we said, the two classes (``Spins`` and
``Device``) can be propagated independently from each other, 
they can also be propagated in a self-consistent loop, where
``Spins`` object modifies the local onsite energy
(:math:`{\Delta U(t)}`) of the
sites where the spins are attached to the electronic system,
and electrons can influence the spin dynamics through their
nonequilibrium (current-driven) spin density :math:`\langle
\hat{\mathbf{s}}_{\rm neq}(t)\rangle -
\langle\hat{\mathbf{s}}_{\rm
eq}(t)\rangle`.

How to read the rest of this manual
===================================

In the next two chapters we describe how to
create and use the ``Spins`` class objects. In 
chapters 4 and 5 we describe the ``Device`` objects,
separately.
This is for users who want to perform either only 
classical micromagnetics using LLG or only quantum
electron dynamics using time-dependent non-equilibrium
Green function formalism (TD-NEGF). In chapter 6,
we describe how to combine the two in a self-consistent
way (what we refer as TD-NEGF+LLG method). 


