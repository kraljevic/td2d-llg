########################
Solving the LLG equation
########################

TD2D package offers a basic solver for the dynamics of 
local magnetic moments, based on the Landau-Lifshitz-Gilbert
(LLG) equation. The ``Spins`` class provides a way to
specify several terms in this equation responsible for
generation of effective magnetic fields.
Currently, the ``Spins`` class with LLG solver supports

 - Space and time varying magnetic anisotropy.
 - Space and time varying exchange interaction.
 - Space and time varying external magnetic field.
 - Space and time varying demagnetization field.
 - Two types of space and time varying :math:`sd` exchange 
   interactions: one to drive the classical moments in
   LLG equation, and another to create effective onsite
   potential necessary for time-dependent non-equilibrium
   electron dynamics.

In the next few subsections, we show examples on how to
propagate local magnetic moments with nonequilibrium
electron spin density, external magnetic field,
magnetic anisotropy etc. At the end of this chapter, 
we show how to define a custom function for the exchange
coupling.


Propagation with nonequilibrium spin density
============================================

After we created the local magnetic moments 
(see the previous section) we need to define 
nonequilibrium electron spin density that will be used to
propagate these moments. The spin density is a 
:math:`(N, 3)` numpy array (same as local ``s`` attribute 
in the ``Spins`` class). First, without performing any 
electron dynamics, we can create a fake spin density 
(for example along the :math:`x` direction) just to
demonstrate how propagation using ``Spins`` object works
(we assume the ``Spins`` object is the same one as in the
last subsection, namely ``cspins`` object).

.. code-block:: python3
    
   ribbon = make_ribbon(length=5, width=4)
   cspins = Spins(ribbon, jsd_to_llg=0.1, jsd_to_negf=0.1)

   s_cd = np.zeros(cspins.s.shape)

Then we set the :math:`x` component of this `fake` spin
density to some nonzero value 

.. code-block:: python3

   s_cd[:, 0] = 0.1

Again, remember that ``s_cd`` has the same shape as the ``s``
attribute, that is (:math:`N, 3`), so the zeroth component
corresponds to :math:`x` direction. Also, we have set the
two components of the :math:`J_{\rm sd}` coupling constant 
when we defined the ``Spins`` class. One, ``jsd_to_llg``
is the coupling from ``s_cd`` to ``s`` in the
LLG, while another ``jsd_to_negf`` is used when one wants to
compute onsite energies in a KWANT tight-binding system
coming from the local magnetic moments (see below).

Finally, we use the nonequilibrium spin density to 
propagate the local magnetic moments

.. code-block:: python3

   cspins.llg(s_cd, time=0)

This will propagate our local moments for a single time
step (by default time step is set to 0.1 fs, and can be
modified when ``Spins`` class object is created by changing
the ``dt`` keyword argument). If we want to propagate for
more time steps, we create a loop

.. code-block:: python3

   for time in np.arange(0, 10, 0.1):
       print(time)
       cspins.llg(s_cd, time)

A full example on how to propagate and save spins 
(in a ``.txt`` file for example) is given in the 
file ``examples/llg/propagate.py``, also shown here

.. code-block:: python3

   from td2d import Spins
   from td2d.kwant_systems import make_ribbon
   import numpy as np

   # First we create a finalized KWANT tight-binding system
   ribbon = make_ribbon(length=5, width=4)

   # Now we create classical spins
   # and set a uniform Jsd coupling
   cspins = Spins(ribbon, jsd_to_llg=0.1, jsd_to_negf=0.1)

   # We can create a uniform (fake) nonequilibrium spin density.
   # It needs to have the same shape as classical spins
   s_cd = np.zeros(cspins.s.shape)

   # We set it nonzero along the x axis
   s_cd[:, 0] = 0.1

   # Next we propagate the classical spins
   times = np.arange(0, 10, 0.1)

   with open('moments_vs_time.txt', "w") as f:

   for time in times:
       out_str = '%.2f ' % time
       print(out_str)

       cspins.llg(s_cd, time)

       for s in cspins.s:
           out_str += '%0.5e %0.5e %0.5e ' % (s[0], s[1], s[2])

       out_str += '\n'
       f.write(out_str)


Note that with simple ``if`` statement, one can additionally 
save (or plot) local spins on every :math:`n`-th time 
step. Also, the ``cspins.llg()`` function is called 
with ``time`` argument.  This
argument will become important when we want to deal with
properties which are changing in time, as we will show
later. 

Modifying the global anisotropy
===============================

In the next example, we will create a tilted spin
configuration, place it in a uniform anisotropy field along
the :math:`y` direction, set the Gilbert damping to some
custom value, and propagate the local spins. 

.. code-block:: python3

   from td2d import Spins
   from td2d.kwant_systems import make_ribbon
   import numpy as np


   ribbon = make_ribbon(length=5, width=4)
   cspins = Spins(ribbon, spin_config=(1, 1, 1), ani=0.1,
                  ani_vec=(0., 1, 0), g_lambda=0.2)

   scd = np.zeros(cspins.s.shape)
   times = np.arange(0, 100, 0.1)

   for time in times:
       print(time)
       cspins.llg(scd, time)
       cspins.plot(filename='fig_%03.0f.png' % (10*time))

Note that uniform spin configuration is set with
``spin_config=(1, 1, 1)``. Although the provided direction
is not a unit vector, as we said previously, TD2D will
normalize it. Next, the parameter ``ani=0.1`` sets the
uniform value for the anisotropy constant to 0.1, and
lastly the ``ani_vec=(0, 1, 0)`` and ``g_lambda=0.2`` sets
the anisotropy direction along the :math:`y` axis, with
Gilbert
damping set to 0.2.

External magnetic field, demagnetization and exchange coupling
==============================================================

In a similar way as with anisotropy, one can set the direction
and strength of an external magnetic field

.. code-block:: python3

   cspins = Spins(ribbon, bf_vec=(0, 1, 0), bf=2.5)

or Heisenberg exchange interaction

.. code-block:: python3

   cspins = Spins(ribbon, jexc=0.1)

or demagnetization field


.. code-block:: python3

   cspins = Spins(ribbon, demag_vec=(0, 1, 0), demag=0.001)


by default, all these are set to zero. The default directions
for the anisotropy and external magnetic field is along the
:math:`z` axis, while for demagnetization field is along the
:math:`y` axis, therefore if this is the preferred direction
the user needs to set only the corresponding scalar value 
(``ani``, ``bf``, or ``demag``). Also, since all these are
keyword arguments, one can turn on anisotropy and magnetic 
field (or demagnetisation) at the same time. 

The TD2D is using
KWANT lattice object to find neighbors of a moment (neighbors of
a moment corresponds to moments which are connected to neighbors of
a site to which the current moment is connected). In other
words, the nearest neighbor coupling set in a KWANT tight-binding
system will correspond to nearest neghbor coupling between moments
in TD2D, and all couplings are the same. In the next example, we
will show how one can create custom fields and exchange couplings.

Create a space varying fields - Example of an interface
=======================================================

Let us imagine a simple example of two magnetic materials creating an 
interface. They are
both 2D and have equal (square) lattices. There is a specific
Heisenberg exchange coupling between them on the interface, 
and they have different intrinsic exchange coupling 
constants, as well as different values and directions of
anisotropies. In order to simulate these two materials, one would
have to define a ribbon system, and two additional functions
for the exchange coupling and anisotropy which vary in the real
space. In the following example we show how to do that. Lets
assume that the ribbon length is ten sites, and that the first five
sites are created from the first material, and last five sites are
created from the second material. First, we would have to define
an anisotropy function which points in one direction for the
first five rows of atoms, and then points in some other direction
for the last five rows.

.. code-block:: python3

   def custom_anisotropy(time, site):

       ani1 = 0.5  # Anisotropy strength of the first material
       ani2 = 0.1  # Anisotropy strength of the second material

       # Anisotropy directions in the first and second material
       vec1 = np.array([0, 0, 1])  
       vec2 = np.array([0, 1, 0])

       x, y = site.pos

       if x < 5:
          return ani1*vec1
       else:
          return ani2*vec2

Note that this function has a simple interface: the first argument
it takes is a time parameter, while the second is 
a KWANT ``Site`` object from which one can determine the
position of a site for which we want to set the anisotropy. The
function returns a vector which is an effective magnetic field
coming from the anisotropy part (see the section on how we solve the
LLG equation (still missing)). In a similar way, one can redefine
other 'onsite' properties, like external magnetic field or
demagnetization field on each moment. The modifying function would
have the same interface (two arguments, one for time and one for
``Site`` object), and would return effective magnetic field for
these terms (again a 3D vector represented as a numpy array of
three floats).

When it comes to modifying the exchange coupling, the interface of
the redefined function is slightly different. The exchange
interaction depends on two moments, so one needs two site objects,
while the redefined function returns only a scalar value 
(the exchange coupling strength).

.. code-block:: python3

   def custom_exchange(time, site1, site2):

       jexc_1 = 0.1   # Exchange in the first material
       jexc_2 = 0.2   # Exchange in the second material

       jexc_12 = 0.5  # Exchange on the interface

       x1, y1 = site1.pos
       x2, y2 = site2.pos

       if x1 < 5 and x2 < 5:
           return jexc_1
       elif x1 > 4 and x2 > 4:
           return jexc_2
       else:
           return jexc_12

The created fields can be included into ``Spins`` class with

.. code-block:: python3

   cspins = Spins(ribbon, ani_func=custom_anisotropy,
                  jexc_func=custom_exchange)

The final spin configuration is shown in the following figure

.. figure:: ../../source/figs/interface.png
   :align: center
   :width: 40%
   
   Final configuration of the interface system


The full code example for this system is given in
``examples/llg/space_varying_fields.py``. Note that similarly to
anisotropy, one can define space varying demagnetisation field and
pass it ``demag_func``, or space varying magnetic field and pass
it to ``bf_func``. Again, as with anisotropy, the return of these
functions should be an effective magnetic field coming from these
terms. Since the first argument of all these functions
is time, beside space, one can create time-varying fields--- as
we show  in the next example.


Time-varying fields - Example of switching on and off magnetic field
====================================================================

As we already showed in the previous example, the first argument
of the redefined fields is time (time in LLG code is given in units
of femtoseconds) so one can define type-varying fields as in the
present example, where we define a magnetic field function which 
is switched on only between 10 and 20 femtoseconds, otherwise is
zero

.. code-block:: python3

   def custom_bfield(time, site):

       if 10 <= time <= 20:
           return np.array([0, 1000, 0])
       else:
           return np.array([0, 0, 0])

Again, this function is passed when we create classical spins object

.. code-block:: python3

   cspins = Spins(ribbon, spin_config=(1, 0, 0), g_lambda=0.2,
                  bf_func=custom_bfield)

Note that in case of magnetic field, the effective field is equal to
the magnetic field, so in the previous example, the field between
10 and 20 femtosecond is set to 1000 T along the :math:`y` direction.

Changing the coupling between electron spin density and local moments
=====================================================================

TD2D provides two functions to describe coupling between electron
spins and local magnetic moments. One is the coupling from
electron spin density to magnetic moments in the LLG solver 
(set by ``jsd_to_llg`` argument), the other is the coupling
necessary to compute the onsite energy modification coming from
local magnetic moments (``jsd_to_negf``, see next section). 
By default, these two variables set a uniform (in time and space)
coupling on each site.
However, similarly to local field properties, both these functions
can be overwritten by some custom user-defined function which 
varies in time or in space. In this 
section we provide an example on how to do that for ``jsd_to_llg``
variable. We will define a custom function which sets nonzero 
coupling between electron spin density and local moments
only in a narrow ring region. The custom function has a 
similar interface as in the previous examples with anisotropy and
magnetic field, except it returns a scalar value for the
:math:`J_{\rm sd}` coupling.

.. code-block:: python3

   def custom_to_llg(time, site):

       """Couple only spins in a ring with Jsd coupling """
       x, y = site.pos

       in_outer = (x-10)**2 + (y-10)**2 < 9**2
       in_inner = (x-10)**2 + (y-10)**2 < 7**2

       if in_outer and not in_inner:
           return 0.3
       else:
           return 0.0

The default ``jsd_to_llg`` function can be overwritten with

.. code-block:: python3

   cspins = Spins(ribbon, spin_config=(0, 0, 1), 
                  jsd_to_llg_func=custom_to_llg)


The full example code is given in ``examples/llg/custom_jsd.py``.
The final result is shown in the following figure

.. figure:: ../../source/figs/ring.png
   :align: center
   :width: 40%
   
   Ribbon with :math:`J_{\rm sd}` coupling set only in the
   ring region.

Since the coupling was nonzero only in the ring region, only moments
in this region align with the nonequilibrium electron spin density
(set along the :math:`x` direction in this example).

Computing the onsite energies
=============================

Another functionality of the ``Spins`` class is the computation
of the onsite energy modification that each magnetic moment 
creates on a site to which it is attached. This energy is 

.. math::

   U_{i} = J_{\rm sd}(\mathbf{r}_i, t)
   \,{\boldsymbol \sigma}\cdot\mathbf{M}_i(t)

where :math:`J_{\rm sd}(\mathbf{r}_i, t)` is the coupling strength,
:math:`{\boldsymbol \sigma}` is the vector of Pauli matrices
while :math:`\mathbf{M}_i(t)` is the local magnetic moment.

Similarly to ``jsd_to_llg``, as we already explained, there is
a default ``jsd_to_negf`` variable witch sets uniform coupling on
each site. Again, this can be overwritten with some custom function
which is to be passed to ``jsd_to_negf_func`` similarly as in the
previous example with ``jsd_to_llg_func``.

To compute the onsite energies, the ``Spins`` class offers 
the  ``onsite`` function. The function also requires a single argument
which is time (since ``jsd_to_negf_func`` can also be time varying)

.. code-block:: python3

   cspins.onsite(time)

The function returns a :math:`(N, 2, 2)` numpy array of onsite
energies (:math:`2\times2` matrices for :math:`N` sites), which 
can be further used to modify the electron Hamiltonian.


