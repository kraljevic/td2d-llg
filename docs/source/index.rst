TDNEGF+LLG Documentation
========================

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Contents:
   
   chapters/intro
   chapters/llg
   chapters/effective_h


Indices and tables
==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
