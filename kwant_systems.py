import numpy as np
import kwant
import matplotlib
import matplotlib.pyplot as plt
import warnings
from td2d.constants import HBAR
plt

warnings.filterwarnings("ignore")
matplotlib.use('Agg')


def make_circle(radius=10):
    """ Create a simple cirular system on a sqare lattice
    """

    def circle(pos):
        x, y = pos
        return (x**2 + y**2 <= radius**2)

    sys = kwant.Builder()
    lat = kwant.lattice.square()

    sys[lat.shape(circle, (0, 0))] = 0
    sys[lat.neighbors()] = -1.0

    sysf = sys.finalized()
    return sysf


def make_ribbon(length=2, width=1, onsite=0, gamma=1.,
                tsoc=0., tsoc_lead=0., plot_sys=False,
                spin=False, m=(0, 0, 1), jsd=0.0):
    """Create 2D ribbon on a square lattice with two leads

    Parameters
    ----------
    length : int, optional (default is 2)
      Length of the ribbon.
    width : int, optional (default is 1)
      Width of the ribbon.
    onsite : float, optional
        Lattice and lead onsite energy (default is 0).
    gamma : float, optional
        Hopping energy (default is 1).
    tsoc : float, optional (default is 0)
        Rashba spin orbit coupling (SOC) in the sample.
    tsoc_lead : float, optional (default is 0)
        Rashba spin orbit coupling (SOC) in the leads.
    plot_sys : bool, optional
        Plot the KWANT tight-binding system (default is False).
    spin : bool, optional
        Make a spinfull tight-binding system (default is False).
    m : tuple of three floats
        The vector of local magnetic moments in the sample.
    jsd : float, optional (default is 0)
        Coupling between magnetic moment m and local electron
        spin density.

    Returns
    -------
    sysf : kwant.Builder.FiniteSystem
        Finalized system with leads attached of type.
    """

    if spin:
        unit = np.array([[1, 0],
                         [0, 1]])

        sigmax = np.array([[0, 1],
                           [1, 0]])
        sigmay = np.array([[0, -1j],
                           [1j, 0]])
        sigmaz = np.array([[1, 0],
                           [0, -1]])
        mx, my, mz = m
        onsite_jsd = -jsd * (mx*sigmax + my*sigmay + mz*sigmaz)
    else:
        unit = 1
        onsite_jsd = 0

    def onsite_sys(site, v0, p1):
        x, y = site.pos
        return unit * onsite + onsite_jsd

    def onsite_lead(site, v0, p1):
        x, y = site.pos
        return unit * onsite

    def hopping_fx(site1, site2, v0, p1):
        if spin:
            res = -unit*gamma - 1j*tsoc*sigmay
        else:
            res = -unit*gamma
        return res

    def hop_fx_lead(site1, site2, v0, p1):
        if spin:
            res = -unit*gamma - 1j*tsoc_lead*sigmay
        else:
            res = -unit*gamma
        return res

    def hopping_fy(site1, site2, v0, p1):
        if spin:
            res = -unit*gamma + 1j*tsoc*sigmax
        else:
            res = -unit*gamma
        return res

    def hop_fy_lead(site1, site2, v0, p1):
        if spin:
            res = -unit*gamma + 1j*tsoc_lead*sigmax
        else:
            res = -unit*gamma
        return res

    sys = kwant.Builder()
    lat = kwant.lattice.square()

    for i in range(length):
        for j in range(width):
            sys[lat(i, j)] = onsite_sys
            if j > 0:
                sys[lat(i, j), lat(i, j-1)] = hopping_fy
            if i > 0:
                sys[lat(i, j), lat(i-1, j)] = hopping_fx

    sym_left_lead = kwant.TranslationalSymmetry((-1, 0))
    left_lead = kwant.Builder(sym_left_lead)

    for j in range(width):
        left_lead[lat(0, j)] = onsite_lead
        if j > 0:
            left_lead[lat(0, j), lat(0, j-1)] = hop_fy_lead
        left_lead[lat(1, j), lat(0, j)] = hop_fx_lead

    sys.attach_lead(left_lead)
    sys.attach_lead(left_lead.reversed())

    sysf = sys.finalized()
    if plot_sys:
        kwant.plot(sysf, num_lead_cells=3, file='sys_rsoc.pdf')

    return sysf


def rice_mele_light(length=2, width=1, gamma=1, B=1, D=1,
                    plot_sys=False):
    """A function to create 2D ribbon of Rice-Mele (RM) model with
    two NM leads (on the left and on the right) and the central
    region is irradiated with light.

    Parameters
    ----------
    length : integer, optional
        length of chain in x-direction.
    width : integer, optional
        width of chain in y-direction.
        gamma : float (optional)
        hopping in the leads and the central region.
    B : float (optional)
        Staggerd hopping potential of RM model.
    D : float (optional)
        Staggered onsite potential of RM model.
    plot_sys : bool, optional
        Plot the KWANT tight-binding system (default is False).

    Returns
    -------
    sysf : kwant.Builder.FiniteSystem
        Finalized system with leads attached of type.
    """

    def onsite_f(site, t, t0, sigma_l, omega_l, phi_l, zmax):
        x, y = site.pos
        res = 0.5*(-D)**(x+y)
        return res

    def onsite_lead(site, t, t0, sigma_l, omega_l, phi_l, zmax):
        x, y = site.pos
        res = 0
        return res

    def hopping_fx(site1, site2, t, t0, sigma_l,
                   omega_l, phi_l, zmax):
        x, y = site1.pos
        coef = -(gamma - 0.5*(-B)**(x+y))
        expa = 1j*zmax*np.exp(-(t-t0)**2/(2*sigma_l**2))
        expb = np.sin(omega_l*t/HBAR)
        res = coef*np.exp(expa * expb)
        return res

    def hopping_fy(site1, site2, t, t0, sigma_l, omega_l,
                   phi_l, zmax):
        x, y = site1.pos
        coef = -(gamma - 0.5*(-B)**(x+y))
        expa = 1j*zmax*np.exp(-(t-t0)**2/(2*sigma_l**2))
        expb = np.cos(omega_l*t/HBAR)
        res = coef * np.exp(expa * expb)
        return res

    def hopping_lead(site1, site2, t, t0, sigma_l,
                     omega_l, phi_l, zmax):
        return -gamma

    sys = kwant.Builder()
    lat = kwant.lattice.square()
    for i in range(length):
        for j in range(width):
            sys[lat(i, j)] = onsite_f
            if j > 0:
                sys[lat(i, j), lat(i, j-1)] = hopping_fx
            if i > 0:
                sys[lat(i, j), lat(i-1, j)] = hopping_fy

    sym_left_lead = kwant.TranslationalSymmetry((-1, 0))
    left_lead = kwant.Builder(sym_left_lead)

    for j in range(width):
        left_lead[lat(0, j)] = onsite_lead
        if j > 0:
            left_lead[lat(0, j), lat(0, j - 1)] = hopping_lead
        left_lead[lat(1, j), lat(0, j)] = hopping_lead

    sys.attach_lead(left_lead)
    sys.attach_lead(left_lead.reversed())

    sysf = sys.finalized()
    if plot_sys:
        kwant.plot(sysf, num_lead_cells=3, file='sysf.pdf')

    return sysf


def spin_pump(length=3, width=1, gamma=1,
              plot_sys=False):
    """A function to create 1D chain with attached NM
       leads and a single precessing spin placed in the
       center of the system.

    Parameters
    ----------
    length : integer, optional
        length of chain in x-direction.
    width : integer, optional
        width of chain in y-direction.
    gamma : float (optional)
        hopping in the leads and the central region.
    plot_sys : bool, optional
        Plot the KWANT tight-binding system (default is False).

    Returns
    -------
    sysf : kwant.Builder.FiniteSystem
        Finalized system with leads attached of type.
    """
    sigma0 = np.array([[1, 0], [0, 1]])
    sigmax = np.array([[0, 1], [1, 0]])
    sigmay = np.array([[0, -1j], [1j, 0]])
    sigmaz = np.array([[1, 0], [0, -1]])

    def onsite_f(site, t, omega, jsd, theta):
        x, y = site.pos
        # Position dependent spin-pump
        if x == 1:
            res_x = -jsd*sigmaz*np.cos(theta*np.pi/180)
            res_y = -jsd*sigmax*np.sin(theta*np.pi/180)
            res_y = res_y * np.cos(omega*t/HBAR)
            res_z = -jsd*sigmay*np.sin(theta*np.pi/180)
            res_z = res_z * np.sin(omega*t/HBAR)
            res = res_x + res_y + res_z
        else:
            res = 0*sigma0
        return res

    def onsite_lead(site, t, omega, jsd, theta):
        res = sigma0*0
        return res

    def hopping_fx(site1, site2, t, omega, jsd, theta):
        res = -sigma0*gamma
        return res

    def hopping_fy(site1, site2, t, omega, jsd, theta):
        res = -sigma0*gamma
        return res

    def hopping_lead(site1, site2, t, omega, jsd, theta):
        res = -sigma0*gamma
        return res

    sys = kwant.Builder()
    lat = kwant.lattice.square()
    for i in range(length):
        for j in range(width):
            sys[lat(i, j)] = onsite_f
            if j > 0:
                sys[lat(i, j), lat(i, j-1)] = hopping_fx
            if i > 0:
                sys[lat(i, j), lat(i-1, j)] = hopping_fy

    sym_left_lead = kwant.TranslationalSymmetry((-1, 0))
    left_lead = kwant.Builder(sym_left_lead)

    for j in range(width):
        left_lead[lat(0, j)] = onsite_lead
        if j > 0:
            left_lead[lat(0, j), lat(0, j - 1)] = hopping_lead
        left_lead[lat(1, j), lat(0, j)] = hopping_lead

    sys.attach_lead(left_lead)
    sys.attach_lead(left_lead.reversed())

    sysf = sys.finalized()
    if plot_sys:
        kwant.plot(sysf, num_lead_cells=3, file='sysf.pdf')

    return sysf


def make_fm(length, width, gamma=1, plot_sys=False):
    """A function to create 1D chain with attached
       NM leads and a single precessing spin placed
       in the center of the system.

    Parameters
    ----------
    length : integer, optional
        length of chain in x-direction.
    width : integer, optional
        width of chain in y-direction.
    gamma : float (optional)
        hopping in the leads and the central region.
    plot_sys : bool, optional
        Plot the KWANT tight-binding system (default is False).

    Returns
    -------
    sysf : kwant.Builder.FiniteSystem
        Finalized system with leads attached of type.
    """
    sigma0 = np.array([[1, 0], [0, 1]])
    sigmax = np.array([[0, 1], [1, 0]])
    sigmay = np.array([[0, -1j], [1j, 0]])
    sigmaz = np.array([[1, 0], [0, -1]])

    def onsite_f(site, t, jsd, theta, phi):
        x, y = site.pos
        resx = -jsd*sigmaz*np.cos(theta*np.pi/180)
        resy = (-jsd*sigmax*np.sin(theta*np.pi/180) *
                np.cos(phi*np.pi/180))
        resz = (-jsd*sigmay*np.sin(theta*np.pi/180) *
                np.sin(phi*np.pi/180))
        res = resx + resy + resz
        return res

    def onsite_lead(site, t, jsd, theta, phi):
        res = sigma0*0
        return res

    def hopping_fx(site1, site2, t, jsd, theta, phi):
        res = -sigma0*gamma
        return res

    def hopping_fy(site1, site2, t, jsd, theta, phi):
        res = -sigma0*gamma
        return res

    def hopping_lead(site1, site2, t, jsd, theta, phi):
        res = -sigma0*gamma
        return res

    sys = kwant.Builder()
    lat = kwant.lattice.square()
    for i in range(length):
        for j in range(width):
            sys[lat(i, j)] = onsite_f
            if j > 0:
                sys[lat(i, j), lat(i, j-1)] = hopping_fx
            if i > 0:
                sys[lat(i, j), lat(i-1, j)] = hopping_fy

    sym_left_lead = kwant.TranslationalSymmetry((-1, 0))
    left_lead = kwant.Builder(sym_left_lead)

    for j in range(width):
        left_lead[lat(0, j)] = onsite_lead
        if j > 0:
            left_lead[lat(0, j), lat(0, j - 1)] = hopping_lead
        left_lead[lat(1, j), lat(0, j)] = hopping_lead

    sys.attach_lead(left_lead)
    sys.attach_lead(left_lead.reversed())

    sysf = sys.finalized()
    if plot_sys:
        kwant.plot(sysf, num_lead_cells=3, file='sysf.png')

    return sysf
