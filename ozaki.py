#!/usr/bin/env python3

from scipy.sparse import diags
from scipy.sparse.linalg import eigsh
import numpy as np
from .constants import KB


class OzakiIntegrator():

    def __init__(self, num_poles=100, rdist=1.0e+20):
        self.poles, self.res = ozaki_poles(num_poles)

        # rdist is just a very large number.
        self.rdist = rdist
        return

    def rhoo(self, green, energy=0, temp_k=300.0,
             **kwargs):
        beta = 1.0 / (KB * temp_k)
        sum_ozaki = 0.0
        rds = self.rdist

        for z, r in zip(self.poles, self.res):
            pole = energy + 1j * z / beta
            try:
                sum_ozaki += (r * green(pole, **kwargs))
            except:
                print("Failed to converge")
                continue
        sum_ozaki = 2j * sum_ozaki / beta
        rho = (sum_ozaki - np.conjugate(sum_ozaki.T)) / 2j
        moment = 0.5j * rds * green(1j*rds, **kwargs)
        rho = rho + moment
        return rho


def ozaki_poles(n_poles):
    '''Compute the first n poles of the Ozaki continued
    fraction representation of the Fermi-Dirac function
    (Phys.Rev.B 75, 035123 (2007)). The implementation
    presented here is adopted from Karrasch (Phys.Rev.B
    82, 125114 (2010))

    ARGUMENTS
    ---------
        n_poles (int): Number of poles to compute

    RETURNS
    -------
        zeros (np.array(double)): An array of n_poles
            elements with the singular points of the
            Ozaki continued fraction representation
        res (np.array(double)): An array of residues at
            positions of zeros.
    '''

    n = n_poles*2 + 1
    x = np.arange(1, n, 1)
    x = 1.0 / (2*np.sqrt(4*x**2 - 1))
    diag_elements = [x, x]

    b = diags(diag_elements, [-1, 1])
    evals, evects = eigsh(b, k=n-1)

    # Function eigsh returns the eigenvectors in columnwise
    # order, so evects[0] is an array of the first elements
    # and not the first eigenvector.
    res = np.abs(evects[0])**2 / (4*evals**2)

    res = res[np.where(evals > 0)]
    evals = evals[np.where(evals > 0)]

    zeros = 1.0 / evals

    # The strange indexing in this part is used just to
    # reverse these two arrays
    return zeros[::-1], res[::-1]
