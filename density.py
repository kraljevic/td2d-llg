#!/usr/bin/env python3
from .ozaki import OzakiIntegrator as Integrator
from .fermi import temperature_average as tavg
import numpy as np
from functools import reduce
from math import pi

adj = np.matrix.getH


class DensityMatrix:

    def __init__(self, device, temp=300.0, erange=7.0, ne=30,
                 nozaki=200, eta=1.e-12, average=True):

        self.device = device
        self.temp = temp
        self.erange = erange
        self.ne = ne
        self.nozaki = nozaki
        self.eta = eta
        self.int = Integrator(nozaki)
        self.average = average
        return

    def rho_cd(self, energy, time, left_ind=0,
               right_ind=1):
        '''Return the current-driven spin-density.'''

        rho_oo, rho_oe = self.rho_oo_oe(energy, time,
                                        left_ind, right_ind)
        rho_ee = self.rho_ee(energy, time, left_ind,
                             right_ind)
        rho_eq = self.rho_eq(energy, time)
        return rho_oo, rho_oe, rho_ee, rho_eq

    def get_bias(self, time, left_ind=0, right_ind=1):
        pot = self.device.get_mu(time)
        mu_left = pot[left_ind]
        mu_right = pot[right_ind]
        bias = mu_right - mu_left
        return bias

    def kernel_wrapper(self, time, left_ind=0, right_ind=1):
        def wrap_kernel(energy):
            kernel_oe = 0
            kernel_oo = 0

            green = self.device.green
            gamma = self.device.gamma
            bias = self.get_bias(time, left_ind, right_ind)

            g = green(energy, time)
            g_left = gamma(energy, time, left_ind)
            g_right = gamma(energy, time, right_ind)

            a = reduce(np.dot, [g, g_left, adj(g)])
            kernel_oo += a
            kernel_oe += a

            a = reduce(np.dot, [adj(g), g_left, g])
            kernel_oo -= a
            kernel_oe += a

            a = reduce(np.dot, [g, g_right, adj(g)])
            kernel_oo -= a
            kernel_oe -= a

            a = reduce(np.dot, [adj(g), g_right, g])
            kernel_oo += a
            kernel_oe -= a

            v_const = bias / (8 * pi)
            return v_const * np.array([kernel_oo, kernel_oe])
        return wrap_kernel

    def rho_oo_oe(self, energy, time, left_ind=0,
                  right_ind=1):
        wrap_kernel = self.kernel_wrapper(time, left_ind,
                                          right_ind)

        if self.average:
            rho_oo, rho_oe = tavg(wrap_kernel,
                                  fermi_energy=energy,
                                  temp=self.temp,
                                  elimit=self.erange,
                                  ne=self.ne)
        else:
            rho_oo, rho_oe = wrap_kernel(energy)
        return rho_oo, rho_oe

    def rho_ee(self, energy, time, left_ind=0,
               right_ind=1):

        pots = self.device.get_mu(time)
        en_left = pots[left_ind]
        en_right = pots[right_ind]

        green = self.device.green
        temp = self.temp

        rho_left = self.int.rhoo(green, en_left,
                                 temp, time=time)
        rho_right = self.int.rhoo(green, en_right, temp,
                                  time=time)
        return 0.5 * (rho_left + rho_right)

    def rho_eq(self, energy, time):
        green = self.device.green
        rho_eq = self.int.rhoo(green, energy,
                               self.temp, time=time)
        return rho_eq
