import numpy as np
from td2d.constants import KB


def fermi(mu, temp, energy):
    res = 1. / (1 + np.exp((energy - mu) / (KB*temp)))
    return res


class DenisEquilibrium:

    def __init__(self, device, efermi=0., temp=300.,
                 e_min=-3., p=21):
        """ An equilibrium density matrix solver

        Parameters
        ---------
        device : an instance of Device class
            Device for which to compute the density matrix
        efermi : float
            Fermi level energy of the system (in eV).
        temp : float
            Temperature of the system (in Kelvins).
        e_min : float
            The lower bound of the integral of
            Green function in the equation for density
            matrix.
        p : int
            Parameter which determines the precision of
            the density matrix. For higher values of p
            the density matrix is more precise.
        """

        self.device = device
        self.mu = efermi
        self.temp = temp
        self.e_min = e_min
        self.p = p

    def rho(self, time):
        """Compute the equilibrium density matrix

        Paramters
        ---------
        time : float
            Time used to construct the Hamiltonain and
            selfenergies of the leads

        Returns
        -------
        rho : numpy ndarray
            Equilibrium density matrix of the system
        """

        # Find the poles and residues
        poles, residues = self.adjuct_pr()

        p0 = poles[0]
        p1 = poles[1]
        p2 = poles[2]
        r0 = residues[0]
        r1 = residues[1]
        r2 = residues[2]
        poles = np.concatenate((p0, p1, p2))
        residues = np.concatenate((r0, r1, r2))

        # Run loop to find the green function at each
        # pole and then sum up for rho
        rho = 0

        for n, z in enumerate(poles):

            gf = self.ret_gf(z, time)
            Res = residues[n] * 2 * np.pi * 1j

            rho_add = (Res*gf - np.conjugate(Res*gf).T)/(2.j)
            rho = rho + rho_add

        rho = -rho / np.pi

        return rho

    def adjuct_pr(self):
        """Adjust (non-overlapping) poles and residues

        Returns
        -------
        poles : list
            List of all three kind of poles (i.e.
            imaginary, conventional and real).
        residues : list
            Corressponding residues of the poles above.
        """

        temp = self.temp
        e_min = self.e_min
        mu = self.mu
        p = self.p

        # Check Validity
        delta = (self.mu - self.e_min) / (KB*self.temp)
        if delta > 1000:
            err_str = ('Temperature is too low for the '
                       'approach to be valid!')
            raise ValueError(err_str)

        # Step 1: Find optimized but non-adjusted
        # temp_im and temp_re that minimize the total
        # number of poles.
        term1 = 0.5*temp
        term2 = temp + (mu - e_min) / (p*KB)
        temp_im = np.sqrt(term1 * term2)
        temp_re = temp_im

        # Step 2: Find mu_im and mu_re
        mu_im = p * KB * temp_im
        mu_re = e_min - p*KB*temp_re

        # Step 3: Adjust and update the values of
        # temp_im and mu_re
        m_max = np.ceil(0.5 * (mu/(np.pi*KB*temp_im) - 1))
        if m_max == 0:
            temp_im_p = 2*temp_im
        else:
            temp_im_p = mu / (2 * np.pi * KB * m_max)

        # Consequently adjust the value mu_re
        n_max = np.ceil(0.5 * (mu_re/(np.pi*KB*temp_im_p) - 1))
        mu_re_p = 2 * np.pi * KB * temp_im_p * n_max

        temp_im = temp_im_p
        mu_re = mu_re_p

        # Step 4: Use these values to find number of poles
        # No. of poles of imaginary kind
        up = mu - mu_re + p*KB*temp_re + p*KB*temp
        dn = 2*np.pi*KB*temp_im
        N_im = int(up / dn)

        # No. of poles of conventional kind
        up = 2*mu_im
        dn = 2*np.pi*KB*temp
        N = int(up / dn)

        # No. of poles of real kind
        up = 2*mu_im
        dn = 2*np.pi*KB*temp_re
        N_re = int(up / dn)

        # Step 5: Use all these values to evaluate the
        # poles and residues
        pol_im = np.zeros(N_im, dtype=complex)
        res_im = np.zeros(N_im, dtype=complex)
        m_max = np.ceil(0.5 * (mu/(np.pi*KB*temp_im) - 1))

        for a in range(N_im):
            m = m_max - a
            z_re = np.pi * KB * temp_im * (2*m + 1)
            z_im = mu_im
            z = z_re + 1j*z_im
            pol_im[a] = z
            f_sys = fermi(mu, temp, z)
            f_re = fermi(mu_re, temp_re, z)
            res_im[a] = -1j*KB*temp_im*(f_sys - f_re)

        # Poles and residues of the conventional kind
        num = np.arange(0, N)
        pol = mu + 1j*np.pi*KB*temp * (2*num + 1)
        res = -KB * temp * fermi(1j*mu_im, 1j*temp_im, pol)

        # Poles and residues of the real kind
        num = np.arange(0, N_re)
        pol_re = mu_re + 1j*np.pi*KB*temp_re * (2*num + 1)
        f_im = fermi(1j*mu_im, 1j*temp_im, pol_re)
        res_re = KB * temp_re * f_im

        poles = [pol_im, pol, pol_re]
        residues = [res_im, res, res_re]
        return poles, residues

    def ret_gf(self, energy, time, eta_pow=4):
        """Evaluate the retarded Green function"""
        ham = self.device.get_h(time)
        dim_h = ham.shape[0]

        eta = 10**(-eta_pow)
        ef_i2 = (energy + 1j*eta)*np.eye(dim_h)
        heff = ham + self.device.selfenergy(energy, time)
        gf = np.linalg.inv(ef_i2 - heff)
        return gf
