import numpy as np
import contextlib
import os

def random_spins(seed=os.getpid(), dim=3):
    """
    computes random spins of unit length.

    Parameters 
    ----------
    seed : integer (optional)
           seed for random number generator,
           default value is set to process id (PID)
    dim  : integer (optional)
           dimension of the random object
           default value is set to 3D

    Returns 
    -------
    returns an instance of 1D numpy array of length dim

    """

    @contextlib.contextmanager
    def temp_seed(seed):
        # store global random state in a temporary variable 
        # and reset it once function call is done  
        state = np.random.get_state()
        np.random.seed(seed)
        try:
            yield
        finally:
            np.random.set_state(state)

    def get_rand_num(site):
        noise = int(10*np.random.rand(1))
        # print('random no. is', np.round(10*np.random.rand(1)))
        with temp_seed(seed + noise):
            spin = np.random.randn(dim)
            norm = np.linalg.norm(spin)
        return spin/norm  

    np.random.seed(seed)

    return get_rand_num

