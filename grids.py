import numpy as np
from collections import namedtuple
import matplotlib.pyplot as plt
from multiprocessing import Pool
import sys


Point = namedtuple('Point', ['x', 'dx', 'y', 'z',
                             'xmin', 'xmax'])


class Grids:

    def __init__(self, function, pool, select_function=None,
                 dy_cutoff=1.e-3, x0=0., x1=1., n_pts=50,
                 fargs=None, n_sub=5, write=True, nmax=8,
                 out_format='out_%02d.npz'):
        ''' Compute function y = f(x) values on an adaptive
        grid over x based on some criteria

        x0 : float
            Lower boundary of x values of the adaptive grid.
        x1 : float
            Upper boundary of the x values of the adaptive
            grid.
        n_pts : integer
            Number of the initial points in the adaptive
            grid. The initial points are uniformly
            distributed.
        dy_cutoff : float
            Difference between two y = f(x) points above
            witch new grid points along x are inserted.
        select_function : function (optional)
            Use this option if you want to define some other
            criteria for doing adaptive sampling. The default
            criteria is the following: provided with two
            sequences of floats (first representing x_i values
            (i = 1, N), and second representing y_i = f(x_i)
            values) the default selection function checks
            each pair of neighboring points y_i and y_(i+1)
            and if their distance is larger than
            dy_cutoff (that is |y_i - y_(i+1)| > dy_cutoff)
            the default select function marks those points,
            and returns indices (i, and i+1) for
            which the criteria is satisfied. The adaptive 
            solver will insert more x-points
            around those indices. If the user wants to use
            some other criteria to define where the new
            points should be inserted, than he can define
            the select_function by himself and pass it as
            an object when he initializes the adaptive
            solver. The interface of this user-defined
            function should be select_function(x, y)
            where x and y are two numpy arrays of floats and
            the function should return an array of integers
            for point indices around which new points 
            should be added (the returned array of indices
            should be converted into set, to remove indices
            occuring twice).
        nmax : integer
            Maximal number of iterations. In a single
            iteration, the array of x and f(x) values is
            cheked by the select function and points where
            to perform subdivision of the x-grid are
            selected, and new values f(x) of those points are
            computed.
        out_format : format string
            A string that needs to contain %d term to save
            data at each iteration. Since data is saved in
            a npz library, it should contain npz extension at
            the end. For example 'iteration_%d.npz'
        n_sub : integer
            Number of equal-sized intervals into which the
            initial interval should be subdivided. It should
            be an odd number.
        fargs : float, tuple of floats or None
            The input function is called with pool.map function.
            if function interface is (x, arg1, arg2) then
            fixed arg1 and arg2 should be provided as a tuple
            (arg1, arg2)
        function : function
            The function to perform addaptive calculations
            with.
        '''
        self.f = function
        self.x0 = x0
        self.x1 = x1
        self.f_select = select_function
        self.dy_cutoff = dy_cutoff
        self.out_format = out_format
        self.fargs = fargs
        self.n_sub = n_sub
        self.nmax = nmax
        self.write = write
        self.conv_criteria = None
        self.conv_value = None
        self.iteration_index = 0
        self.ntotal = n_pts
        self.pool = pool
        self.grid = self.make_initial_grid(x0, x1, n_pts)

    def write_output(self):

        out_filename = self.out_format % self.iteration_index
        x, y, z, dx = self.get_all()
        np.savez(out_filename, x=x, y=y, z=z, dx=dx, xmin=self.x0,
                 xmax=self.x1)

    def test_conv(self):
        '''Computes the value of an integral int_x0^x1 f(x)*dx
        for all present points
        '''

        new_total = len(self.grid)
        old_total = self.ntotal
        n_diff = new_total - old_total

        self.ntotal = new_total

        sum_y = 0
        for p in self.grid:
            if not np.isnan(p.y):
                sum_y += (p.y * p.dx)
        sum_y /= (self.x1 - self.x0)

        if self.conv_criteria is None:
            self.conv_criteria = 1.
            self.conv_value = sum_y
            self.iteration_index += 1
            print('Iteration: %3d  value: %.4e  percent: %.2f' %
                  (self.iteration_index, self.conv_value,
                   self.conv_criteria*100))
        else:
            self.conv_criteria = sum_y / self.conv_value
            self.conv_value = sum_y
            self.iteration_index += 1
            it_str = ('Iteration: %3d  value: %.4e  ' +
                      'percent: %.2f N/O/D: %d/%d/%d')
            print(it_str % (self.iteration_index,
                            self.conv_value,
                            self.conv_criteria*100-100,
                            new_total, old_total, n_diff))
        sys.stdout.flush()

        return

    def get_all(self):
        x = []
        y = []
        dx = []
        z = []
        for p in self.grid:
            x.append(p.x)
            y.append(p.y)
            z.append(p.z)
            dx.append(p.dx)
        x = np.array(x)
        y = np.array(y)
        z = np.array(z)
        dx = np.array(dx)
        return x, y, z, dx

    def get_xy(self):
        x = []
        y = []
        for p in self.grid:
            x.append(p.x)
            y.append(p.y)
        x = np.array(x)
        y = np.array(y)
        return x, y

    def iterate(self):

        for i in range(self.nmax):
            if i != 0:
                self.update_grid()
                self.sort_grid()

            self.recalculate()
            self.test_conv()
            if self.write:
                self.write_output()
                self.plot(filename='adaptive_%d.png' % i)
        self.write_output()

    def select_default(self, x, y):
        '''Select those points for which the change of
        neighboring y is larger than dy_cutoff.
        '''
        indices = []
        for index, (ya, yb) in enumerate(zip(y[0:-1], y[1:])):
            if abs(ya - yb) > self.dy_cutoff:
                indices.extend([index, index+1])
        return set(indices)

    def update_grid(self):
        x, y = self.get_xy()

        if self.f_select:
            indices = self.f_select(x, y)
        else:
            indices = self.select_default(x, y)

        new_point_list = []
        for index in indices:
            point = self.grid[index]
            new_points = self.subdivide(point, n_sub=self.n_sub)
            new_point_list.extend(new_points)
        indices = sorted(indices)[::-1]
        for index in indices:
            del self.grid[index]
        self.grid.extend(new_point_list)

    def recalculate(self):
        x_new = []
        index_new = []
        for point_index, point in enumerate(self.grid):
            if point.y is None:
                x_new.append(point.x)
                index_new.append(point_index)

        if self.fargs:
            if isinstance(self.fargs, tuple):
                ppoints = [(x, *self.fargs) for x in x_new]
            else:
                ppoints = [(x, self.fargs) for x in x_new]
        else:
            ppoints = x_new

        data = self.pool.map(self.f, ppoints)

        for index, yzm in zip(index_new, data):
            self.grid[index] = self.grid[index]._replace(y=yzm[0])
            self.grid[index] = self.grid[index]._replace(z=yzm[1:])

        return

    def plot(self, filename='adaptive.png'):

        fig = plt.figure(figsize=(3.375, 3.0))
        ax = fig.add_axes([0.17, 0.15, 0.80, 0.80])

        self.sort_grid()
        x, y = self.get_xy()

        ax.plot(x, y, lw=0.0, marker='o', mew=0.0,
                color='C0', ms=1.0**2)

        ax.set_xlim(self.x0, self.x1)
        # ax.set_ylim(-0.1e-9, 5)
        plt.savefig(filename, dpi=400)

    def sort_grid(self):
        self.grid = sorted(self.grid, key=lambda xarg: xarg.x)

    def print_grid(self):
        for p in self.grid:
            print('%.4f %.4f %.4f %.4f' %
                  (p.x, p.dx, p.xmin, p.xmax))

    def subdivide(self, point, n_sub=3):
        if n_sub % 2 != 1:
            err_str = ('You can only subdivide on odd \n'
                       'number of parts.')
            raise ValueError(err_str)
        a = np.linspace(point.xmin, point.xmax, n_sub+1)
        middle = 0.5 * (a[0:-1] + a[1:])
        delta = middle[1] - middle[0]
        points = []
        for x_value in middle:
            points.append(Point(x_value, delta, None, None,
                                x_value - 0.5*delta,
                                x_value + 0.5*delta))
        n_old = int(n_sub/2)
        points[n_old] = points[n_old]._replace(y=point.y)
        points[n_old] = points[n_old]._replace(z=point.z)

        return points

    def make_initial_grid(self, x0, x1, n_pts):
        a = np.linspace(x0, x1, n_pts + 1)
        middle = 0.5 * (a[0:-1] + a[1:])
        delta = middle[1] - middle[0]
        points = []
        for x_value in middle:
            points.append(Point(x_value, delta, None, None,
                                x_value - 0.5*delta,
                                x_value + 0.5*delta))
        return points

