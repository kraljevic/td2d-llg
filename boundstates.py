import numpy as np
from .device import Device
from .grids import Grids
from multiprocessing import Pool
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
from scipy import linalg

class Boundstates:

    def __init__(self, device,
                 bs_low=-3, bs_upr=3, bs_de=0.01,
                 bs_tol=10**(-10), bs_iter_max=1000,
                 bs_adaptive=False,
                 gamma=1):
        """
        Parameters
        ----------
        bs_low: float (optional)
            Lower limit of energy till where the solver
            will look for bound states.
        bs_upr: float (optional)
            Upper limit of energy till where the solver
            will look for bound states.
        bs_de: float (optional)
            Energy step dE which is used to find bound states
            or plot the denisty of states.
        bs_tol: float (optional)
            Tolerance of convergence when finding bound states.
        bs_iter_max: int (optional)
            Sets the maximum number of steps used in
            self-consisten scheme to find bound states.
        adaptive: bool (optional)
            This lets you choose to use an adaptive grid to find
            bound states.
        gamma: float(optional)
            Units of hopping in the code. Default is 1 eV.
        """

        self.device = device
        self.bs_low =  bs_low
        self.bs_upr = bs_upr
        self.bs_de = bs_de
        self.bs_tol = bs_tol
        self.bs_iter_max = bs_iter_max
        self.bs_adaptive = bs_adaptive
        self.gamma = gamma

        return

    def ret_gf(self, energy, time, eta_pow=5):
        """Evaluate the retarded Green function"""
        device = self.device
        ham = device.get_h(time)
        dim_h = ham.shape[0]

        eta = 10**(-eta_pow)
        ef_i2 = (energy + 1j*eta)*np.eye(dim_h)
        heff = ham + device.selfenergy(energy, time)
        gf = np.linalg.inv(ef_i2 - heff)
        return gf

    def bandwidth(self):
        """ Function to find the bandwidth of the system """
        device = self.device
        xi_cent = device.xi_cent
        bw_min = np.min(xi_cent) - 2*self.gamma
        bw_max = np.max(xi_cent) + 2*self.gamma
        return bw_min, bw_max

    def dos_en(self, en):
        """ Function to find the density of states at a energy point """
        time = 0
        gf = self.ret_gf(en, time)
        dos = np.trace(gf - np.conjugate(gf.T)) * ( 1j / (2*np.pi) )
        return dos

    def dos_sys(self):
        """ Function to find the density of states on a energy grid """
        device = self.device
        bw_min, bw_max = self.bandwidth()

        bs_adaptive = self.bs_adaptive
        if bs_adaptive:
            n_pts = 10000
            n_sub = 3
            write = False
            n_max = 10
            only_max = None
            # Create an object of the 'Grids' class for bottom band dos
            pool = Pool(2)
            f1 = self.dos_en
            solver_bot = Grids(f1, pool, dy_cutoff=1.e-2,
                              select_function=only_max,
                              x0=self.bs_low, x1=bw_min, n_pts=n_pts,
                              n_sub=n_sub, write=write,
                              nmax=n_max)
            solver_bot.iterate()
            energy_bot, dos_bot = solver.get_xy()

            # Create an object of the 'Grids' class for the top band dos
            solver_top = Grids(f1, pool, dy_cutoff=1.e-2,
                              select_function=only_max,
                              x0=bw_max, x1=self.bs_upr, n_pts=n_pts,
                              n_sub=n_sub, write=write,
                              nmax=n_max)
            solver_top.iterate()
            energy_top, dos_top = solver.get_xy()

            # Create an object of the 'Grids' class for the BAND itself
            solver_band = Grids(f1, pool, dy_cutoff=1.e-2,
                                select_function=only_max,
                                x0=bw_min, x1=bw_max, n_pts=n_pts,
                                n_sub=n_sub, write=write,
                                nmax=n_max)
            solver_band.iterate()
            energy_band, dos_band = solver.get_xy()

            # Now concatentate the energy points and the corressponding dos
            energy = np.concatentate((energy_bot, energy_band))
            energy = np.concatendate((energy, energy_top))
            dos = np.concatenate((dos_bot, dos_band))
            dos = np.concatenate((dos, dos_top))

        else:
            energy = np.arange(self.bs_low,
                               self.bs_upr, self.bs_de)
            dos = np.zeros(len(energy))
            for a, en in enumerate(energy):
                time = 0
                dos[a] = self.dos_en(en)

        return energy, dos

    def rough_bound_state_peaks(self, plot_dos=False):
        """ Function to find rough boundstate peaks
            Note 1: This estimate is done by plotting the DOS of the
            system. If unsure, you can plot the dos by choosing
            plot_dos = True and then check the chosen values. These
            will be rough estimates which will then be improved.
        """
        energy, dos = self.dos_sys()
        peaks_pos, _ = find_peaks(dos, height=0)
        all_peaks = energy[peaks_pos]

        bw_min, bw_max = self.bandwidth()

        # Find the peaks outside of the band
        peaks_bot = all_peaks[all_peaks < bw_min-10**(-10)]
        peaks_top = all_peaks[all_peaks > bw_max+10**(-10)]
        rough_peaks = np.concatenate( (peaks_bot, peaks_top) )

        # Plotting rough DOS if asked for
        if plot_dos:
            fig = plt.figure(figsize=(3.5, 3.5))
            gs = fig.add_gridspec(1, 1)
            ax = fig.add_subplot(gs[0, 0])
            ax.plot(energy, dos, lw=1, color='C1')
            ax.set_xlim([self.bs_low, self.bs_upr])
            for a, pks in enumerate(rough_peaks):
                ax.axvline(x=pks, lw=1, ls=':', color='k')
            fig.savefig('dos_rough.pdf', bbox_inches='tight')

        return rough_peaks

    def exact_bound_state_peaks(self, plot_dos=False):
        """ Function to find the exact boundstate peaks and states """
        rough_bs_peaks = self.rough_bound_state_peaks()
        if rough_bs_peaks == []:
            exact_bs_peaks = []
            exact_bs_states = []
        else:
            # Go over each bound state and make them exact
            exact_bs_peaks = np.zeros(len(rough_bs_peaks))
            exact_bs_pop = np.zeros(len(rough_bs_peaks))
            exact_bs_states = np.zeros((self.device.dim_h,
                                        len(rough_bs_peaks)), dtype=complex)
            for a, peak_value in enumerate(rough_bs_peaks):
                peak, state = self.converge_bs_peak(peak_value)
                exact_bs_peaks[a] = peak
                exact_bs_pop[a] = self.dos_en(peak)
                exact_bs_states[:, a] = state

        # Plotting exact DOS if asked for
        if plot_dos:
            fig = plt.figure(figsize=(3.5, 3.5))
            gs = fig.add_gridspec(1, 1)
            ax = fig.add_subplot(gs[0, 0])
            energy, dos = self.dos_sys(device=device)
            ax.plot(energy, dos, lw=1, color='C1')
            ax.set_xlim([self.bs_low, self.bs_upr])
            for a, pks in enumerate(exact_bs_peaks):
                ax.vline(x=pks, lw=1, ls=':', color='k')
            fig.savefig('dos_exact.pdf', bbox_inches='tight')

        return exact_bs_peaks, exact_bs_states, exact_bs_pop

    def converge_bs_peak(self, peak_value):
        """ Function to the find the state and converge a given rough peak """
        time = 0
        device = self.device
        ham = device.get_h(time)

        diff = self.bs_tol + 100
        peak_old = peak_value
        step = 1
        while diff > self.bs_tol:
            if step < self.bs_iter_max:
                se = device.selfenergy(peak_old, time)
                ham_ren = ham + se

                e_vals, e_vecs = linalg.eig(ham_ren)
                pos = abs(e_vals - peak_old).argmin()

                peak_new = e_vals[pos]
                diff = abs(peak_new - peak_old)

                # Update the peak and the steps
                peak_old = peak_new
                step = step + 1
            else:
                raise ValueError("Bound State convergence could not be reached")

        exact_bs_peak = peak_old
        exact_bs_state = e_vecs[:, pos]

        return  exact_bs_peak, exact_bs_state
